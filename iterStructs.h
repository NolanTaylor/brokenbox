#pragma once

#include "Objects.h"

#ifdef __EMSCRIPTEN__
#include <iostream>
#endif

struct struct_Bio
{
	Sprite brain;
	Sprite letter;
	Sprite dialogueBox;

	int lifted = -1, placed_x, placed_y, breaks = 0;

	std::string bio = "";

	std::string generate_word()
	{
		std::string word;
		int holder = rand() % 50;

		switch (holder)
		{
		case 0:
			word = " dance";
			break;
		case 1:
			word = " sing";
			break;
		case 2:
			word = " yeet";
			break;
		case 3:
			word = " burst";
			break;
		case 4:
			word = " engage";
			break;
		case 5:
			word = " flail";
			break;
		case 6:
			word = " juggle";
			break;
		case 7:
			word = " fumble";
			break;
		case 8:
			word = " punctuate";
			break;
		case 9:
			word = " toast";
			break;
		case 10:
			word = " roast";
			break;
		case 11:
			word = " satisfy";
			break;
		case 12:
			word = " stretch";
			break;
		case 13:
			word = " swell";
			break;
		case 14:
			word = " wonder";
			break;
		case 15:
			word = " majestic";
			break;
		case 16:
			word = " fabulous";
			break;
		case 17:
			word = " flabbertastic";
			break;
		case 18:
			word = " extraordinary";
			break;
		case 19:
			word = " imprudent";
			break;
		case 20:
			word = " lavish";
			break;
		case 21:
			word = " grand";
			break;
		case 22:
			word = " histrionic";
			break;
		case 23:
			word = " exotic";
			break;
		case 24:
			word = " wonderful";
			break;
		case 25:
			word = " fanciful";
			break;
		case 26:
			word = " nolan";
			break;
		case 27:
			word = " teriyaki sauce";
			break;
		case 28:
			word = " mustard";
			break;
		case 29:
			word = " peanut butter";
		case 30:
			word = " glommer";
			break;
		case 31:
			word = " cat";
			break;
		case 32:
			word = " undulate";
			break;
		case 33:
			word = " animation";
			break;
		case 34:
			word = " canvas";
			break;
		case 35:
			word = " bag";
			break;
		case 36:
			word = " juice";
			break;
		case 37:
			word = " girl";
			break;
		case 38:
			word = " upgrade";
			break;
		case 39:
			word = " signature";
			break;
		case 40:
			word = " flabber";
			break;
		case 41:
			word = " penis";
			break;
		case 42:
			word = " easter";
			break;
		case 43:
			word = " rabbit";
			break;
		case 44:
			word = " reeeeeeee";
			break;
		case 45:
			word = " matter";
			break;
		case 46:
			word = " arsenic";
			break;
		case 47:
			word = " lump";
			break;
		case 48:
			word = " tiny";
			break;
		case 49:
			word = " delicious";
			break;
		case 50:
			word = " kazoo";
			break;
		}

		return word;
	}

	std::string generate_word_common()
	{
		std::string word;
		int holder = rand() % 43;

		switch (holder)
		{
		case 0:
			word = " the";
			break;
		case 1:
			word = " be";
			break;
		case 2:
			word = " of";
			break;
		case 3:
			word = " while";
			break;
		case 4:
			word = " because";
			break;
		case 5:
			word = " and";
			break;
		case 6:
			word = " a";
			break;
		case 7:
			word = " in";
			break;
		case 8:
			word = " that";
			break;
		case 9:
			word = " have";
			break;
		case 10:
			word = " i";
			break;
		case 11:
			word = " it";
			break;
		case 12:
			word = " for";
			break;
		case 13:
			word = " not";
			break;
		case 14:
			word = " on";
			break;
		case 15:
			word = " with";
			break;
		case 16:
			word = " he";
			break;
		case 17:
			word = " as";
			break;
		case 18:
			word = " you";
			break;
		case 19:
			word = " do";
			break;
		case 20:
			word = " at";
			break;
		case 21:
			word = " this";
			break;
		case 22:
			word = " but";
			break;
		case 23:
			word = " his";
			break;
		case 24:
			word = " by";
			break;
		case 25:
			word = " from";
			break;
		case 26:
			word = " say";
			break;
		case 27:
			word = " or";
			break;
		case 28:
			word = " will";
			break;
		case 29:
			word = " an";
			break;
		case 30:
			word = " one";
			break;
		case 31:
			word = " all";
			break;
		case 32:
			word = " would";
			break;
		case 33:
			word = " what";
			break;
		case 34:
			word = " so";
			break;
		case 35:
			word = " if";
			break;
		case 36:
			word = " get";
			break;
		case 37:
			word = " just";
			break;
		case 38:
			word = " him";
			break;
		case 39:
			word = " over";
			break;
		case 40:
			word = " also";
			break;
		case 41:
			word = " yet";
			break;
		case 42:
			word = " most";
			break;
		case 43:
			word = " find";
		}

		return word;
	}

	std::string generate_word_ending()
	{
		std::string word;
		int holder = rand() % 20;

		switch (holder)
		{
		case 0:
			word = "ing";
			break;
		case 1:
			word = "ly";
			break;
		case 2:
			word = "al";
			break;
		case 3:
			word = "ation";
			break;
		case 4:
			word = "s";
			break;
		case 5:
			word = "ed";
			break;
		case 6:
			word = "er";
			break;
		case 7:
			word = "izzle";
			break;
		case 8:
			word = "oid";
			break;
		case 9:
			word = "ity";
			break;
		case 10:
			word = "ology";
			break;
		case 11:
			word = "ant";
			break;
		case 12:
			word = "ish";
			break;
		case 13:
			word = "ness";
			break;
		case 14:
			word = "ism";
			break;
		case 15:
			word = "icous";
			break;
		case 16:
			word = "ful";
			break;
		case 17:
			word = "ify";
			break;
		case 18:
			word = "est";
			break;
		case 19:
			word = "ize";
			break;
		case 20:
			word = "iddle";
			break;
		}

		return word;
	}

	void load()
	{
		brain.loadFromFile("Assets/brain.png");
		letter.loadFromFile("Assets/letter.png");
		dialogueBox.loadFromFile("Assets/dialogue.png");
	}

	void free()
	{
		brain.free();
		letter.free();
		dialogueBox.free();
	}
};

struct struct_Lauren
{
	Sprite Lauren;
	Sprite fade;

	int alpha = 0;

	void load()
	{
		Lauren.loadFromFile("Assets/Characters/Lauren.png");
		fade.loadFromFile("Assets/fade.png");
	}

	void free()
	{
		Lauren.free();
		fade.free();
	}
};

struct struct_Nolan
{
	struct_Nolan() { }
	~struct_Nolan() { }

	Sprite speak_ico;
	Sprite e_key;

	Character sprite[17];
	Character curtain[6];

	Engage engage[12];

	Sound Ruins;

	SDL_Rect speak_clip[4];

	enum index
	{
		tables, couch, chair, door, box, lauren,
		adamNicole, katJess, fellas, katy, sophia,
		jo, arya, sofiaElla, chloeLeila, fellas_all,
		ethan
	};

	void load()
	{
		e_key.loadFromFile("Assets/e_key.png");
		speak_ico.loadFromFile("Assets/speak_ico.png");

		for (int i = 0; i < sizeof(curtain) / sizeof(curtain[0]); i++)
		{ curtain[i].loadFromFile("Assets/curtain.png"); }

		curtain[0].setRange(142, 36, curtain[0].w, curtain[0].h);
		curtain[1].setRange(142, 102, curtain[0].w, curtain[0].h);
		curtain[2].setRange(142, 168, curtain[0].w, curtain[0].h);
		curtain[3].setRange(1419, 36, curtain[0].w, curtain[0].h);
		curtain[4].setRange(1419, 102, curtain[0].w, curtain[0].h);
		curtain[5].setRange(1419, 168, curtain[0].w, curtain[0].h);

		sprite[tables].loadFromFile("Assets/tables.png");
		sprite[couch].loadFromFile("Assets/couch.png");
		sprite[chair].loadFromFile("Assets/chair.png");
		sprite[door].loadFromFile("Assets/door.png");
		sprite[box].loadFromFile("Assets/box.png");
		sprite[lauren].loadFromFile("Assets/lauren.png");
		sprite[adamNicole].loadFromFile("Assets/adamNicole.png");
		sprite[katJess].loadFromFile("Assets/katherineJess.png");
		sprite[fellas].loadFromFile("Assets/fellas.png");
		sprite[katy].loadFromFile("Assets/katy.png");
		sprite[sophia].loadFromFile("Assets/sophia.png");
		sprite[jo].loadFromFile("Assets/jo.png");
		sprite[arya].loadFromFile("Assets/arya.png");
		sprite[sofiaElla].loadFromFile("Assets/sofiaElla.png");
		sprite[chloeLeila].loadFromFile("Assets/chloeLeila.png");
		sprite[fellas_all].loadFromFile("Assets/fellas_all.png");
		sprite[ethan].loadFromFile("Assets/ethan.png");

		sprite[tables].set(1734, 86);
		sprite[couch].set(886, 209, NULL, NULL, 235);
		sprite[chair].set(833, 199, NULL, NULL, 235);
		sprite[door].set(1185, 86);
		sprite[box].set(0, 96, NULL, NULL, 165, 360, 90);
		sprite[lauren].set(854, 794, 889, 794, 817);
		sprite[adamNicole].set(850, 193, 833, 184, 235);
		sprite[katJess].set(1257, 381, 1324, 375, 436, 180, 30);
		sprite[fellas].set(1652, 5);
		sprite[katy].set(945, 904, 934, 905, 925, 360, 60);
		sprite[sophia].set(953, 856);
		sprite[jo].set(14, 233, 41, 230, 260, 360, 30);
		sprite[arya].set(1296, 1117, 1324, 1117, 1145);
		sprite[sofiaElla].set(1296, 1117);
		sprite[chloeLeila].set(475, 240, NULL, NULL, 320, 180, 45);
		sprite[fellas_all].set(1652, 5);
		sprite[ethan].set(260, 230, 250, 220);

		engage[0].setRange(845, 200, 70, 40, "adam");
		engage[1].setRange(1260, 400, 60, 40, "katJess");
		engage[2].setRange(1680, 0, 100, 50, "fellas");
		engage[3].setRange(173, 4, 47, 40, "door1");
		engage[4].setRange(1578, 4, 48, 40, "door2");
		engage[5].setRange(1486, 313, 6, 3, "door3");
		engage[6].setRange(1544, 1193, 5, 10, "door4");
		engage[7].setRange(951, 907, 49, 46, "katy");
		engage[8].setRange(14, 233, 50, 50, "jo");
		engage[9].setRange(1326, 1146, 50, 30, "arya");
		engage[10].setRange(869, 768, 7, 2, "lauren");
		engage[11].setRange(265, 295, 20, 5, "ethan");

		Ruins.loadFromFile("Assets/Audio/Ruins.wav");

		for (int i = 0; i < 4; i++)
		{
			speak_clip[i].x = i * 17;
			speak_clip[i].y = 0;
			speak_clip[i].w = 15;
			speak_clip[i].h = 7;
		}

		for (int i = 0; i < 4; i++)
		{
			sprite[box].clip[i].x = i * 73;
			sprite[box].clip[i].y = 0;
			sprite[box].clip[i].w = 72;
			sprite[box].clip[i].h = 141;
		}

		for (int i = 0; i < 6; i++)
		{
			sprite[katJess].clip[i].x = i * 68;
			sprite[katJess].clip[i].y = 0;
			sprite[katJess].clip[i].w = 67;
			sprite[katJess].clip[i].h = 82;
		}

		for (int i = 0; i < 6; i++)
		{
			sprite[katy].clip[i].x = i * 74;
			sprite[katy].clip[i].y = 0;
			sprite[katy].clip[i].w = 73;
			sprite[katy].clip[i].h = 43;
		}

		for (int i = 0; i < 12; i++)
		{
			sprite[jo].clip[i].x = i * 53;
			sprite[jo].clip[i].y = 0;
			sprite[jo].clip[i].w = 52;
			sprite[jo].clip[i].h = 58;
		}

		for (int i = 0; i < 4; i++)
		{
			sprite[chloeLeila].clip[i].x = i * 95;
			sprite[chloeLeila].clip[i].y = 0;
			sprite[chloeLeila].clip[i].w = 94;
			sprite[chloeLeila].clip[i].h = 89;
		}
	}

	void free()
	{
		speak_ico.free();
		e_key.free();

		for (int i = 0; i < sizeof(sprite) / sizeof(sprite[0]); i++)
		{ sprite[i].free(); }

		for (int i = 0; i < sizeof(curtain) / sizeof(curtain[0]); i++)
		{ curtain[i].free(); }
	}
};

struct struct_Adam
{
	Sprite Adam;
	Sprite Nicole;

	int adam_x = 0, nicole_x = 700;

	bool front = 1;

	void load()
	{
		Adam.loadFromFile("Assets/Characters/Adam.png");
		Nicole.loadFromFile("Assets/Characters/Nicole.png");
	}

	void free()
	{
		Adam.free();
		Nicole.free();
	}
};

struct struct_katJess
{
	Sprite Katherine;
	Sprite Jessica;

	int katherine_x = -100, jessica_x = 100;

	bool front = 0;

	void load()
	{
		Katherine.loadFromFile("Assets/Characters/Katherine.png");
		Jessica.loadFromFile("Assets/Characters/Jessica.png");
	}

	void free()
	{
		Katherine.free();
		Jessica.free();
	}
};

struct struct_Fellas
{
	Sprite Alex;
	Sprite Doran;
	Sprite Justin;
	Sprite Arya;

	int front = 0;
	int alex_x = 0, doran_x = 0, justin_x = 0, arya_x = 0;

	void load()
	{
		Alex.loadFromFile("Assets/Characters/Alex.png");
		Doran.loadFromFile("Assets/Characters/Doran.png");
		Justin.loadFromFile("Assets/Characters/Justin.png");
		Arya.loadFromFile("Assets/Characters/Arya.png");
	}

	void free()
	{
		Alex.free();
		Doran.free();
		Justin.free();
		Arya.free();
	}
};

struct struct_Katy
{
	Sprite Katy;
	Sprite Sophia;

	bool front = 0;

	int katy_x = 0, sophia_x = 650, size = 1;

	void load()
	{
		Katy.loadFromFile("Assets/Characters/Katy.png");
		Sophia.loadFromFile("Assets/Characters/Sophia.png");
	}

	void free()
	{
		Katy.free();
		Sophia.free();
	}
};

struct struct_Jo
{
	Sprite Jo;

	void load()
	{
		Jo.loadFromFile("Assets/Characters/Jo.png");
	}

	void free()
	{
		Jo.free();
	}
};

struct struct_Arya
{
	Sprite Arya;
	Sprite Sofia;

	int front = 0, arya_x = 0;

	void load()
	{
		Arya.loadFromFile("Assets/Characters/Arya.png");
		Sofia.loadFromFile("Assets/Characters/Sofia.png");
	}

	void free()
	{
		Arya.free();
		Sofia.free();
	}
};

struct struct_Ethan
{
	Sprite Ethan;

	void load()
	{
		Ethan.loadFromFile("Assets/Characters/Ethan.png");
	}

	void free()
	{
		Ethan.free();
	}
};

struct struct_Backstage
{
	Sprite e_key;
	Sprite tapeMeasure;

	Engage engage[15];

	Sound Ruins;

	void load()
	{
		e_key.loadFromFile("Assets/e_key.png");
		tapeMeasure.loadFromFile("Assets/tapeMeasure.png");

		engage[0].setRange(170, 0, 15, 50, "fridge");
		engage[1].setRange(85, 0, 45, 50, "cabinet");
		engage[2].setRange(44, 160, 4, 5, "tape");
		engage[3].setRange(249, 5, 14, 4, "door5");
		engage[4].setRange(246, 176, 19, 4, "door6");
		engage[5].setRange(340, 79, 24, 85, "door7");
		engage[6].setRange(1168, 177, 40, 4, "door8");
		engage[7].setRange(1561, 73, 4, 19, "door9");
		engage[8].setRange(1561, 314, 2, 2, "door10");
		engage[9].setRange(1440, 434, 77, 5, "door11");
		engage[10].setRange(1363, 505, 4, 1, "door12");
		engage[11].setRange(1560, 540, 6, 3, "door13");
		engage[12].setRange(1370, 957, 3, 3, "door14");
		engage[13].setRange(980, 28, 13, 4, "laptop");
		engage[14].setRange(1068, 29, 29, 5, "rachel");

		Ruins.loadFromFile("Assets/Audio/Ruins_Muffled.wav");
	}

	void free()
	{
		e_key.free();
		tapeMeasure.free();
	}
};

struct struct_maleDressingRoom
{
	Sprite e_key;

	Engage engage[2];

	void load()
	{
		e_key.loadFromFile("Assets/e_key.png");

		engage[0].setRange(176, 302, 2, 2, "door15");
		engage[1].setRange(485, 114, 23, 4, "door16");
	}

	void free()
	{
		e_key.free();
	}
};

struct struct_dressingRoomShow1
{
	Sprite e_key;

	Engage engage[2];

	void load()
	{
		e_key.loadFromFile("Assets/e_key.png");

		engage[0].setRange(485, 114, 23, 4, "door16");
		engage[1].setRange(0, 0, 0, 0, "someone?");
	}

	void free()
	{
		e_key.free();
	}
};
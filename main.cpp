#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <SDL.h>
#include <SDL_image.h>
#include <time.h>
#include "mixer.h"
#include "iterStructs.h"
#include "Objects.h"
#include "Obstacles.h"

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

#define musicChannel 2
#define soundChannel 3
#define tag 0
#define tag_x 130
#define tag_y 554
#define speak 1
#define speak_x	115
#define speak_y 590
#define choice1 2
#define choice1_x 150
#define choice1_y 640
#define choice2 3
#define choice2_x 300
#define choice2_y 640

void RenderLoopCallback(void* arg);
void callbackOther(void* arg);
void callbackTech1(void* arg);
void callbackShow1(void* arg);
void next();
void toTech1();
void toShow1();
void toClass2();

class Start
{
public:
	Start() { }
	~Start() { free(); }

	Uint32 startTime = 0;
	Uint32 endTime = 0;
	Uint32 delta = 0;
	short fps = 60;
	short timePerFrame = 1000 / 60; // miliseconds

	Sprite search;
	Sprite searchBar;
	Sprite searchResults;
	Sprite stackOverflowPage;
	Sprite cursor;
	Sprite dialogueBox;
	Sprite start;
	Sprite start_hover;
	Sprite scene;
	Sprite scene_hover;
	Sprite moar;
	Sprite moar_hover;

	Text dialogue;

	Sound elsewhere;

	SDL_Event event;

	SDL_Rect bar = { 0, 0, 6, 32 };

	const int button_x = 28, start_y = 215, scene_y = 290, moar_y = 365, button_w = 165, button_h = 37;

	int cursor_x = 300, cursor_y = 200, act = 0, choice = -1,
		mouse_x = NULL, mouse_y = NULL;

	bool quit = false, start_h = false, scene_h = false, moar_h = false;

	void load()
	{
		search.loadFromFile("Assets/search.png");
		searchBar.loadFromFile("Assets/searchBar.png");
		searchResults.loadFromFile("Assets/searchResults.png");
		stackOverflowPage.loadFromFile("Assets/stackOverflowPage.png");
		cursor.loadFromFile("Assets/cursor.png");
		dialogueBox.loadFromFile("Assets/dialogue.png");
		dialogue.load("Assets/text.png", "Assets/Audio/textClip_und.wav"); // feeling pretty; might change later
		start.loadFromFile("Assets/START.png");
		start_hover.loadFromFile("Assets/START_HOVER.png");
		scene.loadFromFile("Assets/SCENE.png");
		scene_hover.loadFromFile("Assets/SCENE_HOVER.png");
		moar.loadFromFile("Assets/MOAR.png");
		moar_hover.loadFromFile("Assets/MOAR_HOVER.png");

		elsewhere.loadFromFile("Assets/Audio/It's_Raining_Somewhere_Else.wav");
	}

	void free()
	{
		search.free();
		searchBar.free();
		searchResults.free();
		stackOverflowPage.free();
		cursor.free();
		dialogueBox.free();
		dialogue.free();
		start.free();
		start_hover.free();
		scene.free();
		scene_hover.free();
		moar.free();
		moar_hover.free();
		//rainingElsewhere.free();
	}

	void loopinTime()
	{
		#ifdef __EMSCRIPTEN__
			emscripten_set_main_loop_arg(&RenderLoopCallback, this, 120, 1);
		#else
		while (!quit)
		{
			if (!startTime)
				startTime = SDL_GetTicks();
			else
				delta = endTime - startTime;

			if (delta < timePerFrame)
				SDL_Delay(timePerFrame - delta);
			if (delta > timePerFrame)
				fps = 1000 / delta;

			iter();

			startTime = endTime;
			endTime = SDL_GetTicks();
		}
		#endif
	}

	void iter()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_MOUSEBUTTONDOWN:
				if (act == 1)
				{
					if (start_h)
					{ act++; }
					else if (scene_h)
					{ }
					else if (moar_h)
					{ }
				}
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_SPACE:
					if (act == 1)
					{
						if (start_h)
						{ act++; }
						else if (scene_h)
						{ }
						else if (moar_h)
						{ }
					}
					break;
				case SDLK_w:
					if (act == 1)
					{
						if (choice > 0)
						{ choice--; }
					}
					break;
				case SDLK_s:
					if (act == 1)
					{
						if (choice < 2)
						{ choice++; }
					}
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		SDL_RenderClear(renderer);

		switch (act)
		{
		case 0:
			load();
			elsewhere.setChannel(musicChannel);
			elsewhere.play(-1);
			SDL_Delay(5000); // wait for init? maybe...
			cursor_x = 615;
			cursor_y = 500;
			act++;
			break;
		case 1:
			search.render(0, (SCREEN_HEIGHT - search.getHeight()) / 2);

			if (mouse_x > button_x && mouse_x < button_x + button_w && mouse_y > start_y && mouse_y < start_y + button_h)
			{ start_h = true; choice = -1; }
			else if (choice == 0)
			{ start_h = true; }
			else
			{ start_h = false; }

			if (mouse_x > button_x && mouse_x < button_x + button_w && mouse_y > scene_y && mouse_y < scene_y + button_h)
			{ scene_h = true; choice = -1; }
			else if (choice == 1)
			{ scene_h = true; }
			else
			{ scene_h = false; }

			if (mouse_x > button_x && mouse_x < button_x + button_w && mouse_y > moar_y && mouse_y < moar_y + button_h)
			{ moar_h = true; choice = -1; }
			else if (choice == 2)
			{ moar_h = true; }
			else
			{ moar_h = false; }
			
			if (!start_h)
			{ start.render(button_x, start_y); }
			else
			{ start_hover.render(button_x, start_y); }

			if (!scene_h)
			{ scene.render(button_x, scene_y); }
			else
			{ scene_hover.render(button_x, scene_y); }

			if (!moar_h)
			{ moar.render(button_x, moar_y); }
			else
			{ moar_hover.render(button_x, moar_y); }

			break;
		case 2:
			search.render(0, (SCREEN_HEIGHT - search.getHeight()) / 2);
			cursor.render(cursor_x, cursor_y);
			// maybe move overlay......
			// if (start) { act++; }
			act++;
			break;
		case 3:
			// it's raining somewhere else?
			search.render(0, (SCREEN_HEIGHT - search.getHeight()) / 2);
			cursor.render(cursor_x, cursor_y);
			cursor_x -= 2; cursor_y--;

			if (cursor_x < 314) { act++; }
			break;
		case 4:
			search.render(0, (SCREEN_HEIGHT - search.getHeight()) / 2);
			cursor.render(cursor_x, cursor_y);
			SDL_Delay(500);
			act++;
			break;
		case 5:
			search.render(0, (SCREEN_HEIGHT - search.getHeight()) / 2);
			searchBar.render(300, 335, &bar);
			switch (bar.w)
			{
			case 6:
				bar.w += 10; break;
			case 16:
				bar.w += 11; break;
			case 27:
				bar.w += 21; break;
			case 48:
				bar.w += 5; break;
			case 53:
				bar.w += 11; break;
			case 64:
				bar.w += 16; break;
			case 80:
				bar.w += 4; break;
			case 84:
				bar.w += 5; break;
			case 89:
				bar.w += 9; break;
			case 98:
				bar.w += 11; break;
			case 109:
				bar.w += 11; break;
			case 120:
				bar.w += 4; break;
			case 124:
				bar.w += 5; break;
			case 129:
				bar.w += 16; break;
			case 145:
				bar.w += 15; break;
			case 160:
				bar.w += 4; break;
			case 164:
				bar.w += 6; break;
			case 170:
				bar.w += 16; break;
			case 186:
				bar.w += 7; break;
			case 193:
				bar.w += 10; break;
			case 203:
				bar.w += 7; break;
			case 210:
				bar.w += 15; break;
			case 225:
				act++; break;
			} // add typing and clicking sounds
			SDL_Delay(100);
			cursor.render(cursor_x, cursor_y);
			break;
		case 6:
			search.render(0, (SCREEN_HEIGHT - search.getHeight()) / 2);
			searchBar.render(300, 335, &bar);
			cursor.render(cursor_x, cursor_y);
			SDL_Delay(500);
			act++;
			break;
		case 7:
			search.render(0, (SCREEN_HEIGHT - search.getHeight()) / 2);
			searchBar.render(300, 335, &bar);
			cursor.render(cursor_x, cursor_y);
			if (cursor_y > 400) { act++; }
			cursor_y++; cursor_x++;
			break;
		case 8:
			search.render(0, (SCREEN_HEIGHT - search.getHeight()) / 2);
			searchBar.render(300, 335, &bar);
			cursor.render(cursor_x, cursor_y);
			SDL_Delay(500);
			act++;
			break;
		case 9:
			searchResults.render(0, (SCREEN_HEIGHT - searchResults.getHeight()) / 2);
			cursor.render(cursor_x, cursor_y);
			act++;
			break;
		case 10:
			searchResults.render(0, (SCREEN_HEIGHT - searchResults.getHeight()) / 2);
			cursor.render(cursor_x, cursor_y);
			if (cursor_y > 260) { cursor_y -= 2; cursor_x--; }
			else { act++; }
			break;
		case 11:
			searchResults.render(0, (SCREEN_HEIGHT - searchResults.getHeight()) / 2);
			cursor.render(cursor_x, cursor_y);
			SDL_Delay(500);
			act++;
			break;
		case 12:
			stackOverflowPage.render(0, (SCREEN_HEIGHT - stackOverflowPage.getHeight()) / 2);
			cursor.render(cursor_x, cursor_y);
			dialogue.setString(tag, "Lauren");
			dialogue.setString(speak, "Hey shouldn't you be doin' something productive?");
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, speak_x, speak_y);
			act++;
			break;
		case 13:
			stackOverflowPage.render(0, (SCREEN_HEIGHT - stackOverflowPage.getHeight()) / 2);
			cursor.render(cursor_x, cursor_y);
			SDL_Delay(3000);
			act++;
			break;
		case 14:
			stackOverflowPage.render(0, (SCREEN_HEIGHT - stackOverflowPage.getHeight()) / 2);
			cursor.render(cursor_x, cursor_y);
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);
			dialogue.type(speak, true);
			if (dialogue.getTyping(speak)) { dialogue.stop(); act++; }
			break;
		case 15:
			stackOverflowPage.render(0, (SCREEN_HEIGHT - stackOverflowPage.getHeight()) / 2);
			cursor.render(cursor_x, cursor_y);
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);
			dialogue.type(speak, false);
			SDL_Delay(2000);
			act++;
			break;
		case 16:
			elsewhere.stop();
			elsewhere.free();
			free();
			#ifdef __EMSCRIPTEN__
				emscripten_cancel_main_loop();
				next();
			#endif
			quit = true;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}
};

class Other
{
public:
	Other() { }
	~Other() { free(); }

	Uint32 startTime = 0;
	Uint32 endTime = 0;
	Uint32 delta = 0;
	short fps = 60;
	short timePerFrame = 1000 / 60; // miliseconds

	Sprite theatre_;
	Sprite backstage_;
	Sprite maleDressingRoom_;
	Sprite dialogueBox;

	Sound rainingElsewhere;

	Actor nolan;

	Text dialogue;

	SDL_Event event;

	SDL_Rect camera = { 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT };
	SDL_Rect walkingSprite[20];
	SDL_Rect theatre[50];
	SDL_Rect backstage[30];
	SDL_Rect maleDressingRoom[10];

	SDL_Rect* room = theatre;

	enum loops
	{
		otherLoop, backstageLoop, bioLoop, laurenLoop, laurenLoop_after, laurenLoop_bio,
		nolanLoop, nolanLoop_backstage, nolanLoop_maleDressingRoom, adamLoop, katJessLoop,
		fellasLoop, fellasLoop_after, fellasLoop_arya, katyLoop, katyLoop_after, joLoop,
		aryaLoop, aryaLoop_after, aryaLoop_tape, ethanLoop, ethanLoop_after,
		maleDressingRoomLoop,
	};

	enum interact // array of those interacted with
	{
		fellasInt, katyInt, aryaInt, ethanInt,
		cabinetInt, tapeInt,
	};

	const Uint8* state = SDL_GetKeyboardState(NULL);

	const int THEATRE_WIDTH = 1800, THEATRE_HEIGHT = 1300;
	const int BACKSTAGE_WIDTH = 1651, BACKSTAGE_HEIGHT = 1180;
	const int DRESSING_WIDTH = 900, DRESSING_HEIGHT = 700;

	int act = 0, mouse_x = NULL, mouse_y = NULL, globalCount = 0, choice = 0,
		char_x = (SCREEN_WIDTH - 720) / 2, char_y = SCREEN_HEIGHT - 630;
	int loopBack = otherLoop;
	int loopFront = laurenLoop; // starts at lauren

	bool quit = false, speech = true, advance = false;
	bool arya_free = false, bio_achieved = false;
	bool tapeMeasure_sprite = true;

	bool interactions[10];

	/* --- sprites to be loaded for separate loops --- */

	struct_Lauren lauren;
	struct_Bio bio;
	struct_Nolan overlay;
	struct_Adam adam;
	struct_katJess katJess;
	struct_Fellas fellas;
	struct_Katy katy;
	struct_Jo jo;
	struct_Arya arya;
	struct_Ethan ethan;

	struct_Backstage back;

	struct_maleDressingRoom male;

	/*-------------------------------------------------*/

	void load()
	{
		theatre_.loadFromFile("Assets/theatre.png");
		backstage_.loadFromFile("Assets/backstage.png");
		maleDressingRoom_.loadFromFile("Assets/maleDressingRoom.png");
		nolan.loadFromFile("Assets/walking.png");
		dialogue.load("Assets/text.png", "Assets/Audio/textClip_und.wav");
		dialogueBox.loadFromFile("Assets/dialogue.png");

		setTheatre(theatre);
		setBackstage(backstage);
		setMaleDressingRoom(maleDressingRoom);

		for (int i = 0; i < 20; i++) // dimensions for walking sprite sheet (unfinished)
		{
			walkingSprite[i].x = (i % 5) * 36;
			walkingSprite[i].y = (i / 5) * 83;
			walkingSprite[i].w = 35;
			walkingSprite[i].h = 82;
		}

		for (int i = 0; i < sizeof(interactions) / sizeof(interactions[0]); i++)
		{ interactions[i] = false; }
	}

	void free()
	{
		theatre_.free();
		backstage_.free();
		maleDressingRoom_.free();
		nolan.free();
		dialogue.free();
	}

	bool inRange(SDL_Rect rect)
	{
		if (nolan.x + nolan.CHAR_WIDTH > rect.x && nolan.x < rect.x + rect.w &&
			nolan.y + nolan.CHAR_HEIGHT > rect.y && nolan.y < rect.y + rect.h)
		{ return true; }
		else
		{ return false; }
	}

	void loopinTime()
	{
		#ifdef __EMSCRIPTEN__
			emscripten_set_main_loop_arg(&callbackOther, this, 120, 1);
		#else
		while (!quit)
		{
			if (!startTime)
				startTime = SDL_GetTicks();
			else
				delta = endTime - startTime;

			if (delta < timePerFrame)
				SDL_Delay(timePerFrame - delta);
			if (delta > timePerFrame)
				fps = 1000 / delta;

			iter();

			startTime = endTime;
			endTime = SDL_GetTicks();
		}
		#endif
	}

	void iter()
	{
		switch (loopBack)
		{
		case otherLoop:
			iter_other();
			break;
		case backstageLoop:
			iter_backstage();
			break;
		case maleDressingRoomLoop:
			iter_maleDressingRoom();
			break;
		}

		switch (loopFront)
		{
		case nolanLoop:
			iter_Nolan();
			break;
		case nolanLoop_backstage:
			iter_Nolan_backstage();
			break;
		case nolanLoop_maleDressingRoom:
			iter_Nolan_maleDressingRoom();
			break;
		case bioLoop:
			iter_Bio();
			break;
		case laurenLoop:
			iter_Lauren();
			break;
		case laurenLoop_after:
			iter_Lauren_after();
			break;
		case laurenLoop_bio:
			iter_Lauren_bio();
			break;
		case adamLoop:
			iter_Adam();
			break;
		case katJessLoop:
			iter_katJess();
			break;
		case fellasLoop:
			iter_Fellas();
			break;
		case fellasLoop_after:
			iter_Fellas_after();
			break;
		case fellasLoop_arya:
			iter_Fellas_arya();
			break;
		case katyLoop:
			iter_Katy();
			break;
		case katyLoop_after:
			iter_Katy_after();
			break;
		case joLoop:
			iter_Jo();
			break;
		case aryaLoop:
			iter_Arya();
			break;
		case aryaLoop_after:
			iter_Arya_after();
			break;
		case aryaLoop_tape:
			iter_Arya_tape();
			break;
		case ethanLoop:
			iter_Ethan();
			break;
		case ethanLoop_after:
			iter_Ethan_after();
			break;
		}
	}
	
	/*-------------------- BACK LOOPS ---------------------*/

	void iter_other()
	{
		SDL_RenderClear(renderer);

		camera.x = (nolan.x + nolan.CHAR_WIDTH / 2) - SCREEN_WIDTH / 2;
		camera.y = (nolan.y + nolan.CHAR_HEIGHT / 2) - SCREEN_HEIGHT / 2;

		if (camera.x < 0) { camera.x = 0; }
		if (camera.y < 0) { camera.y = 0; }
		if (camera.x > THEATRE_WIDTH - camera.w) { camera.x = THEATRE_WIDTH - camera.w; }
		if (camera.y > THEATRE_HEIGHT - camera.h) { camera.y = THEATRE_HEIGHT - camera.h; }

		theatre_.render(0, 0, &camera);

		/*-------------------------------------------------------------------------------------*/

		for (int i = 0; i < sizeof(overlay.sprite) / sizeof(overlay.sprite[0]); i++)
		{
			if (!overlay.sprite[i].front && overlay.sprite[i].display)
			{
				if (overlay.sprite[i].clip_init)
				{
					overlay.sprite[i].render(overlay.sprite[i].x - camera.x, overlay.sprite[i].y - camera.y,
						&overlay.sprite[i].clip[globalCount % overlay.sprite[i].mod / overlay.sprite[i].div]);
				}
				else
				{ overlay.sprite[i].render(overlay.sprite[i].x - camera.x, overlay.sprite[i].y - camera.y); }

				if (overlay.sprite[i].ico_x != NULL)
				{
					overlay.speak_ico.render(overlay.sprite[i].ico_x - camera.x, overlay.sprite[i].ico_y - camera.y,
						&overlay.speak_clip[globalCount % 120 / 30]);
				}
			}
		}

		for (int i = 0; i < sizeof(overlay.curtain) / sizeof(overlay.curtain[0]); i++)
		{
			if (!overlay.curtain[i].front)
			{ overlay.curtain[i].render(overlay.curtain[i].range.x - camera.x, overlay.curtain[i].range.y - camera.y); }
		}

		nolan.render(nolan.x - camera.x, nolan.y - camera.y, &walkingSprite[nolan.anim]);

		for (int i = 0; i < sizeof(overlay.sprite) / sizeof(overlay.sprite[0]); i++)
		{
			if (overlay.sprite[i].front && overlay.sprite[i].display)
			{
				if (overlay.sprite[i].clip_init)
				{
					overlay.sprite[i].render(overlay.sprite[i].x - camera.x, overlay.sprite[i].y - camera.y,
						&overlay.sprite[i].clip[globalCount % overlay.sprite[i].mod / overlay.sprite[i].div]);
				}
				else
				{ overlay.sprite[i].render(overlay.sprite[i].x - camera.x, overlay.sprite[i].y - camera.y); }

				if (overlay.sprite[i].ico_x != NULL)
				{
					overlay.speak_ico.render(overlay.sprite[i].ico_x - camera.x, overlay.sprite[i].ico_y - camera.y,
						&overlay.speak_clip[globalCount % 120 / 30]);
				}
			}
		}

		for (int i = 0; i < sizeof(overlay.curtain) / sizeof(overlay.curtain[0]); i++)
		{
			if (overlay.curtain[i].front)
			{ overlay.curtain[i].render(overlay.curtain[i].range.x - camera.x, overlay.curtain[i].range.y - camera.y); }
		}

		for (int i = 0; i < sizeof(overlay.engage) / sizeof(overlay.engage[0]); i++)
		{
			if (overlay.engage[i].engage)
			{ overlay.e_key.render(nolan.x - 5 - camera.x, nolan.y - 5 - camera.y); break; }
		}

		/*-------------------------------------------------------------------------------------*/
	}

	void iter_backstage()
	{
		SDL_RenderClear(renderer);

		camera.x = (nolan.x + nolan.CHAR_WIDTH / 2) - SCREEN_WIDTH / 2;
		camera.y = (nolan.y + nolan.CHAR_HEIGHT / 2) - SCREEN_HEIGHT / 2;

		if (camera.x < 0) { camera.x = 0; }
		if (camera.y < 0) { camera.y = 0; }
		if (camera.x > BACKSTAGE_WIDTH - camera.w) { camera.x = BACKSTAGE_WIDTH - camera.w; }
		if (camera.y > BACKSTAGE_HEIGHT - camera.h) { camera.y = BACKSTAGE_HEIGHT - camera.h; }

		backstage_.render(0, 0, &camera);

		if (tapeMeasure_sprite)
		{ back.tapeMeasure.render(35 - camera.x, 157 - camera.y); }

		nolan.render(nolan.x - camera.x, nolan.y - camera.y, &walkingSprite[nolan.anim]);

		for (int i = 0; i < sizeof(back.engage) / sizeof(back.engage[0]); i++)
		{
			if (back.engage[i].engage)
			{ back.e_key.render(nolan.x - 5 - camera.x, nolan.y - 5 - camera.y); break; }
		}
	}

	void iter_maleDressingRoom()
	{
		SDL_RenderClear(renderer);

		camera.x = (nolan.x + nolan.CHAR_WIDTH / 2) - SCREEN_WIDTH / 2;
		camera.y = (nolan.y + nolan.CHAR_HEIGHT / 2) - SCREEN_HEIGHT / 2;

		if (camera.x < 0) { camera.x = 0; }
		if (camera.y < 0) { camera.y = 0; }
		if (camera.x > DRESSING_WIDTH - camera.w) { camera.x = DRESSING_WIDTH - camera.w; }
		if (camera.y > DRESSING_HEIGHT - camera.h) { camera.y = DRESSING_HEIGHT - camera.h; }

		maleDressingRoom_.render(0, 0, &camera);

		nolan.render(nolan.x - camera.x, nolan.y - camera.y, &walkingSprite[nolan.anim]);

		for (int i = 0; i < sizeof(male.engage) / sizeof(male.engage[0]); i++)
		{
			if (male.engage[i].engage)
			{ male.e_key.render(nolan.x - 5 - camera.x, nolan.y - 5 - camera.y); break; }
		}
	}

	/*-------------------- FRONT LOOPS -------------------*/

	void iter_Bio()  // it's working so please DON'T TOUCH IT!!!1!
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_MOUSEBUTTONDOWN:
				if (mouse_x > 500 && mouse_x < 580 && mouse_y > 150 && mouse_y < 165)
				{ bio.lifted = 3; bio.placed_x = mouse_x; bio.placed_y = mouse_y; }
				else if (mouse_x > 560 && mouse_x < 640 && mouse_y > 200 && mouse_y < 215)
				{ bio.lifted = 4; bio.placed_x = mouse_x; bio.placed_y = mouse_y; }
				else if (mouse_x > 700 && mouse_x < 780 && mouse_y > 160 && mouse_y < 175)
				{ bio.lifted = 5; bio.placed_x = mouse_x; bio.placed_y = mouse_y; }
				else if (mouse_x > 690 && mouse_x < 770 && mouse_y > 250 && mouse_y < 265)
				{ bio.lifted = 6; bio.placed_x = mouse_x; bio.placed_y = mouse_y; }
				else if (mouse_x > 550 && mouse_x < 590 && mouse_y > 300 && mouse_y < 315)
				{ bio.lifted = 7; bio.placed_x = mouse_x; bio.placed_y = mouse_y; }
				else if (mouse_x > 730 && mouse_x < 770 && mouse_y > 320 && mouse_y < 335)
				{ bio.lifted = 8; bio.placed_x = mouse_x; bio.placed_y = mouse_y; }
				else if (mouse_x > 650 && mouse_x < 690 && mouse_y > 290 && mouse_y < 305)
				{ bio.lifted = 9; bio.placed_x = mouse_x; bio.placed_y = mouse_y; }
				else if (mouse_x > 460 && mouse_x < 500 && mouse_y > 220 && mouse_y < 235)
				{ bio.lifted = 10; bio.placed_x = mouse_x; bio.placed_y = mouse_y; }
				else if (mouse_x > 510 && mouse_x < 550 && mouse_y > 265 && mouse_y < 280)
				{ bio.lifted = 11; bio.placed_x = mouse_x; bio.placed_y = mouse_y; }
				else if (mouse_x > 750 && mouse_x < 790 && mouse_y > 280 && mouse_y < 295)
				{ bio.lifted = 12; bio.placed_x = mouse_x; bio.placed_y = mouse_y; }
				else if (mouse_x > 655 && mouse_x < 695 && mouse_y > 180 && mouse_y < 195)
				{ bio.lifted = 13; bio.placed_x = mouse_x; bio.placed_y = mouse_y; }
				else if (mouse_x > 670 && mouse_x < 710 && mouse_y > 230 && mouse_y < 245)
				{ bio.lifted = 14; bio.placed_x = mouse_x; bio.placed_y = mouse_y; }
				else if (mouse_x > 580 && mouse_x < 660 && mouse_y > 110 && mouse_y < 125)
				{ bio.lifted = 15; bio.placed_x = mouse_x; bio.placed_y = mouse_y; }
				else if (mouse_x > 730 && mouse_x < 770 && mouse_y > 200 && mouse_y < 215)
				{ bio.lifted = 16; bio.placed_x = mouse_x; bio.placed_y = mouse_y; }
				else if (mouse_x > 340 && mouse_x < 380 && mouse_y > 545 && mouse_y < 560)
				{
					act++;
					dialogue.free();
					dialogue.load("Assets/text.png", "Assets/Audio/textClip_und.wav");
					for (int i = 3; i <= 16; i++)
					{ dialogue.setString(i, ""); }
					dialogue.setCoords(speak, speak_x, speak_y);
					dialogue.setString(speak, "A masterpiece");
					speech = true;
					advance = false;
				}
				break;
			case SDL_MOUSEBUTTONUP:
				if ((mouse_x > 30 && mouse_x < 400 && mouse_y > 30 && mouse_y < 560) && bio.lifted > 0)
				{
					if (bio.bio.length() - (bio.breaks * 40) > 40)
					{ bio.bio += "/"; bio.breaks++; }

					bio.bio += dialogue.getString(bio.lifted);
					dialogue.setString(1, bio.bio);

					dialogue.setString(3, bio.generate_word());
					dialogue.setString(4, bio.generate_word());
					dialogue.setString(5, bio.generate_word());
					dialogue.setString(6, bio.generate_word());
					dialogue.setString(7, bio.generate_word_common());
					dialogue.setString(8, bio.generate_word_common());
					dialogue.setString(9, bio.generate_word_common());
					dialogue.setString(10, bio.generate_word_common());
					dialogue.setString(11, bio.generate_word_common());
					dialogue.setString(12, bio.generate_word_ending());
					dialogue.setString(13, bio.generate_word_ending());
					dialogue.setString(14, bio.generate_word_ending());
					dialogue.setString(15, bio.generate_word());
					dialogue.setString(16, bio.generate_word_common());
				}

				dialogue.setCoords(3, 500, 150); dialogue.setCoords(4, 560, 200);
				dialogue.setCoords(5, 700, 160); dialogue.setCoords(6, 690, 250);
				dialogue.setCoords(7, 550, 300); dialogue.setCoords(8, 730, 320);
				dialogue.setCoords(9, 650, 290); dialogue.setCoords(10, 460, 220);
				dialogue.setCoords(11, 510, 265); dialogue.setCoords(12, 750, 280);
				dialogue.setCoords(13, 655, 180); dialogue.setCoords(14, 670, 230);
				dialogue.setCoords(15, 580, 110); dialogue.setCoords(16, 730, 200);

				bio.lifted = -1; bio.placed_x = NULL; bio.placed_y = NULL;

				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		switch (bio.lifted)
		{
		case 3:
			dialogue.setCoords(3, 500 + mouse_x - bio.placed_x, 150 + mouse_y - bio.placed_y);
			break;
		case 4:
			dialogue.setCoords(4, 560 + mouse_x - bio.placed_x, 200 + mouse_y - bio.placed_y);
			break;
		case 5:
			dialogue.setCoords(5, 700 + mouse_x - bio.placed_x, 160 + mouse_y - bio.placed_y);
			break;
		case 6:
			dialogue.setCoords(6, 690 + mouse_x - bio.placed_x, 250 + mouse_y - bio.placed_y);
			break;
		case 7:
			dialogue.setCoords(7, 550 + mouse_x - bio.placed_x, 300 + mouse_y - bio.placed_y);
			break;
		case 8:
			dialogue.setCoords(8, 730 + mouse_x - bio.placed_x, 320 + mouse_y - bio.placed_y);
			break;
		case 9:
			dialogue.setCoords(9, 650 + mouse_x - bio.placed_x, 290 + mouse_y - bio.placed_y);
			break;
		case 10:
			dialogue.setCoords(10, 460 + mouse_x - bio.placed_x, 220 + mouse_y - bio.placed_y);
			break;
		case 11:
			dialogue.setCoords(11, 510 + mouse_x - bio.placed_x, 265 + mouse_y - bio.placed_y);
			break;
		case 12:
			dialogue.setCoords(12, 750 + mouse_x - bio.placed_x, 280 + mouse_y - bio.placed_y);
			break;
		case 13:
			dialogue.setCoords(13, 655 + mouse_x - bio.placed_x, 180 + mouse_y - bio.placed_y);
			break;
		case 14:
			dialogue.setCoords(14, 670 + mouse_x - bio.placed_x, 230 + mouse_y - bio.placed_y);
			break;
		case 15:
			dialogue.setCoords(15, 580 + mouse_x - bio.placed_x, 110 + mouse_y - bio.placed_y);
			break;
		case 16:
			dialogue.setCoords(16, 730 + mouse_x - bio.placed_x, 200 + mouse_y - bio.placed_y);
			break;
		default:
			break;
		}

		bio.letter.render(0, 20, NULL, 1.4);
		bio.brain.render(410, 50, NULL, 2);

		for (int i = 0; i <= 16; i++)
		{ dialogue.type(i); }

		switch (act)
		{
		case 0:
			rainingElsewhere.loadFromFile("Assets/Audio/It's_Raining_Somewhere_Else.wav");
			rainingElsewhere.setChannel(musicChannel);
			bio.load();
			dialogue.free();
			dialogue.load("Assets/textBlack.png", "Assets/Audio/textClip_und.wav");
			speech = true;
			rainingElsewhere.play(-1);
			dialogue.setString(0, "drag words from nolan's brain to create a bio");
			dialogue.setString(1, "");
			dialogue.setString(2, "done");
			dialogue.setCoords(0, 50, 40);
			dialogue.setCoords(1, 56, 66);	 dialogue.setCoords(2, 340, 545);
			dialogue.setCoords(3, 500, 150); dialogue.setCoords(4, 560, 200);
			dialogue.setCoords(5, 700, 160); dialogue.setCoords(6, 690, 250);
			dialogue.setCoords(7, 550, 300); dialogue.setCoords(8, 730, 320);
			dialogue.setCoords(9, 650, 290); dialogue.setCoords(10, 460, 220);
			dialogue.setCoords(11, 510, 265); dialogue.setCoords(12, 750, 280);
			dialogue.setCoords(13, 655, 180); dialogue.setCoords(14, 670, 230);
			dialogue.setCoords(15, 580, 110); dialogue.setCoords(16, 730, 200);
			act++;
			break;
		case 1:
			dialogue.setString(3, bio.generate_word());
			dialogue.setString(4, bio.generate_word());
			dialogue.setString(5, bio.generate_word());
			dialogue.setString(6, bio.generate_word());
			dialogue.setString(7, bio.generate_word_common());
			dialogue.setString(8, bio.generate_word_common());
			dialogue.setString(9, bio.generate_word_common());
			dialogue.setString(10, bio.generate_word_common());
			dialogue.setString(11, bio.generate_word_common());
			dialogue.setString(12, bio.generate_word_ending());
			dialogue.setString(13, bio.generate_word_ending());
			dialogue.setString(14, bio.generate_word_ending());
			dialogue.setString(15, bio.generate_word());
			dialogue.setString(16, bio.generate_word_common());
			act++;
			break;
		case 2:
			// run loop
			break;
		case 3:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{
				dialogue.setString(speak, "Now to give it to Lorb");
				act++;
				speech = true;
				advance = false;
			}

			break;
		case 4:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{
				speech = true;
				advance = false;
				act++;
			} break;
		case 5:
			rainingElsewhere.stop();
			rainingElsewhere.free();
			bio.free();
			loopFront = nolanLoop_backstage;
			act = 0;
			bio_achieved = true;
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, speak_x, speak_y);
			break;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}

	void iter_Nolan()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_e:
					for (int i = 0; i < sizeof(overlay.engage) / sizeof(overlay.engage[0]); i++)
					{
						if (overlay.engage[i].engage) // actually have no idea what's going on here
						{
							if (overlay.engage[i].name == "adam")
							{ act++; loopFront = adamLoop; }
							else if (overlay.engage[i].name == "katJess")
							{ act++; loopFront = katJessLoop; }
							else if (overlay.engage[i].name == "fellas")
							{
								act++;
								if (arya_free)
								{ loopFront = fellasLoop_arya; }
								else if (!interactions[interact::fellasInt])
								{ loopFront = fellasLoop; }
								else
								{ loopFront = fellasLoop_after; }
							}
							else if (overlay.engage[i].name == "door1")
							{
								act = 3;
								room = backstage;
								nolan.x = 232;
								nolan.y = 93;
								loopBack = backstageLoop;
								loopFront = nolanLoop_backstage;
							}
							else if (overlay.engage[i].name == "door2")
							{
								if (arya_free)
								{
									act = 3;
									room = backstage;
									nolan.x = 1149;
									nolan.y = 98;
									loopBack = backstageLoop;
									loopFront = nolanLoop_backstage;
								}
								else
								{ act = -2; globalCount = 0; }
							}
							else if (overlay.engage[i].name == "door3")
							{
								act = 3;
								room = backstage;
								nolan.x = 1363;
								nolan.y = 466;
								loopBack = backstageLoop;
								loopFront = nolanLoop_backstage;
							}
							else if (overlay.engage[i].name == "door4")
							{
								act = 3;
								room = backstage;
								nolan.x = 1363;
								nolan.y = 950;
								loopBack = backstageLoop;
								loopFront = nolanLoop_backstage;
							}
							else if (overlay.engage[i].name == "katy")
							{
								act++;
								if (!interactions[interact::katyInt])
								{ loopFront = katyLoop; }
								else
								{ loopFront = katyLoop_after; }
							}
							else if (overlay.engage[i].name == "jo")
							{ act++; loopFront = joLoop; }
							else if (overlay.engage[i].name == "arya")
							{
								if (arya_free) {} // break;
								else if (!tapeMeasure_sprite)
								{ loopFront = aryaLoop_tape; act++; }
								else if (!interactions[interact::aryaInt])
								{ loopFront = aryaLoop; act++; }
								else
								{ loopFront = aryaLoop_after; act++; }
							}
							else if (overlay.engage[i].name == "lauren")
							{
								if (bio_achieved)
								{ loopFront = laurenLoop_bio; act++; }
								else
								{ loopFront = laurenLoop_after; act++; }
							}
							else if (overlay.engage[i].name == "ethan")
							{ 
								if (!interactions[interact::ethanInt])
								{ loopFront = ethanLoop; act++; }
								else
								{ loopFront = ethanLoop_after; act++; }
							}
						}
					}
					break;
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		if (globalCount < 359) // about 6 seconds per loop
		{ globalCount++; }
		else
		{ globalCount = 0; }

		switch (act)
		{
		case -2:
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, speak_x, speak_y);
			dialogue.setString(tag, "Alex");
			dialogue.setString(speak, "Hey, ain't nobody pass 'till the fellas get their toll");
			speech = true;
			act++;
			break;
		case -1:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{ act = 1; advance = false; }

			break;
		case 0:
			overlay.load();
			overlay.Ruins.setChannel(musicChannel);
			overlay.Ruins.play(-1);
			act++;
			break;
		case 1:
			if (state[SDL_SCANCODE_W] || state[SDL_SCANCODE_S] || state[SDL_SCANCODE_A] || state[SDL_SCANCODE_D])
			{
				if (nolan.count < 59)
				{ nolan.count++; }
				else
				{ nolan.count = 0; }
			}

			if (state[SDL_SCANCODE_W]) { nolan.move(0, -1, room); }
			else if (state[SDL_SCANCODE_S]) { nolan.move(0, 1, room); }

			if (state[SDL_SCANCODE_A]) { nolan.move(-1, 0, room); }
			else if (state[SDL_SCANCODE_D]) { nolan.move(1, 0, room); }

			for (int i = 0; i < sizeof(overlay.sprite) / sizeof(overlay.sprite[0]); i++)
			{ overlay.sprite[i].setLayer(nolan.y + nolan.CHAR_HEIGHT); }

			for (int i = 0; i < sizeof(overlay.curtain) / sizeof(overlay.curtain[0]); i++)
			{ overlay.curtain[i].setLayer_curtain(nolan.x, nolan.y, nolan.CHAR_WIDTH, nolan.CHAR_HEIGHT); }

			if (overlay.curtain[2].front)
			{ overlay.sprite[overlay.index::ethan].front = true; }
			else 
			{ overlay.sprite[overlay.index::ethan].front = false; }

			for (int i = 0; i < sizeof(overlay.engage) / sizeof(overlay.engage[0]); i++)
			{
				if (inRange(overlay.engage[i].range))
				{ overlay.engage[i].engage = true; }
				else
				{ overlay.engage[i].engage = false; }
			}

			break;
		case 2:
			act = 0;
			break;
		case 3:
			act = 0;
			overlay.free();
			overlay.Ruins.stop();
			overlay.Ruins.free();
			break;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}

	void iter_Nolan_backstage()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_e:
					for (int i = 0; i < sizeof(back.engage) / sizeof(back.engage[0]); i++)
					{
						if (back.engage[i].engage)
						{
							if (back.engage[i].name == "fridge")
							{ act = -1; }
							else if (back.engage[i].name == "cabinet")
							{ act = -3; }
							else if (back.engage[i].name == "tape")
							{ if (tapeMeasure_sprite) { act = -7; } }
							else if (back.engage[i].name == "door5")
							{ act = -11; }
							else if (back.engage[i].name == "door6")
							{
								act = 3;
								room = theatre;
								nolan.x = 176;
								nolan.y = 15;
								loopBack = otherLoop;
								loopFront = nolanLoop;
							}
							else if (back.engage[i].name == "door7")
							{ act = -11; }
							else if (back.engage[i].name == "door8")
							{
								act = 3;
								room = theatre;
								nolan.x = 1583;
								nolan.y = 15;
								loopBack = otherLoop;
								loopFront = nolanLoop;
							}
							else if (back.engage[i].name == "door9")
							{ act = -11; }
							else if (back.engage[i].name == "door10")
							{ act = -13; }
							else if (back.engage[i].name == "door11")
							{ act = -11; }
							else if (back.engage[i].name == "door12")
							{
								act = 3;
								room = theatre;
								nolan.x = 1458;
								nolan.y = 308;
								loopBack = otherLoop;
								loopFront = nolanLoop;
							}
							else if (back.engage[i].name == "door13")
							{
								act = 3;
								room = maleDressingRoom;
								nolan.x = 176;
								nolan.y = 240;
								loopBack = maleDressingRoomLoop;
								loopFront = nolanLoop_maleDressingRoom;
							}
							else if (back.engage[i].name == "door14")
							{
								act = 3;
								room = theatre;
								nolan.x = 1000;
								nolan.y = 50;
								loopBack = otherLoop;
								loopFront = nolanLoop;
							}
							else if (back.engage[i].name == "laptop")
							{ act = -15; }
							else if (back.engage[i].name == "rachel")
							{ act = -17; }
						}
					}
					break;
				case SDLK_a:
					if (act < 0)
						choice = 0;
					break;
				case SDLK_d:
					if (act < 0)
						choice = 1;
					break;
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		if (globalCount < 359) // about 6 seconds per loop
		{ globalCount++; }
		else
		{ globalCount = 0; }

		switch (act)
		{
		case -18:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{ act = 1; advance = false; }

			break;
		case -17:
			dialogue.setString(tag, "Rachel");
			dialogue.setString(speak, "Hey, you left your computer here");
			dialogue.setCoords(speak, speak_x, speak_y);
			advance = false;
			speech = true;
			act--;

			break;
		case -16:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{
				back.Ruins.stop();
				back.Ruins.free();
				act = 0; advance = false;
				loopFront = bioLoop;
				back.engage[13].setRange(0, 0, 0, 0);
			}

			break;
		case -15:
			dialogue.setString(tag, "");
			dialogue.setString(speak, "Oh, finally. Now you can write your bio for Lorb");
			dialogue.setCoords(speak, speak_x, speak_y);
			advance = false;
			speech = true;
			act--;

			break;
		case -14:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{ act = 1; advance = false; }

			break;
		case -13:
			dialogue.setString(tag, "");
			dialogue.setString(speak, "Prolly shouldn't go in there");
			dialogue.setCoords(speak, speak_x, speak_y);
			advance = false;
			speech = true;
			act--;

			break;
		case -12:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{ act = 1; advance = false; }

			break;
		case -11:
			dialogue.setString(tag, "");
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setString(speak, "Locked");
			dialogue.setCoords(speak, speak_x, speak_y);
			advance = false;
			speech = true;
			act--;

			break;
		case -10:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{ act = 1; advance = false; tapeMeasure_sprite = false; back.engage[2].setRange(0, 0, 0, 0); }

			break;
		case -9:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{
				dialogue.setString(speak, "You took the tape measure");
				act--;
				speech = true;
				advance = false;
			}

			break;
		case -8:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{
				if (interactions[tapeInt])
				{
					dialogue.setString(speak, "Sofia prolly needs that");
					act--;
					speech = true;
					advance = false;
				}
				else
				{ act = 1; advance = false; }
			}

			break;
		case -7:
			dialogue.setString(tag, "");
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setString(speak, "A tape measure");
			dialogue.setCoords(speak, speak_x, speak_y);
			advance = false;
			speech = true;
			act--;

			break;
		case -6:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{ act = 1; advance = false; interactions[cabinetInt] = true; }

			break;
		case -5:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (!speech)
			{
				dialogue.type(choice1, false);
				dialogue.type(choice2, false);
			}

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{
				dialogue.setString(speak, "You took a snazzy cane and top hat");
				act--;
				speech = true;
				advance = false;
			}

			break;
		case -4:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{
				if (interactions[cabinetInt])
				{
					act = 1; advance = false;
				}
				else
				{
					dialogue.setString(speak, "Take one?");
					dialogue.setString(choice1, "Yes");
					dialogue.setString(choice2, "No");
					act--;
					speech = true;
					advance = false;
				}
			}

			break;
		case -3:
			dialogue.setString(tag, "");
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setString(speak, "A cabinet full of props");
			dialogue.setCoords(speak, speak_x, speak_y);
			dialogue.setCoords(choice1, choice1_x, choice1_y);
			dialogue.setCoords(choice2, choice2_x, choice2_y);
			advance = false;
			speech = true;
			act--;
			break;
		case -2:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{ act = 1; advance = false; }

			break;
		case -1:
			dialogue.setString(tag, "");
			dialogue.setString(speak, "It's a fridge");
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, speak_x, speak_y);
			advance = false;
			speech = true;
			act--;
			break;
		case 0:
			back.load();
			back.Ruins.setChannel(musicChannel);
			back.Ruins.play(-1);
			act++;
			break;
		case 1:
			if (state[SDL_SCANCODE_W] || state[SDL_SCANCODE_S] || state[SDL_SCANCODE_A] || state[SDL_SCANCODE_D])
			{
				if (nolan.count < 59)
				{ nolan.count++; }
				else
				{ nolan.count = 0; }
			}

			if (state[SDL_SCANCODE_W]) { nolan.move(0, -1, room); }
			else if (state[SDL_SCANCODE_S]) { nolan.move(0, 1, room); }

			if (state[SDL_SCANCODE_A]) { nolan.move(-1, 0, room); }
			else if (state[SDL_SCANCODE_D]) { nolan.move(1, 0, room); }

			for (int i = 0; i < sizeof(back.engage) / sizeof(back.engage[0]); i++)
			{
				if (inRange(back.engage[i].range))
				{ back.engage[i].engage = true; }
				else
				{ back.engage[i].engage = false; }
			}

			break;
		case 2:
			act = 0;
			break;
		case 3:
			back.free();
			back.Ruins.stop();
			back.Ruins.free();
			act = 0;
			break;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}

	void iter_Nolan_maleDressingRoom()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_e:
					for (int i = 0; i < sizeof(male.engage) / sizeof(male.engage[0]); i++)
					{
						if (male.engage[i].engage)
						{
							if (male.engage[i].name == "door15")
							{
								act = 3;
								room = backstage;
								loopBack = backstageLoop;
								loopFront = nolanLoop_backstage;
								nolan.x = 1531;
								nolan.y = 526;
							}
							else if (male.engage[i].name == "door16")
							{ act = -1; }
						}
					}
					break;
				case SDLK_a:
					if (act < 0)
						choice = 0;
					break;
				case SDLK_d:
					if (act < 0)
						choice = 1;
					break;
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		if (globalCount < 359) // about 6 seconds per loop
		{ globalCount++; }
		else
		{ globalCount = 0; }

		switch (act)
		{
		case -5:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{ act = 1; advance = false; }

			break;
		case -4:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{
				dialogue.setString(speak, "Oh...");
				act--;
				speech = true;
				advance = false;
			}

			break;
		case -3:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{
				dialogue.setString(speak, "Well, because Nolan was too lazy to draw a bathroom and there prolly/wouldn't be anything too interesting in there anyways");
				act--;
				speech = true;
				advance = false;
			}

			break;
		case -2:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{
				dialogue.setString(speak, "Why not?");
				act--;
				speech = true;
				advance = false;
			}

			break;
		case -1:
			dialogue.setString(tag, "");
			dialogue.setString(speak, "You can't go in there");
			dialogue.setCoords(speak, speak_x, speak_y);
			advance = false;
			speech = true;
			act--;

			break;
		case 0:
			male.load();
			act++;
			break;
		case 1:
			if (state[SDL_SCANCODE_W] || state[SDL_SCANCODE_S] || state[SDL_SCANCODE_A] || state[SDL_SCANCODE_D])
			{
				if (nolan.count < 59)
				{ nolan.count++; }
				else
				{ nolan.count = 0; }
			}

			if (state[SDL_SCANCODE_W]) { nolan.move(0, -1, room); }
			else if (state[SDL_SCANCODE_S]) { nolan.move(0, 1, room); }

			if (state[SDL_SCANCODE_A]) { nolan.move(-1, 0, room); }
			else if (state[SDL_SCANCODE_D]) { nolan.move(1, 0, room); }

			for (int i = 0; i < sizeof(male.engage) / sizeof(male.engage[0]); i++)
			{
				if (inRange(male.engage[i].range))
				{ male.engage[i].engage = true; }
				else
				{ male.engage[i].engage = false; }
			}

			break;
		case 2:
			act = 0;
			break;
		case 3:
			male.free();
			act = 0;
			break;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}

	void iter_Lauren()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		lauren.Lauren.render(char_x, char_y);
		dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
		dialogue.type(tag, false);

		if (dialogue.type(speak, speech))
		{ speech = false; }

		switch (act)
		{
		case 0:
			lauren.load();
			dialogue.setString(tag, "Lauren");
			dialogue.setString(speak, "For example your program bio/*cough* *cough* due by the end of class *cough*");
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, speak_x, speak_y);
			advance = false;
			act++;
			break;
		case 1:
			if (advance)
			{
				act++;
				advance = false;
			} break;
		case 2:
			lauren.free();
			overlay.sprite[overlay.index::sofiaElla].display = false;
			overlay.sprite[overlay.index::fellas_all].display = false;
			loopFront = nolanLoop;
			act = 0;
			break;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}

	void iter_Lauren_after()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		lauren.Lauren.render(char_x, char_y);

		dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
		dialogue.type(tag, false);

		if (dialogue.type(speak, speech))
		{ speech = false; }

		switch (act)
		{
		case 0:
			lauren.load();
			act++;
			break;
		case 1:
			dialogue.setString(tag, "Lauren");
			dialogue.setString(speak, "Have you finished you bio yet?");
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, speak_x, speak_y);
			speech = true;
			advance = false;
			act++;
			break;
		case 2:
			if (advance)
			{
				dialogue.setString(speak, "Well I need it by the end of class");
				act++;
				speech = true;
				advance = false;
			} break;
		case 3:
			if (advance)
			{
				dialogue.setString(speak, "Your computer? I don't know, check the couch backstage");
				speech = true;
				advance = false;
				act++;
			} break;
		case 4:
			if (advance)
			{
				speech = true;
				advance = false;
				act++;
			} break;
		case 5:
			lauren.free();
			loopFront = nolanLoop;
			act = 1;
			break;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}

	void iter_Lauren_bio()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		lauren.Lauren.render(char_x, char_y);

		dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
		dialogue.type(tag, false);

		if (dialogue.type(speak, speech))
		{ speech = false; }

		switch (act)
		{
		case 0:
			lauren.load();
			act++;
			break;
		case 1:
			dialogue.setString(tag, "Lauren");
			dialogue.setString(speak, "So I've been reading your bio...");
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, speak_x, speak_y);
			dialogue.stop();
			speech = true;
			advance = false;
			act++;
			break;
		case 2:
			if (advance)
			{
				dialogue.setString(speak, "It's...");
				act++;
				speech = true;
				advance = false;
			} break;
		case 3:
			if (advance)
			{
				dialogue.setString(speak, "Y'know, class is almost over");
				speech = true;
				advance = false;
				act++;
			} break;
		case 4:
			if (advance)
			{
				dialogue.setString(speak, "But thanks for submitting on time *cough* *cough* Michael *cough*");
				speech = true;
				advance = false;
				act++;
			} break;
		case 5:
			if (advance)
			{
				dialogue.setString(speak, "");
				speech = true;
				advance = false;
				lauren.fade.setAlpha(0);
				act++;
			} break;
		case 6:
			lauren.fade.render(0, 0);
			if (lauren.alpha < 255)
			{ lauren.fade.setAlpha(lauren.alpha); lauren.alpha++; }
			else
			{ act++; }
			break;
		case 7:
			lauren.free();
			quit = true;
			act = 0;
			overlay.Ruins.stop();
			overlay.Ruins.free();
			overlay.free();
			Mix_FreeChunk(dialogue.audio);

			#ifdef __EMSCRIPTEN__
				printf("at cancel loop()\n");
				emscripten_cancel_main_loop();
				toTech1();
			#endif

			break;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}

	void iter_Adam()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		if (adam.front)
		{
			adam.Nicole.render(char_x + adam.nicole_x, char_y);
			adam.Adam.render(char_x + adam.adam_x, char_y);
		}
		else
		{
			adam.Adam.render(char_x + adam.adam_x, char_y);
			adam.Nicole.render(char_x + adam.nicole_x, char_y);
		}

		dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
		dialogue.type(tag, false);

		if (dialogue.type(speak, speech))
		{ speech = false; }

		switch (act)
		{
		case 0:
			adam.load();
			act++;
			break;
		case 1:
			dialogue.setString(tag, "Adam");
			dialogue.setString(speak, "Baby? What?! You mean...? Hey... Mary.../Mary! You mean you're... you're on the nest?");
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, speak_x, speak_y);
			speech = true;
			advance = false;
			act++;
			break;
		case 2:
			if (advance)
			{
				act++;
				adam.front = 0;
				advance = false;
			} break;
		case 3:
			if (adam.adam_x > -100)
			{ adam.adam_x -= 3; adam.nicole_x -= 16; }
			else
			{ act++; speech = true; }
			break;
		case 4:
			dialogue.setString(tag, "Nicole");
			dialogue.setString(speak, "Be-gaaaaaawk!1?!!?!?1!");
			act++;
			break;
		case 5:
			if (advance)
			{
				dialogue.setString(tag, "Adam");
				dialogue.setString(speak, "Oh, hey Nolan!");
				adam.front = 1;
				speech = true;
				advance = false;
				act++;
			} break;
		case 6:
			if (advance)
			{
				speech = true;
				advance = false;
				act++;
			} break;
		case 7:
			adam.free();
			loopFront = nolanLoop;
			act = 1;
			break;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}

	void iter_katJess()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		if (katJess.front)
		{
			katJess.Katherine.render(char_x + katJess.katherine_x, char_y);
			katJess.Jessica.render(char_x + katJess.jessica_x, char_y);
		}
		else
		{
			katJess.Jessica.render(char_x + katJess.jessica_x, char_y);
			katJess.Katherine.render(char_x + katJess.katherine_x, char_y);
		}

		dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
		dialogue.type(tag, false);

		if (dialogue.type(speak, speech))
		{ speech = false; }

		switch (act)
		{
		case 0:
			katJess.load();
			act++;
			break;
		case 1:
			dialogue.setString(tag, "Katherine");
			dialogue.setString(speak, "");
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, speak_x, speak_y);
			speech = true;
			advance = false;
			act++;
			break;
		case 2:
			if (advance)
			{
				act++;
				speech = true;
				advance = false;
			} break;
		case 7:
			katJess.free();
			loopFront = nolanLoop;
			act = 1;
			break;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}

	void iter_Fellas()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		fellas.Justin.render(char_x + fellas.justin_x, char_y);
		fellas.Doran.render(char_x + fellas.doran_x, char_y);
		fellas.Alex.render(char_x + fellas.alex_x, char_y);

		dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
		dialogue.type(tag, false);

		if (dialogue.type(speak, speech))
		{ speech = false; }

		switch (act)
		{
		case 0:
			fellas.load();
			dialogue.setString(tag, "Alex");
			dialogue.setString(speak, "Hey, where do ya think you're goin'?");
			speech = true;
			advance = false;
			fellas.alex_x = -100;
			fellas.doran_x = 50;
			fellas.justin_x = 150;
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, speak_x, speak_y);
			act++;
			break;
		case 1:
			if (advance)
			{
				dialogue.setString(speak, "Ain't nobody passes the fellas 'till the toll is paid");
				advance = false;
				speech = true;
				act++;
			} break;
		case 2:
			if (advance)
			{
				dialogue.setString(tag, "Justin");
				dialogue.setString(speak, "Hey, where's Arya?");
				fellas.front = 1;
				speech = true;
				advance = false;
				act++;
			} break;
		case 3:
			if (advance)
			{
				dialogue.setString(tag, "Doran");
				dialogue.setString(speak, "Yo, we missin' a fella!?");
				fellas.front = 2;
				speech = true;
				advance = false;
				act++;
			} break;
		case 4:
			if (advance)
			{
				dialogue.setString(tag, "Alex");
				dialogue.setString(speak, "Yeah... Hey, I think we missin' a fella!!");
				speech = true;
				advance = false;
				act++;
			} break;
		case 5:
			if (advance)
			{
				dialogue.setString(speak, "Hey, tellya what. If you can find Arya we'll let ya pass. Deal?");
				speech = true;
				advance = false;
				act++;
			} break;
		case 6:
			if (advance)
			{
				advance = false;
				act++;
			} break;
		case 7:
			fellas.free();
			interactions[interact::fellasInt] = true;
			loopFront = nolanLoop;
			act = 1;
			break;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}

	void iter_Fellas_after()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		fellas.Justin.render(char_x + fellas.justin_x, char_y);
		fellas.Alex.render(char_x + fellas.alex_x, char_y);

		dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
		dialogue.type(tag, false);

		if (dialogue.type(speak, speech))
		{ speech = false; }

		switch (act)
		{
		case 0:
			fellas.load();
			dialogue.setString(tag, "Alex");
			dialogue.setString(speak, "Just find Arya");
			speech = true;
			advance = false;
			fellas.alex_x = -100;
			fellas.justin_x = 50;
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, speak_x, speak_y);
			act++;
			break;
		case 1:
			if (advance)
			{
				advance = false;
				act++;
			} break;
		case 2:
			fellas.free();
			loopFront = nolanLoop;
			act = 1;
			break;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}

	void iter_Fellas_arya()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		fellas.Arya.render(char_x + fellas.arya_x, char_y);
		fellas.Justin.render(char_x + fellas.justin_x, char_y);
		fellas.Alex.render(char_x + fellas.alex_x, char_y);

		dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
		dialogue.type(tag, false);

		if (dialogue.type(speak, speech))
		{ speech = false; }

		switch (act)
		{
		case 0:
			fellas.load();
			dialogue.setString(tag, "Alex");
			dialogue.setString(speak, "Hey, thanks Nolan!");
			speech = true;
			advance = false;
			fellas.alex_x = -100;
			fellas.justin_x = 50;
			fellas.arya_x = 150;
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, speak_x, speak_y);
			act++;
			break;
		case 1:
			if (advance)
			{
				dialogue.setString(speak, "Now the fellas are complete");
				advance = false;
				speech = true;
				act++;
			} break;
		case 2:
			if (advance)
			{
				advance = false;
				act++;
			} break;
		case 3:
			fellas.free();
			loopFront = nolanLoop;
			act = 1;
			break;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}

	void iter_Katy()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		if (katy.front)
		{
			katy.Katy.render(char_x + katy.katy_x, char_y);
			katy.Sophia.render(char_x + katy.sophia_x, char_y);
		}
		else
		{
			katy.Sophia.render(char_x + katy.sophia_x, char_y);
			katy.Katy.render(char_x + katy.katy_x, char_y);
		}

		dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
		dialogue.type(tag, false);

		if (dialogue.type(speak, speech, katy.size))
		{ speech = false; }

		switch (act)
		{
		case 0:
			katy.load();
			dialogue.setString(tag, "Katy");
			dialogue.setString(speak, "Yeehaw it's Katy!");
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, speak_x, speak_y);
			speech = true;
			advance = false;
			act++;
			break;
		case 1:
			if (advance)
			{
				dialogue.setString(speak, "...");
				advance = false;
				speech = true;
				act++;
			} break;
		case 2:
			if (advance)
			{
				dialogue.setString(speak, "I said... YEEHAW IT'S KATY!!!");
				speech = true;
				advance = false;
				act++;
			} break;
		case 3:
			if (advance)
			{
				dialogue.setString(speak, "...");
				speech = true;
				advance = false;
				act++;
			} break;
		case 4:
			if (advance)
			{
				dialogue.setString(speak, "YEEHAW IT'S KATY!!!1!");
				katy.size = 4;
				katy.front = 1;
				speech = true;
				advance = false;
				act++;
			} break;
		case 5:
			if (advance)
			{
				if (katy.sophia_x > 150)
				{
					katy.sophia_x -= 12;
					katy.katy_x -= 3;
				}
				else
				{
					dialogue.setString(tag, "Sophia");
					dialogue.setString(speak, "umm...");
					katy.size = 1;
					speech = true;
					advance = false;
					act++;
				}
			} break;
		case 6:
			if (advance)
			{
				dialogue.setString(speak, "wtf katy?");
				speech = true;
				advance = false;
				act++;
			} break;
		case 7:
			if (advance)
			{
				dialogue.setString(tag, "Katy");
				dialogue.setString(speak, "weak");
				katy.front = 0;
				speech = true;
				advance = false;
				act++;
			} break;
		case 8:
			if (advance)
			{
				dialogue.setString(tag, "Sophia");
				dialogue.setString(speak, "Excuse me?!");
				katy.front = 1;
				speech = true;
				advance = false;
				act++;
			} break;
		case 9:
			if (advance)
			{
				dialogue.setString(tag, "Katy");
				dialogue.setString(speak, "WEAK!");
				katy.front = 0;
				speech = true;
				advance = false;
				act++;
			} break;
		case 10:
			if (advance)
			{
				dialogue.setString(tag, "Sophia");
				dialogue.setString(speak, "Yee-frickin'-haw!");
				katy.front = 1;
				speech = true;
				advance = false;
				act++;
			} break;
		case 11:
			if (katy.katy_x > -150)
			{ katy.katy_x -= 10; }
			else
			{ act++; advance = false; }
		case 12:
			if (katy.sophia_x > -800)
			{
				katy.sophia_x -= 14;
				katy.katy_x -= 10;
			}
			else
			{
				if (advance)
				{ act++; }
			}
		case 13:
			if (advance)
			{
				advance = false;
				act++;
			} break;
		case 14:
			katy.free();
			interactions[interact::katyInt] = true;
			loopFront = nolanLoop;
			act = 1;
			break;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}

	void iter_Katy_after()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		if (katy.front)
		{
			katy.Katy.render(char_x + katy.katy_x, char_y);
			katy.Sophia.render(char_x + katy.sophia_x, char_y);
		}
		else
		{
			katy.Sophia.render(char_x + katy.sophia_x, char_y);
			katy.Katy.render(char_x + katy.katy_x, char_y);
		}

		dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
		dialogue.type(tag, false);

		if (dialogue.type(speak, speech, katy.size))
		{ speech = false; }

		switch (act)
		{
		case 0:
			katy.load();
			dialogue.setString(tag, "Sophia");
			dialogue.setString(speak, "What does a Nolan eat for breakfast?");
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, speak_x, speak_y);
			katy.katy_x = -100;
			katy.sophia_x = 100;
			katy.front = 1;
			speech = true;
			advance = false;
			act++;
			break;
		case 1:
			if (advance)
			{
				dialogue.setString(speak, "...");
				advance = false;
				speech = true;
				act++;
			} break;
		case 2:
			if (advance)
			{
				dialogue.setString(speak, "GraNolan!");
				advance = false;
				speech = true;
				act++;
			} break;
		case 3:
			if (advance)
			{
				dialogue.setString(tag, "Katy");
				dialogue.setString(speak, "...");
				katy.front = 0;
				advance = false;
				speech = true;
				act++;
			} break;
		case 4:
			if (advance)
			{
				dialogue.setString(tag, "Sophia");
				dialogue.setString(speak, "Sorry, I'll leave now");
				katy.front = 1;
				advance = false;
				speech = true;
				act++;
			} break;
		case 5:
			if (advance)
			{
				act++;
				advance = false;
				speech = true;
			} break;
		case 6:
			katy.free();
			loopFront = nolanLoop;
			act = 1;
			break;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}

	void iter_Jo()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		jo.Jo.render(char_x, char_y);

		dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
		dialogue.type(tag, false);

		if (dialogue.type(speak, speech))
		{ speech = false; }

		switch (act)
		{
		case 0:
			jo.load();
			act++;
			break;
		case 1:
			dialogue.setString(tag, "Jo");
			dialogue.setString(speak, "Nolan!");
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, speak_x, speak_y);
			speech = true;
			advance = false;
			act++;
			break;
		case 2:
			if (advance)
			{
				act++;
				advance = false;
			} break;
		case 3:
			jo.free();
			loopFront = nolanLoop;
			act = 1;
			break;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}

	void iter_Arya()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		if (!arya.front)
		{
			arya.Sofia.render(char_x + 100, char_y);
			arya.Arya.render(char_x - 100, char_y);
		}
		else
		{
			arya.Arya.render(char_x - 100, char_y);
			arya.Sofia.render(char_x + 100, char_y);
		}

		dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
		dialogue.type(tag, false);

		if (dialogue.type(speak, speech))
		{ speech = false; }

		switch (act)
		{
		case 0:
			arya.load();
			act++;
			break;
		case 1:
			dialogue.setString(tag, "Arya");
			dialogue.setString(speak, "Help me!");
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, speak_x, speak_y);
			speech = true;
			advance = false;
			act++;
			break;
		case 2:
			if (advance)
			{
				dialogue.setString(tag, "Sofia");
				dialogue.setString(speak, "Nope, not until you're measured");
				arya.front = 1;
				act++;
				speech = true;
				advance = false;
			} break;
		case 3:
			if (advance)
			{
				dialogue.setString(speak, "I think I left the other measuring tape backstage...");
				act++;
				speech = true;
				advance = false;
			} break;
		case 4:
			if (advance)
			{
				dialogue.setString(speak, "Now stay still and stop being so tall");
				act++;
				speech = true;
				advance = false;
			} break;
		case 5:
			if (advance)
			{
				act++;
				advance = false;
			} break;
		case 6:
			arya.free();
			loopFront = nolanLoop;
			interactions[interact::aryaInt] = true;
			interactions[interact::tapeInt] = true;
			act = 1;
			break;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}

	void iter_Arya_after()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		arya.Arya.render(char_x - 100, char_y);
		arya.Sofia.render(char_x + 100, char_y);

		dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
		dialogue.type(tag, false);

		if (dialogue.type(speak, speech))
		{ speech = false; }

		switch (act)
		{
		case 0:
			arya.load();
			act++;
			break;
		case 1:
			dialogue.setString(tag, "Sofia");
			dialogue.setString(speak, "You'll be free when I figure out how to measure the other kilometer of your leg");
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, speak_x, speak_y);
			speech = true;
			advance = false;
			act++;
			break;
		case 2:
			if (advance)
			{
				dialogue.setString(speak, "I think I left the other measuring tape backstage...");
				act++;
				speech = true;
				advance = false;
			} break;
		case 3:
			if (advance)
			{
				act++;
				advance = false;
			} break;
		case 4:
			arya.free();
			loopFront = nolanLoop;
			act = 1;
			break;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}

	void iter_Arya_tape()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		if (!arya.front)
		{
			arya.Sofia.render(char_x + 100, char_y);
			arya.Arya.render(char_x - 100 + arya.arya_x, char_y);
		}
		else
		{
			arya.Arya.render(char_x - 100 + arya.arya_x, char_y);
			arya.Sofia.render(char_x + 100, char_y);
		}

		dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
		dialogue.type(tag, false);

		if (dialogue.type(speak, speech))
		{ speech = false; }

		switch (act)
		{
		case 0:
			arya.load();
			act++;
			break;
		case 1:
			arya.front = 1;
			dialogue.setString(tag, "Sofia");
			dialogue.setString(speak, "Ah, you found the tape, thanks!");
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, speak_x, speak_y);
			speech = true;
			advance = false;
			act++;
			break;
		case 2:
			if (advance)
			{
				dialogue.setString(speak, "You're free to go");
				act++;
				speech = true;
				advance = false;
			} break;
		case 3:
			if (advance)
			{
				arya.front = 0;
				dialogue.setString(tag, "Arya");
				dialogue.setString(speak, "Aaah, freedom!");
				act++;
				speech = true;
				advance = false;
			} break;
		case 4:
			if (arya.arya_x > -600)
			{ arya.arya_x -= 6; }
			else
			{ act++; advance = false; } break;
		case 5:
			if (advance)
			{ act++; advance = false; } break;
		case 6:
			arya.free();
			arya_free = true;
			overlay.sprite[overlay.index::arya].display = false;
			overlay.sprite[overlay.index::fellas].display = false;
			overlay.sprite[overlay.index::sofiaElla].display = true;
			overlay.sprite[overlay.index::fellas_all].display = true;
			loopFront = nolanLoop;
			act = 1;
			break;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}

	void iter_Ethan()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		ethan.Ethan.render(char_x, char_y);

		dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
		dialogue.type(tag, false);

		if (dialogue.type(speak, speech))
		{ speech = false; }

		switch (act)
		{
		case 0:
			ethan.load();
			dialogue.setString(tag, "Ethan");
			dialogue.setString(speak, "Oh, uh... George!");
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, speak_x, speak_y);
			speech = true;
			advance = false;
			act++;
			break;
		case 1:
			if (advance)
			{
				dialogue.setString(speak, "Uhh... George your dad is dead..");
				advance = false;
				speech = true;
				act++;
			} break;
		case 2:
			if (advance)
			{
				dialogue.setString(speak, "Uhh... George I lost the eight thousand dollars");
				speech = true;
				advance = false;
				act++;
			} break;
		case 3:
			if (advance)
			{
				dialogue.setString(speak, "Oh, uhh... George, I'm no good to you!");
				speech = true;
				advance = false;
				act++;
			} break;
		case 4:
			if (advance)
			{
				dialogue.setString(speak, "I'm sorry Goerge, I spent all the money on horses and strippers");
				speech = true;
				advance = false;
				act++;
			} break;
		case 5:
			if (advance)
			{
				dialogue.setString(speak, "George I killed your dad too...");
				speech = true;
				advance = false;
				act++;
			} break;
		case 6:
			if (advance)
			{
				dialogue.setString(speak, "Uhh... George, I told the board to elect you and gave all your college money to Harry...");
				speech = true;
				advance = false;
				act++;
			} break;
		case 7:
			if (advance)
			{
				dialogue.setString(speak, "Oh, I'm sorry George!");
				speech = true;
				advance = false;
				act++;
			} break;
		case 8:
			ethan.free();
			interactions[interact::ethanInt] = true;
			loopFront = nolanLoop;
			act = 1;
			break;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}

	void iter_Ethan_after()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		ethan.Ethan.render(char_x, char_y);

		dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
		dialogue.type(tag, false);

		if (dialogue.type(speak, speech))
		{ speech = false; }

		switch (act)
		{
		case 0:
			ethan.load();
			dialogue.setString(tag, "Ethan");
			dialogue.setString(speak, "George, your dad's had a stroke!!");
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, speak_x, speak_y);
			speech = true;
			advance = false;
			act++;
			break;
		case 1:
			if (advance)
			{
				advance = false;
				speech = true;
				act++;
			} break;
		case 2:
			ethan.free();
			interactions[interact::ethanInt] = true;
			loopFront = nolanLoop;
			act = 1;
			break;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}
};

class Tech1
{
public:
	Tech1() { }
	~Tech1() { free(); }

	Uint32 startTime = 0;
	Uint32 endTime = 0;
	Uint32 delta = 0;
	short fps = 60;
	short timePerFrame = 1000 / 60; // miliseconds

	Sprite dialogueBox;
	Sprite fade;

	Text dialogue;

	SDL_Event event;

	int act = 0, front = -1, front_left = -1, alpha = 0, size = 1;

	const int char_x = (SCREEN_WIDTH - 720) / 2, char_y = SCREEN_HEIGHT - 630;

	bool quit = false, speech = true, advance = false;

	enum character
	{ adam, oscar, alex, doran, serena, kyleigh, maayan, ethan, nicole, justin };

	Sprite sprite[10];
	
	bool blit[10];

	int pos[10];

	void load()
	{
		dialogue.load("Assets/text.png", "Assets/Audio/textClip_und.wav");
		dialogueBox.loadFromFile("Assets/dialogue.png");
		fade.loadFromFile("Assets/fade.png");
		sprite[adam].loadFromFile("Assets/Characters/Adam.png");
		sprite[oscar].loadFromFile("Assets/Characters/Oscar.png");
		sprite[alex].loadFromFile("Assets/Characters/Alex.png");
		sprite[doran].loadFromFile("Assets/Characters/Doran.png");
		sprite[serena].loadFromFile("Assets/Characters/Serena.png");
		sprite[kyleigh].loadFromFile("Assets/Characters/Kyleigh.png");
		sprite[maayan].loadFromFile("Assets/Characters/Bichael.png");
		sprite[ethan].loadFromFile("Assets/Characters/Ethan.png");
		sprite[nicole].loadFromFile("Assets/Characters/Nicole.png");
		sprite[justin].loadFromFile("Assets/Characters/Justin.png");

		for (int i = 0; i < sizeof(blit) / sizeof(blit[0]); i++)
		{ blit[i] = false; }

		for (int i = 0; i < sizeof(pos) / sizeof(pos[0]); i++)
		{ pos[i] = 0; }
	}

	void free()
	{
		dialogue.free();
		dialogueBox.free();
		fade.free();

		for (int i = 0; i < sizeof(sprite) / sizeof(sprite[0]); i++)
		{ sprite[i].free(); }
	}

	void loopinTime()
	{
		#ifdef __EMSCRIPTEN__
			printf("at looopinTime()\n");
			emscripten_set_main_loop_arg(&callbackTech1, this, 120, 1);
			printf("main loop set\n");
		#else

		while (!quit)
		{
			if (!startTime)
				startTime = SDL_GetTicks();
			else
				delta = endTime - startTime;

			if (delta < timePerFrame)
				SDL_Delay(timePerFrame - delta);
			if (delta > timePerFrame)
				fps = 1000 / delta;

			iter();

			startTime = endTime;
			endTime = SDL_GetTicks();
		}
		#endif
	}

	void iter()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_RenderClear(renderer);

		for (int i = 0; i < sizeof(sprite) / sizeof(sprite[0]); i++)
		{ if (blit[i]) { sprite[i].render(char_x + pos[i], char_y); } }

		if (front != -1) { sprite[front].render(char_x + pos[front], char_y); }

		if (front_left != -1) { sprite[front_left].render(char_x + pos[front_left], char_y); }

		if (act > 1)
		{ dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight()); }
		dialogue.type(tag, false);

		if (dialogue.type(speak, speech, size))
		{ speech = false; }

		switch (act)
		{
		case 0:
			load();
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, 280, 300);
			dialogue.setString(tag, "");
			dialogue.setString(speak, "November 18 Tech Week");
			size = 2;
			speech = true;
			advance = false;
			act++;
			break;
		case 1:
			if (advance)
			{
				dialogue.setCoords(speak, speak_x, speak_y);
				dialogue.setString(tag, "Rachel");
				dialogue.setString(speak, "AAAAAAAAAAAAHHHHHHHHHH!!!!");
				size = 1;
				speech = true;
				advance = false;
				act++;
			} break;
		case 2:
			if (advance)
			{
				dialogue.setString(tag, "");
				dialogue.setString(speak, "Yep, tech week");
				speech = true;
				advance = false;
				act++;
			} break;
		case 3:
			if (advance)
			{
				dialogue.setString(tag, "Adam");
				dialogue.setString(speak,
					"Okay. Okay. Hold on, now! Please! Now now, please, everybody!/Don't panic! I know you want your money.");
				blit[adam] = true;
				pos[adam] = -300;
				speech = true;
				advance = false;
				act++;
			} break;
		case 4:
			if (advance)
			{
				blit[oscar] = true;
				pos[oscar] = 350;
				blit[alex] = true;
				pos[alex] = 200;
				blit[doran] = true;
				pos[doran] = 350;
				blit[serena] = true;
				pos[serena] = 250;
				blit[kyleigh] = true;
				pos[kyleigh] = 300;
				dialogue.setString(tag, "all");
				dialogue.setString(speak, "Yeah, we want our money George! Where's our money?!?!?");
				speech = true;
				advance = false;
				act++;
			} break;
		case 5:
			if (advance)
			{
				dialogue.setString(tag, "Doran");
				dialogue.setString(speak, "We want our money George! Where's our money!?");
				front = doran;
				speech = true;
				advance = false;
				act++;
			} break;
		case 6:
			if (pos[doran] > 100)
  			{ pos[doran] -= 7; }
			else if (advance)
			{
				dialogue.setString(tag, "Adam");
				dialogue.setString(speak, "Just a minute! Let me explain, your money isn't here, no. It's in people's houses.");
				front_left = adam;
				speech = true;
				advance = false;
				act++;
			} break;
		case 7:
			if (advance)
			{
				dialogue.setString(speak, "It's in Ernie's house! And the Grimaldi's house! And a hundered other houses!");
				speech = true;
				advance = false;
				act++;
			} break;
		case 8:
			if (advance)
			{
				dialogue.setString(tag, "Alex");
				dialogue.setString(speak, "What's my money doin' in Ernie's house?!?!");
				front = alex;
				speech = true;
				advance = false;
				act++;
			} break;
		case 9:
			if (advance)
			{
				dialogue.setString(tag, "Kyleigh");
				dialogue.setString(speak, "Yeah, my money should be in my house!");
				front = kyleigh;
				speech = true;
				advance = false;
				act++;
			} break;
		case 10:
			if (advance)
			{
				dialogue.setString(tag, "Ethan");
				dialogue.setString(speak, "That's how the building and loan works. What do you want us to do?/Forclose on them? Throw them out of their houses?");
				front_left = ethan;
				speech = true;
				advance = false;
				act++;
			} break;
		case 11:
			if (advance)
			{
				dialogue.setString(tag, "Oscar");
				dialogue.setString(speak, "Well if that'll get me my money, then yeah I want you to do that");
				front = oscar;
				front_left = -1;
				speech = true;
				advance = false;
				act++;
			} break;
		case 12:
			if (advance)
			{
				dialogue.setString(tag, "Alex");
				dialogue.setString(speak, "If I'm the one who invested in the building and loan then I should technically/own their house right?");
				front = alex;
				speech = true;
				advance = false;
				act++;
			} break;
		case 13:
			if (advance)
			{
				dialogue.setString(tag, "Oscar");
				dialogue.setString(speak, "You should really be giving us their houses since we paid for them.");
				front = oscar;
				speech = true;
				advance = false;
				act++;
			} break;
		case 14:
			if (advance)
			{
				dialogue.setString(tag, "Alex");
				dialogue.setString(speak, "Dibs on Ernie's swimming pool!!");
				front = alex;
				speech = true;
				advance = false;
				act++;
			} break;
		case 15:
			if (advance)
			{
				dialogue.setString(tag, "Oscar");
				dialogue.setString(speak, "The Grimaldi's have always had really nice silverware...");
				front = oscar;
				speech = true;
				advance = false;
				act++;
			} break;
		case 16:
			if (advance)
			{
				dialogue.setString(tag, "Doran");
				dialogue.setString(speak, "I don't care! I got 242 dollars in shares here. Close my account, I want my money now!!");
				front = doran;
				front_left = -1;
				speech = true;
				advance = false;
				act++;
			} break;
		case 17:
			if (advance)
			{
				dialogue.setString(tag, "Adam");
				dialogue.setString(speak, "Okay, sign this form an you'll get your money in sixty days.");
				front_left = adam;
				speech = true;
				advance = false;
				act++;
			} break;
		case 18:
			if (advance)
			{
				dialogue.setString(tag, "Doran");
				dialogue.setString(speak, "SIXTY DAYS!?!?");
				front = doran;
				front_left = -1;
				speech = true;
				advance = false;
				act++;
			} break;
		case 19:
			if (advance)
			{
				dialogue.setString(tag, "Adam");
				dialogue.setString(speak, "Well, that's what you all agreed to when you bought your shares.");
				front_left = adam;
				speech = true;
				advance = false;
				act++;
			} break;
		case 20:
			if (advance)
			{
				dialogue.setString(tag, "Oscar");
				dialogue.setString(speak, "I didn't agree to nothin' like that!");
				front = oscar;
				speech = true;
				advance = false;
				act++;
			} break;
		case 21:
			if (advance)
			{
				dialogue.setString(tag, "Serena");
				dialogue.setString(speak, "Ha! I got my money!!");
				front = serena;
				speech = true;
				advance = false;
				act++;
			} break;
		case 22:
			if (advance)
			{
				dialogue.setString(speak, "Old man Potter will pay you fifty cents on the dollar for your shares");
				speech = true;
				advance = false;
				act++;
			} break;
		case 23:
			if (advance)
			{
				dialogue.setString(tag, "Kyleigh");
				dialogue.setString(speak, "Now what do you say George?");
				front = kyleigh;
				speech = true;
				advance = false;
				act++;
			} break;
		case 24:
			if (advance)
			{
				dialogue.setString(tag, "Adam");
				dialogue.setString(speak, "Well, now, we have to stick to the agreement...");
				front_left = adam;
				speech = true;
				advance = false;
				act++;
			} break;
		case 25:
			if (advance)
			{
				dialogue.setString(tag, "Doran");
				dialogue.setString(speak, "Oh yeah? C'mon everybody! Let's take our shares to Potter! Half's better than nothing!");
				front = doran;
				front_left = -1;
				speech = true;
				advance = false;
				act++;
			} break;
		case 26:
			if (advance)
			{
				dialogue.setString(tag, "Oscar");
				dialogue.setString(speak, "Mutiny!");
				front = oscar;
				speech = true;
				advance = false;
				act++;
			} break;
		case 27:
			if (advance)
			{
				dialogue.setString(tag, "Serena");
				dialogue.setString(speak, "I love conflict!");
				front = serena;
				speech = true;
				advance = false;
				act++;
			} break;
		case 28:
			if (advance)
			{
				dialogue.setString(tag, "Adam");
				dialogue.setString(speak, "Guys shut up!");
				front_left = adam;
				speech = true;
				advance = false;
				act++;
			} break;
		case 29:
			if (advance)
			{
				dialogue.setString(speak, "Don't you see?");
				speech = true;
				advance = false;
				act++;
			} break;
		case 30:
			if (advance)
			{
				dialogue.setString(speak, "Potter wants you living in his shacks and paying the rent he decides. To him,/you're just a bunch of bargains. Now, we can get through this,/but we've got to believe in each other. Have faith!");
				speech = true;
				advance = false;
				act++;
			} break;
		case 31:
			if (advance)
			{
				dialogue.setString(tag, "Alex");
				dialogue.setString(speak, "I'm not a bargain. I'm a real boy!");
				front = alex;
				speech = true;
				advance = false;
				act++;
			} break;
		case 32:
			if (advance)
			{
				dialogue.setString(tag, "Doran");
				dialogue.setString(speak, "That's a lot of fine talk George, but I've got doctor bills to pay!");
				front = doran;
				front_left = -1;
				speech = true;
				advance = false;
				act++;
			} break;
		case 33:
			if (advance)
			{
				dialogue.setString(tag, "Kyleigh");
				dialogue.setString(speak, "You can't feed your kids on faith!");
				front = kyleigh;
				speech = true;
				advance = false;
				act++;
			} break;
		case 34:
			if (advance)
			{
				dialogue.setString(tag, "Oscar");
				dialogue.setString(speak, "Yeah, my kids eat brocoli!");
				front = oscar;
				speech = true;
				advance = false;
				act++;
			} break;
		case 35:
			if (advance)
			{
				dialogue.setString(tag, "Nicole");
				dialogue.setString(speak, "George!, George darling!");
				front = kyleigh;
				blit[nicole] = true;
				pos[nicole] = -400;
				front_left = nicole;
				speech = true;
				advance = false;
				act++;
			} break;
		case 36:
			if (pos[nicole] < -150)
			{ pos[nicole] += 8; }
			else if (advance)
			{
				dialogue.setString(speak, "We've still got some money. The honeymoon money!");
				speech = true;
				advance = false;
				act++;
			} break;
		case 37:
			if (advance)
			{
				dialogue.setString(tag, "Adam");
				dialogue.setString(speak, "We do? Oh, we do! Wait folks! I've got two thousand dollars here, my own money.");
				front_left = adam;
				speech = true;
				advance = false;
				act++;
			} break;
		case 38:
			if (advance)
			{
				dialogue.setString(tag, "all");
				dialogue.setString(speak, "MONEY MONEY CLAMOR MONEY!!!1!");
				speech = true;
				advance = false;
				act++;
			} break;
		case 39:
			if (pos[kyleigh] > 50)
			{
				pos[kyleigh] -= 4;
				pos[serena] -= 6;
				pos[doran] -= 8;
				pos[alex] -= 10;
				pos[oscar] -= 12;
			}
			else { act++; } break;
		case 40:
			if (pos[serena] < 200)
			{ pos[serena] += 4; }
			if (pos[doran] < 250)
			{ pos[doran] += 5; }
			if (pos[alex] < 300)
			{ pos[alex] += 6; }
			if (pos[nicole] > -600)
			{ pos[nicole] -= 8; }
			if (pos[oscar] < 350)
			{ pos[oscar] += 4; }
			else if (advance)
			{
				dialogue.setString(tag, "Adam");
				dialogue.setString(speak, "All right, Charlie! How much do you need?");
				front_left = adam;
				blit[nicole] = false;
				speech = true;
				advance = false;
				act++;
			} break;
		case 41:
			if (advance)
			{
				dialogue.setString(tag, "Doran");
				dialogue.setString(speak, "242 dollars!");
				front = doran;
				front_left = -1;
				speech = true;
				advance = false;
				act++;
			} break;
		case 42:
			if (pos[doran] > -50)
			{ pos[doran] -= 4; }
			else
			{
				if (advance)
				{
					dialogue.setString(tag, "Adam");
					dialogue.setString(speak, "C'mon Charlie! How about just enough to tide you over!");
					front_left = adam;
					speech = true;
					advance = false;
					act++;
				}
			} break;
		case 43:
			if (advance)
			{
				dialogue.setString(tag, "Doran");
				dialogue.setString(speak, "I want my 242 dollars!!");
				front_left = -1;
				speech = true;
				advance = false;
				act++;
			} break;
		case 44:
			if (advance)
			{
				dialogue.setString(tag, "Adam");
				dialogue.setString(speak, "Fine, Uncle Billy give Charlie here 242 dollars...");
				front_left = adam;
				speech = true;
				advance = false;
				act++;
			} break;
		case 45:
			if (advance)
			{
				dialogue.setString(tag, "Alex");
				dialogue.setString(speak, "Man, that Charlie, what a dick...");
				front = alex;
				speech = true;
				advance = false;
				act++;
			} break;
		case 46:
			if (advance)
			{
				dialogue.setString(tag, "Doran");
				dialogue.setString(speak, "That closes my account then.");
				front = doran;
				front_left = -1;
				speech = true;
				advance = false;
				act++;
			} break;
		case 47:
			if (pos[doran] > -700)
			{ pos[doran] -= 6; }
			else if (advance)
			{
				dialogue.setString(tag, "Adam");
				dialogue.setString(speak, "No, that does not close your account.");
				front_left = adam;
				blit[doran] = false;
				speech = true;
				advance = false;
				act++;
			} break;
		case 48:
			if (advance)
			{
				dialogue.setString(speak, "Okay, alright... Eddy now.");
				speech = true;
				advance = false;
				act++;
			} break;
		case 49:
			if (advance)
			{
				dialogue.setString(tag, "");
				dialogue.setString(speak, "Well I got 300 dollars in here...");
				speech = true;
				advance = false;
				act++;
			} break;
		case 50:
			if (advance)
			{
				dialogue.setString(tag, "Adam");
				dialogue.setString(speak, "Oh, but c'mon Eddy! How much do you need?");
				speech = true;
				advance = false;
				act++;
			} break;
		case 51:
			if (advance)
			{
				dialogue.setString(tag, "");
				dialogue.setString(speak, "Um... Well, just twenty dollars then.");
				speech = true;
				advance = false;
				act++;
			} break;
		case 52:
			if (advance)
			{
				dialogue.setString(tag, "Adam");
				dialogue.setString(speak, "Thank you Eddy! Okay, Ms. Davis how about you?");
				speech = true;
				advance = false;
				act++;
			} break;
		case 53:
			if (advance)
			{
				dialogue.setString(tag, "Maayan");
				dialogue.setString(speak, "But this is your own money Goerge.");
				front = maayan;
				front_left = -1;
				pos[maayan] = 100;
				speech = true;
				advance = false;
				act++;
			} break;
		case 54:
			if (advance)
			{
				dialogue.setString(tag, "Adam");
				dialogue.setString(speak, "That's okay, what do you need?");
				front_left = adam;
				speech = true;
				advance = false;
				act++;
			} break;
		case 55:
			if (advance)
			{
				dialogue.setString(tag, "Maayan");
				dialogue.setString(speak, "Well... Would 17.40 be too much?");
				front = maayan;
				front_left = -1;
				speech = true;
				advance = false;
				act++;
			} break;
		case 56:
			if (advance)
			{
				dialogue.setString(tag, "Adam");
				dialogue.setString(speak, "17.40?! Bless you dear, pay it back when you can now.");
				front_left = adam;
				speech = true;
				advance = false;
				act++;
			} break;
		case 57:
			if (advance)
			{
				dialogue.setString(tag, "Adam");
				dialogue.setString(speak, "Okay, who's next now? Let's try to spread it out now...");
				front_left = adam;
				fade.setAlpha(alpha);
				speech = true;
				advance = false;
				act++;
			} break;
		case 58:
			if (pos[maayan] > -700)
			{ pos[maayan] -= 6; }
			fade.render(0, 0);
			if (alpha < 255) { fade.setAlpha(alpha); alpha++; }
			else { act++; } break;
		case 59:
			fade.render(0, 0);
			if (advance)
			{
				blit[kyleigh] = false;
				blit[serena] = false;
				blit[maayan] = false;
				blit[justin] = true;
				blit[doran] = true;
				blit[serena] = true;
				pos[doran] = -400;
				pos[adam] = -250;
				pos[serena] = -100;
				pos[alex] = 350;
				pos[justin] = 450;
				pos[oscar] = 250;
				dialogue.setString(tag, "Doran");
				dialogue.setString(speak, "Mr. Bailey, you all right?");
				front_left = doran;
				speech = true;
				advance = false;
				act++;
			} break;
		case 60:
			fade.render(0, 0);
			if (alpha > 0) { fade.setAlpha(alpha); alpha--; }
			else { act++; advance = false; } break;
		case 61:
			if (advance)
			{
				dialogue.setString(speak, "Martini! George here don't look so good.");
				speech = true;
				advance = false;
				act++;
			} break;
		case 62:
			if (advance)
			{
				dialogue.setString(tag, "Serena");
				dialogue.setString(speak, "You right, Nick. Mr. Bailey! You drinking too much! Too much!");
				front_left = serena;
				speech = true;
				advance = false;
				act++;
			} break;
		case 63:
			if (advance)
			{
				dialogue.setString(tag, "Oscar");
				dialogue.setString(speak, "Bailey? Which Bailey are you talking about?");
				front = oscar;
				front_left = -1;
				speech = true;
				advance = false;
				act++;
			} break;
		case 64:
			if (pos[oscar] > 100)
			{ pos[oscar] -= 3; }
			else if (advance)
			{
				dialogue.setString(tag, "Serena");
				dialogue.setString(speak, "This Mr. Bailey? Mr. George Bailey?");
				front_left = serena;
				speech = true;
				advance = false;
				act++;
			} break;
		case 65:
			if (advance)
			{
				dialogue.setString(tag, "Oscar");
				dialogue.setString(speak, "George Bailey? Why you...");
				front_left = adam;
				speech = true;
				advance = false;
				act++;
			} break;
		case 66:
			if (pos[oscar] > -100)
			{ pos[oscar] -= 8; }
			if (pos[doran] > -650)
			{ pos[doran] -= 4; }
			if (pos[serena] > -600)
			{ pos[serena] -= 4; }
			if (pos[adam] > -400)       
			{ pos[adam] -= 6; }
			else if (advance)
			{
				dialogue.setString(speak, "Serves you right Bailey! Ain't it enough she slaves away teaching your kids!?/She cried for over an hour you louse!");
				speech = true;
				advance = false;
				act++;
			} break;
		case 67:
			if (pos[oscar] > -150)
			{ pos[oscar] -= 8; }
			if (pos[adam] > -500)
			{ pos[adam] -= 6; }
			else if (advance)
			{
				dialogue.setString(tag, "Alex");
				dialogue.setString(speak, "Yeah! You get him Welch!");
				front = alex;
				speech = true;
				advance = false;
				act++;
			} break;
		case 68:
			if (advance)
			{
				dialogue.setString(tag, "Serena");
				dialogue.setString(speak, "Hey! Beat it Mr. Welch! You punch my best friend! Outta here! Go!");
				front = oscar;
				front_left = serena;
				speech = true;
				advance = false;
				act++;
			} break;
		case 69:
			if (pos[oscar] < 1100)
			{ pos[oscar] += 10; }
			if (pos[serena] < 1100)
			{ pos[serena] += 9; }
			else if (advance)
			{
				dialogue.setString(tag, "Doran");
				dialogue.setString(speak, "Go on Welch! Nevermind the tab.");
				front_left = doran;
				blit[oscar] = false;
				blit[serena] = false;
				speech = true;
				advance = false;
				act++;
			} break;
		case 70:
			if (pos[doran] < -400)
			{ pos[doran] += 4; }
			else if (advance)
			{
				dialogue.setString(tag, "Alex");
				dialogue.setString(speak, "Yeah! Right in the nose Martini!");
				front = alex;
				speech = true;
				advance = false;
				act++;
			} break;
		case 71:
			fade.render(0, 0);
			if (alpha < 255) { fade.setAlpha(alpha); alpha++; }
			else { act++; } break;
		case 72:
			fade.render(0, 0);
			if (advance)
			{
				blit[doran] = false;
				blit[serena] = false;
				blit[oscar] = false;
				blit[justin] = false;
				blit[alex] = false;
				blit[nicole] = true;
				pos[adam] = -350;
				pos[nicole] = -200;
				dialogue.setString(tag, "Adam");
				dialogue.setString(speak, "Mary, please! Where are our kids!? I need you Mary please!!");
				front_left = adam;
				front = -1;
				speech = true;
				advance = false;
				act++;
			} break;
		case 73:
			fade.render(0, 0);
			if (alpha > 0) { fade.setAlpha(alpha); alpha--; }
			else { act++; advance = false; } break;
		case 74:
			if (advance)
			{
				dialogue.setString(tag, "Nicole");
				dialogue.setString(speak, "Let go! Get away from me!! Help! Somebody help me!! He's mad!!!");
				front = nicole;
				front_left = doran;
				blit[doran] = true;
				blit[alex] = true;
				pos[doran] = -600;
				pos[alex] = -600;
				speech = true;
				advance = false;
				act++;
			} break;
		case 75:
			if (pos[doran] < -400)
			{ pos[doran] += 8; }
			if (pos[alex] < -300)
			{ pos[alex] += 9; }
			if (pos[adam] < -250)
			{ pos[adam] += 4; }
			if (pos[nicole] < 300)
			{ pos[nicole] += 7; }
			else if (advance)
			{
				dialogue.setString(tag, "Adam");
				dialogue.setString(speak, "Hey! Get out of my way! Mary! Wait! Don't run away!!");
				front_left = adam;
				speech = true;
				advance = false;
				act++;
			} break;
		case 76:
			if (advance)
			{
				dialogue.setString(tag, "Nicole");
				dialogue.setString(speak, "Help! There's a madman after me!!");
				speech = true;
				advance = false;
				act++;
			} break;
		case 77:
			if (pos[nicole] < 1100)
			{ pos[nicole] += 6; }
			else if (advance)
			{
				dialogue.setString(tag, "Adam");
				dialogue.setString(speak, "I've got to see her! Mary!!");
				speech = true;
				advance = false;
				act++;
			} break;
		case 78:
			if (pos[alex] > -550)
			{ pos[alex] -= 8; }
			if (pos[doran] > -550)
			{ pos[doran] -= 6; }
			if (pos[adam] < 1200)
			{ pos[adam] += 12; }
			else if (advance)
			{
				advance = false;
				act++;
			} break;
		case 79:
			fade.render(0, 0);
			if (alpha < 255) { fade.setAlpha(alpha); alpha++; }
			else { act++; } break;
		case 80:
			fade.render(0, 0);
			if (advance)
			{
				advance = false;
				act++;
			} break;
		case 81: // nerd
			free();
			quit = true;
			act = 0;
			// stop music
			// free music
			#ifdef __EMSCRIPTEN__
				emscripten_cancel_main_loop();
				toShow1();
			#endif
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}
};

class Show1
{
public:
	Show1() { }
	~Show1() { free(); }

	Uint32 startTime = 0;
	Uint32 endTime = 0;
	Uint32 delta = 0;
	short fps = 60;
	short timePerFrame = 1000 / 60; // miliseconds

	Sprite dialogueBox;
	Sprite male;
	Sprite fade;
	Sprite page1;
	Sprite page2;
	Sprite page3;
	Sprite page4;
	Sprite page5;
	Sprite page6;
	Sprite page7;
	Sprite page8;
	Sprite adam;

	Actor nolan;

	Sound auld_lang_syne;

	Text dialogue;

	SDL_Event event;

	SDL_Rect camera = { 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT };
	SDL_Rect walkingSprite[20];
	SDL_Rect room[10];

	const Uint8* state = SDL_GetKeyboardState(NULL);

	int act = 0, front = -1, front_left = -1, alpha = 255, size = 1, choice = 0, globalCount = 0;

	const int DRESSING_WIDTH = 900, DRESSING_HEIGHT = 700;
	const int char_x = (SCREEN_WIDTH - 720) / 2, char_y = SCREEN_HEIGHT - 630;

	bool quit = false, speech = true, advance = false, left = false, right = false;

	Sprite sprite[10];

	Picture picture[22];

	bool blit[10];

	int pos[10];

	struct_dressingRoomShow1 overlay;

	void load()
	{
		dialogue.load("Assets/text.png", "Assets/Audio/textClip_und.wav");
		dialogueBox.loadFromFile("Assets/dialogue.png");
		fade.loadFromFile("Assets/fade.png");
		male.loadFromFile("Assets/maleDressingRoom.png");
		page1.loadFromFile("Assets/page1.png");
		page2.loadFromFile("Assets/page2.png");
		page3.loadFromFile("Assets/page3.png");
		page4.loadFromFile("Assets/page4.png");
		page5.loadFromFile("Assets/page5.png");
		page6.loadFromFile("Assets/page6.png");
		page7.loadFromFile("Assets/page7.png");
		page8.loadFromFile("Assets/page8.png");
		//adam.loadFromFile("Assets/adam_show1.png");

		nolan.loadFromFile("Assets/walking.png");

		overlay.load();

		setMaleDressingRoom(room);

		for (int i = 0; i < 20; i++) // dimensions for walking sprite sheet (unfinished)
		{
			walkingSprite[i].x = (i % 5) * 36;
			walkingSprite[i].y = (i / 5) * 83;
			walkingSprite[i].w = 35;
			walkingSprite[i].h = 82;
		}

		for (int i = 0; i < sizeof(blit) / sizeof(blit[0]); i++)
		{ blit[i] = false; }

		for (int i = 0; i < sizeof(pos) / sizeof(pos[0]); i++)
		{ pos[i] = 0; }
	}

	void free()
	{
		dialogue.free();
		dialogueBox.free();
		fade.free();
		male.free();
		page1.free();
		page2.free();
		page3.free();
		page4.free();
		page5.free();
		page6.free();
		page7.free();
		page8.free();

		nolan.free();

		overlay.free();

		for (int i = 0; i < sizeof(sprite) / sizeof(sprite[0]); i++)
		{ sprite[i].free(); }
	}

	bool inRange(SDL_Rect rect)
	{
		if (nolan.x + nolan.CHAR_WIDTH > rect.x&& nolan.x < rect.x + rect.w &&
			nolan.y + nolan.CHAR_HEIGHT > rect.y&& nolan.y < rect.y + rect.h)
		{ return true; }
		else
		{ return false; }
	}

	void loopinTime()
	{
		#ifdef __EMSCRIPTEN__
			emscripten_set_main_loop_arg(&callbackShow1, this, 120, 1);
		#else

		while (!quit)
		{
			if (!startTime)
				startTime = SDL_GetTicks();
			else
				delta = endTime - startTime;

			if (delta < timePerFrame)
				SDL_Delay(timePerFrame - delta);
			if (delta > timePerFrame)
				fps = 1000 / delta;

			iter();

			startTime = endTime;
			endTime = SDL_GetTicks();
		}
		#endif
	}

	void iter()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_e:
					for (int i = 0; i < sizeof(overlay.engage) / sizeof(overlay.engage[0]); i++)
					{
						if (overlay.engage[i].engage)
						{
							if (overlay.engage[i].name == "door16")
							{ act = -1; }
						}
					}
					break;
				case SDLK_SPACE:
					if (speech)
					{ speech = false; dialogue.stop(); }
					else
					{ advance = true; }
					break;
				case SDLK_a:
					left = true;
					break;
				case SDLK_d:
					right = true;
					break;
				case SDLK_p:
					if (act == 12)
					{ act = 13; }
					break;
				}
				break;
			}
		}

		SDL_RenderClear(renderer);

		if (act < 0 || act == 12)
		{
			male.render(0, 0, &camera);

			nolan.render(nolan.x - camera.x, nolan.y - camera.y, &walkingSprite[nolan.anim]);
		}

		for (int i = 0; i < sizeof(sprite) / sizeof(sprite[0]); i++)
		{ if (blit[i]) { sprite[i].render(char_x + pos[i], char_y); } }

		if (front != -1) { sprite[front].render(char_x + pos[front], char_y); }

		if (front_left != -1) { sprite[front_left].render(char_x + pos[front_left], char_y); }

		if (act < 0)
		{ dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight()); }
		dialogue.type(tag, false);

		if (dialogue.type(speak, speech, size))
		{ speech = false; }

		switch (act)
		{
		case -5:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{ act = 12; advance = false; dialogue.setString(speak, ""); }

			break;
		case -4:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{
				dialogue.setString(speak, "Oh...");
				act--;
				speech = true;
				advance = false;
			}

			break;
		case -3:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{
				dialogue.setString(speak, "Well, because Nolan was too lazy to draw a bathroom and there prolly/wouldn't be anything too interesting in there anyways");
				act--;
				speech = true;
				advance = false;
			}

			break;
		case -2:
			dialogueBox.render((SCREEN_WIDTH - dialogueBox.getWidth()) / 2, SCREEN_HEIGHT - dialogueBox.getHeight());
			dialogue.type(tag, false);

			if (dialogue.type(speak, speech))
			{ speech = false; }

			if (advance)
			{
				dialogue.setString(speak, "Why not?");
				act--;
				speech = true;
				advance = false;
			}

			break;
		case -1:
			dialogue.setString(tag, "");
			dialogue.setString(speak, "You can't go in there");
			dialogue.setCoords(speak, speak_x, speak_y);
			advance = false;
			speech = true;
			act--;

			break;
		case 0:
			load();
			dialogue.setCoords(tag, tag_x, tag_y);
			dialogue.setCoords(speak, 280, 300);
			dialogue.setString(tag, "");
			dialogue.setString(speak, "November 21 First Show");
			size = 2;
			speech = true;
			advance = false;
			act++;
			break;
		case 1:
			if (advance)
			{
				dialogue.setString(speak, "");
				dialogue.setCoords(speak, speak_x, speak_y);
				size = 1;
				speech = true;
				advance = false;
				act++;
			} break;
		case 2:
			page1.render((SCREEN_WIDTH - page1.w) / 2, 0);
			fade.render(0, 0);
			if (alpha > 0)
			{ fade.setAlpha(alpha); alpha--; }
			else { act++; } break;
		case 3:
			page1.render((SCREEN_WIDTH - page1.w) / 2, 0);

			if (left)
			{ act--; left = false; }

			if (right)
			{ act++; right = false; }

			break;
		case 4:
			page2.render((SCREEN_WIDTH - page2.w) / 2, 0);

			if (left)
			{ act--; left = false; }

			if (right)
			{ act++; right = false; }

			break;
		case 5:
			page3.render((SCREEN_WIDTH - page3.w) / 2, 0);

			if (left)
			{ act--; left = false; }

			if (right)
			{ act++; right = false; }

			break;
		case 6:
			page4.render((SCREEN_WIDTH - page4.w) / 2, 0);

			if (left)
			{ act--; left = false; }

			if (right)
			{ act++; right = false; }

			break;
		case 7:
			page5.render((SCREEN_WIDTH - page5.w) / 2, 0);

			if (left)
			{ act--; left = false; }

			if (right)
			{ act++; right = false; }

			break;
		case 8:
			page6.render((SCREEN_WIDTH - page6.w) / 2, 0);

			if (left)
			{ act--; left = false; }

			if (right)
			{ act++; right = false; }

			break;
		case 9:
			page7.render((SCREEN_WIDTH - page7.w) / 2, 0);

			if (left)
			{ act--; left = false; }

			if (right)
			{ act++; right = false; }

			break;
		case 10:
			page8.render((SCREEN_WIDTH - page8.w) / 2, 0);

			if (left)
			{ act--; left = false; }

			if (right)
			{ act++; right = false; }

			break;
		case 11:
			page8.render((SCREEN_WIDTH - page8.w) / 2, 0);
			fade.render(0, 0);
			if (alpha < 255)
			{ fade.setAlpha(alpha); alpha++; }
			else { act++; nolan.x = 237; nolan.y = 218; } break;
		case 12:

			if (globalCount < 359) // about 6 seconds per loop
			{ globalCount++; }
			else
			{  globalCount = 0; }

			/*---------------------------------------------------------------------------------------------*/

			camera.x = (nolan.x + nolan.CHAR_WIDTH / 2) - SCREEN_WIDTH / 2;
			camera.y = (nolan.y + nolan.CHAR_HEIGHT / 2) - SCREEN_HEIGHT / 2;

			if (camera.x < 0) { camera.x = 0; }
			if (camera.y < 0) { camera.y = 0; }
			if (camera.x > DRESSING_WIDTH - camera.w) { camera.x = DRESSING_WIDTH - camera.w; }
			if (camera.y > DRESSING_HEIGHT - camera.h) { camera.y = DRESSING_HEIGHT - camera.h; }

			for (int i = 0; i < sizeof(overlay.engage) / sizeof(overlay.engage[0]); i++)
			{
				if (overlay.engage[i].engage)
				{ overlay.e_key.render(nolan.x - 5 - camera.x, nolan.y - 5 - camera.y); break; }
			}

			if (state[SDL_SCANCODE_W] || state[SDL_SCANCODE_S] || state[SDL_SCANCODE_A] || state[SDL_SCANCODE_D])
			{
				if (nolan.count < 59)
				{ nolan.count++; }
				else
				{ nolan.count = 0; }
			}

			if (state[SDL_SCANCODE_W]) { nolan.move(0, -1, room); }
			else if (state[SDL_SCANCODE_S]) { nolan.move(0, 1, room); }

			if (state[SDL_SCANCODE_A]) { nolan.move(-1, 0, room); }
			else if (state[SDL_SCANCODE_D]) { nolan.move(1, 0, room); }

			//adam.render(adam_x, adam_y, &adam_clip[]);

			for (int i = 0; i < sizeof(overlay.engage) / sizeof(overlay.engage[0]); i++)
			{
				if (inRange(overlay.engage[i].range))
				{ 	overlay.engage[i].engage = true; }
				else
				{ overlay.engage[i].engage = false; }
			}

			/*---------------------------------------------------------------------------------------------*/

			break;
		case 13:
			picture[0].loadFromFile("Assets/Pictures/show1_1.png");
			picture[1].loadFromFile("Assets/Pictures/show1_2.png");
			picture[2].loadFromFile("Assets/Pictures/show1_3.png");
			picture[3].loadFromFile("Assets/Pictures/show1_4.png");
			picture[4].loadFromFile("Assets/Pictures/show1_5.png");
			picture[5].loadFromFile("Assets/Pictures/show1_6.png");
			picture[6].loadFromFile("Assets/Pictures/show1_7.png");
			picture[7].loadFromFile("Assets/Pictures/show1_8.png");
			picture[8].loadFromFile("Assets/Pictures/show1_9.png");
			picture[9].loadFromFile("Assets/Pictures/show1_10.png");
			picture[10].loadFromFile("Assets/Pictures/show1_11.png");
			picture[11].loadFromFile("Assets/Pictures/show1_12.png");
			picture[12].loadFromFile("Assets/Pictures/show1_13.png");
			picture[13].loadFromFile("Assets/Pictures/show1_14.png");
			picture[14].loadFromFile("Assets/Pictures/show1_15.png");
			picture[15].loadFromFile("Assets/Pictures/show1_16.png");
			picture[16].loadFromFile("Assets/Pictures/show1_17.png");
			picture[17].loadFromFile("Assets/Pictures/show1_18.png");
			picture[18].loadFromFile("Assets/Pictures/show1_19.png");
			picture[19].loadFromFile("Assets/Pictures/show1_20.png");
			picture[20].loadFromFile("Assets/Pictures/show1_21.png");
			picture[21].loadFromFile("Assets/Pictures/show1_22.png");

			auld_lang_syne.loadFromFile("Assets/Audio/Show1_end.wav");

			auld_lang_syne.setChannel(musicChannel);
			auld_lang_syne.play(0);

			picture[6].set(0, 1, 2); picture[6].on = true;
			picture[12].set(300, 1, 1); picture[12].on = true;
			act++;
			break;
		case 14:
			for (int i = 0; i < sizeof(picture) / sizeof(picture[0]); i++)
			{
				if (picture[i].on)
				{
					picture[i].iter();
					picture[i].render();
				}
			}

			if (picture[12].y < 150)
			{ picture[0].set(300, 1, 3); picture[0].on = true; }

			if (picture[6].y < 400)
			{ picture[8].set(100, 1, 1); picture[8].on = true; }

			if (picture[6].y < 0)
			{ picture[11].set(50, 1, 2); picture[11].on = true; }

			if (picture[11].y < 300)
			{ picture[13].set(200, 1, 1); picture[13].on = true; }

			if (picture[11].y < 200)
			{ picture[1].set(0, 1, 3); picture[1].on = true; }

			if (picture[13].y < 0)
			{ picture[3].set(100, 1, 1); picture[3].on = true; }

			if (picture[13].y < 0)
			{ picture[2].set(250, 1, 4); picture[2].on = true; }

			if (picture[3].y < 0)
			{ picture[15].set(0, 1, 2); picture[15].on = true; }

			if (picture[2].y < 200)
			{ picture[4].set(300, 1, 3); picture[4].on = true; }

			if (picture[2].y < -10)
			{ picture[7].set(0, 1, 2); picture[7].on = true; }

			if (picture[7].y < 450)
			{ picture[9].set(450, 1, 3); picture[9].on = true; }

			if (picture[7].y < 100)
			{ picture[16].set(100, 1, 2); picture[16].on = true; }

			if (picture[16].y < 500)
			{ picture[20].set(100, 1, 3); picture[20].on = true; }

			if (picture[16].y < 300)
			{ picture[21].set(250, 1, 3); picture[21].on = true; }

			if (picture[20].y < 250)
			{ picture[14].set(400, 1, 2); picture[14].on = true; }

			if (picture[16].y < 400)
			{ picture[17].set(300, 1, 2); picture[17].on = true; }

			if (picture[17].y < 250)
			{ picture[10].set(150, 1, 3); picture[10].on = true; }

			if (picture[17].y < 0)
			{ picture[18].set(500, 2, 3); picture[18].on = true; }

			if (picture[18].y < 150)
			{ picture[19].set(200, 1, 2); picture[19].on = true; }

			if (picture[19].y < -244)
			{ act++; }

			break;
		case 15:
			free();
			auld_lang_syne.stop();
			auld_lang_syne.free();
			quit = true;
			act = 0;
			#ifdef __EMSCRIPTEN__
				emscripten_cancel_main_loop();
				toClass2();
			#endif
			break;
		case 999:
			break;
		default:
			break;
		}

		SDL_RenderPresent(renderer);
	}
};

class Class2
{
public:
	Class2() { }
	~Class2() { }

	Uint32 startTime = 0;
	Uint32 endTime = 0;
	Uint32 delta = 0;
	short fps = 60;
	short timePerFrame = 1000 / 60; // miliseconds

	SDL_Rect camera = { 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT };

	SDL_Event event;

	const Uint8* state = SDL_GetKeyboardState(NULL);

	Sprite theatre;
	Sprite backstage;
	Sprite stage1;
	Sprite stage2;
	Sprite tutorial;
	Sprite tutorial2;
	Sprite area1;
	Sprite area2;
	Sprite area3;
	Sprite area4;
	Sprite blade;
	Sprite move_display;
	Sprite jump_display;
	Sprite dash_display;
	Sprite slide_display;
	Sprite e_key;
	Sprite dialogueBox;
	Sprite madeline;
	Sprite arrow;
	Sprite nolan;

	Sprite sprite[8];

	Sound zote;

	Spring spring_1[8];

	Sawblade blade_2[3];

	celesteText dialogue;

	SDL_Rect theatre_asRoom[5];
	SDL_Rect backstage_asRoom[17];
	SDL_Rect stage1_asRoom[18];
	SDL_Rect stage2_asRoom[20];
	SDL_Rect tutorial_asRoom[4];
	SDL_Rect tutorial2_asRoom[5];
	SDL_Rect area1_asRoom[7];
	SDL_Rect area2_asRoom[9];
	SDL_Rect area3_asRoom[8];
	SDL_Rect area4_asRoom[5];
	SDL_Rect* room = tutorial_asRoom; // start at theatre

	SDL_Rect clip[112];
	SDL_Rect michael_clip[8];
	SDL_Rect madeline_clip[4];
	SDL_Rect npc_clip[12];

	Engage engage[6];

	enum actor
	{
		michael, chloe, sophia, justin, adam, katherine, nicole, arya
	};

	const int THEATRE_WIDTH = 1600, THEATRE_HEIGHT = 700;
	const int BACKSTAGE_WIDTH = 2000, BACKSTAGE_HEIGHT = 700;
	const int STAGE1_WIDTH = 900, STAGE1_HEIGHT = 2000;
	const int STAGE2_WIDTH = 900, STAGE2_HEIGHT = 2000;

	const int w = 11 * 4, h = 17 * 4;

	const int dialogue_x = 50, dialogue_y = 50, dialogue2_y = 600;
	const int arrow_x = 758, arrow_y = 120;

	/* land_left, rise_left, fall_left, left, still_left, NULL, still_right, right, fall_right, rise_right, land_right; */

	int x = 0, y = 0, count = 0, globalCount = 0, anim = 0;
	int direction = 1, direction_before = 1;
	int left_still_anim = 0, right_still_anim = 0, left_anim = 0, right_anim = 0,
		left_rise_anim = 0, right_rise_anim = 0, left_fall_anim = 0, right_fall_anim = 0,
		left_land_anim = 0, right_land_anim = 0,
		speed_x = 0, speed_y = 0, jump_stage = 0, dash_stage = 0,
		slide = 0;

	int mouse_x = 0, mouse_y = 0, act = 0, act_2 = 0, control = 0;
	int arrow_y_add = 0, madeline_anim;

	bool jump = true, wall_jump = false, dash = true, dashing = false;
	bool move_disp = true, jump_disp = true, dash_disp = true, slide_disp = true, arrow_switch = false,
		 speech = false, advance = false;

	bool quit = false;

	std::string front = "nolan";

	void load()
	{
		theatre.loadFromFile("Assets/theatre2.png");
		backstage.loadFromFile("Assets/backstage2.png");
		stage1.loadFromFile("Assets/stage1.png");
		stage2.loadFromFile("Assets/stage2.png");
		tutorial.loadFromFile("Assets/tutorial.png");
		tutorial2.loadFromFile("Assets/tutorial2.png");
		area1.loadFromFile("Assets/area1.png");
		area2.loadFromFile("Assets/area2.png");
		area3.loadFromFile("Assets/area3.png");
		area4.loadFromFile("Assets/area4.png");
		blade.loadFromFile("Assets/blade.png");
		move_display.loadFromFile("Assets/move_display.png");
		jump_display.loadFromFile("Assets/jump_display.png");
		dash_display.loadFromFile("Assets/dash_display.png");
		e_key.loadFromFile("Assets/e_key_celeste.png");
		dialogueBox.loadFromFile("Assets/dialogue_celeste.png");
		madeline.loadFromFile("Assets/madeline.png");
		arrow.loadFromFile("Assets/arrow.png");
		nolan.loadFromFile("Assets/nolan.png");
		sprite[michael].loadFromFile("Assets/theo_michael.png");
		sprite[chloe].loadFromFile("Assets/chloe.png");
		sprite[sophia].loadFromFile("Assets/sophia_2.png");
		sprite[justin].loadFromFile("Assets/justin.png");
		sprite[adam].loadFromFile("Assets/adam.png");
		sprite[katherine].loadFromFile("Assets/katherine.png");

		dialogue.loadFromFile("Assets/text_celeste.png");
		dialogue.load();

		engage[michael].setRange(358, 295, 250, 5, "michael");
		engage[chloe].setRange(445, 423, 200, 20, "chloe");
		engage[justin].setRange(111, 354, 200, 20, "justin");
		engage[katherine].setRange(316, 189, 200, 20, "katherine");

		for (int i = 0; i < 112; i++)
		{
			clip[i].x = i % 8 * 15;
			clip[i].y = i / 8 * 23;
			clip[i].w = 14;
			clip[i].h = 22;
		}

		for (int i = 0; i < 8; i++)
		{
			michael_clip[i].x = i % 8 * 61;
			michael_clip[i].y = 0;
			michael_clip[i].w = 60;
			michael_clip[i].h = 20;
		}

		for (int i = 0; i < 4; i++)
		{
			madeline_clip[i].x = i % 4 * 101;
			madeline_clip[i].y = 0;
			madeline_clip[i].w = 100;
			madeline_clip[i].h = 100;
		}

		for (int i = 0; i < 12; i++)
		{
			npc_clip[i].x = i % 12 * 12;
			npc_clip[i].y = 0;
			npc_clip[i].w = 11;
			npc_clip[i].h = 21;
		}

		setTheatre2(theatre_asRoom);
		setBackstage2(backstage_asRoom);
		setStage1(stage1_asRoom);
		setStage2(stage2_asRoom);
		setTutorial(tutorial_asRoom);
		setTutorial2(tutorial2_asRoom);
		setArea1(area1_asRoom);
		setArea2(area2_asRoom);
		setArea3(area3_asRoom);
		setArea4(area4_asRoom);
	}

	void free()
	{
		theatre.free();
		backstage.free();
		stage1.free();
		stage2.free();
		tutorial.free();
		tutorial.free();
		area1.free();
		area2.free();
		area3.free();
		area4.free();
		blade.free();
		move_display.free();
		jump_display.free();
		dash_display.free();
		slide_display.free();
		e_key.free();
		dialogueBox.free();
		madeline.free();
		arrow.free();
		nolan.free();

		dialogue.free();
		
		for (int i = 0; i < sizeof(sprite) / sizeof(sprite[0]); i++)
		{ sprite[i].free(); }
	}

	void loopinTime()
	{
		#ifdef __EMSCRIPTEN__
			emscripten_set_main_loop_arg(&callbackShow1, this, 120, 1);
		#else

		while (!quit)
		{
			if (!startTime)
				startTime = SDL_GetTicks();
			else
				delta = endTime - startTime;

			if (delta < timePerFrame)
				SDL_Delay(timePerFrame - delta);
			if (delta > timePerFrame)
				fps = 1000 / delta;

			iter();

			startTime = endTime;
			endTime = SDL_GetTicks();
		}
		#endif
	}

	void reset()
	{
		if (room == backstage_asRoom)
		{
			x = 10;
			y = 600 - h;
		}
		else if (room == stage1_asRoom)
		{
			x = 10;
			y = 1850 - h;
		}
		else if (room == stage2_asRoom)
		{
			x = 10;
			y = 205 - h;
		}
		else if (room == tutorial_asRoom)
		{
			x = 10;
			y = 600 - h;
		}
	}

	bool inRange(SDL_Rect rect)
	{
		if (x + w > rect.x && x < rect.x + rect.w && y + h > rect.y && y < rect.y + rect.h)
		{ return true; }
		else
		{ return false; }
	}

	void iter()
	{
		while (SDL_PollEvent(&event) != 0)
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				CEASE = true;
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_e:
					for (int i = 0; i < sizeof(engage) / sizeof(engage[0]); i++)
					{
						if (engage[i].engage)
						{
							if (engage[i].name == "michael")
							{
								front = "michael";
								printf("yeet\n");
							}
							else if (engage[i].name == "chloe")
							{
								front = "chloe";
							}
							else if (engage[i].name == "justin")
							{
								front = "justin";
							}
							else if (engage[i].name == "katherine")
							{
								front = "katherine";
							}
						}
					}
					break;
				case SDLK_r:
					std::cout << x << std::endl;
					std::cout << y << std::endl;
					break;
				case SDLK_SPACE:
					if (speech)
					{ speech = false; }
					else
					{ advance = true; }
					break;
				case SDLK_z:
					if (speech)
					{ speech = false; }
					else
					{ advance = true; }
					break;
				}
				break;
			}
		}

		SDL_GetMouseState(&mouse_x, &mouse_y);

		SDL_RenderClear(renderer);

		switch (act)
		{
		case 0:
			x = 100;
			y = 100;

			spring_1[0].set(135, 1842, "vertical");
			spring_1[1].set(872, 1335, "left");
			spring_1[2].set(488, 1239, "right");
			spring_1[3].set(872, 1131, "left");
			spring_1[4].set(282, 1160, "vertical");
			spring_1[5].set(841, 721, "vertical");
			spring_1[6].set(872, 307, "left");
			spring_1[7].set(282, 192, "right");

			blade_2[0].set(100, 350, 100, 750, 3, 0.6);
			blade_2[1].set(295, 500, 295, 900, 3, 0.6);
			blade_2[2].set(50, 1200, 345, 1200, 2, 0.5);

			blade_2[1].y = 900;

			act++;
			break;
		case 1:
			camera.x = (x + w / 2) - SCREEN_WIDTH / 2;
			camera.y = (y + h / 2) - SCREEN_HEIGHT / 2;

			if (camera.x < 0) { camera.x = 0; }
			if (camera.y < 0) { camera.y = 0; }

			if (room == theatre_asRoom)
			{
				if (camera.x > THEATRE_WIDTH - camera.w) { camera.x = THEATRE_WIDTH - camera.w; }
				if (camera.y > THEATRE_HEIGHT - camera.h) { camera.y = THEATRE_HEIGHT - camera.h; }

				theatre.render(0, 0, &camera);

				if (move_disp)
				{
					move_display.render(72 - camera.x, 406 - camera.y);

					if (state[SDL_SCANCODE_D] || state[SDL_SCANCODE_RIGHT])
					{ move_disp = false; }
				}
				if (jump_disp)
				{
					jump_display.render(800 - camera.x, 548 - camera.y);

					if (state[SDL_SCANCODE_SPACE] || state[SDL_SCANCODE_Z])
					{ jump_disp = false; }
				}

				if (x > THEATRE_WIDTH)
				{
					room = tutorial_asRoom;
					x = 10;
					y = 600 - h;
				}
			}
			else if (room == tutorial_asRoom)
			{
				if (camera.x > SCREEN_WIDTH - camera.w) { camera.x = SCREEN_WIDTH - camera.w; }
				if (camera.y > SCREEN_HEIGHT - camera.h) { camera.y = SCREEN_HEIGHT - camera.h; }

				tutorial.render(0, 0, &camera);

				if (dash_disp)
				{
					dash_display.render(251 - camera.x, 616 - camera.y);

					if (state[SDL_SCANCODE_LSHIFT] || state[SDL_SCANCODE_X])
					{ dash_disp = false; }
				}

				if (x > SCREEN_WIDTH)
				{
					room = tutorial2_asRoom;
					x = 10;
					y = 600 - h;
				}
				else if (x + w < 0)
				{
					room = theatre_asRoom;
					x = THEATRE_WIDTH - w - 10;
					y = 545 - h;
				}
				else if (y > SCREEN_HEIGHT)
				{
					reset();
				}
			}
			else if (room == tutorial2_asRoom)
			{
				if (camera.x > SCREEN_WIDTH - camera.w) { camera.x = SCREEN_WIDTH - camera.w; }
				if (camera.y > SCREEN_HEIGHT - camera.h) { camera.y = SCREEN_HEIGHT - camera.h; }

				tutorial2.render(0, 0, &camera);

				if (x > SCREEN_WIDTH)
				{
					room = area1_asRoom;
					x = 10;
					y = 600 - h;
				}
				else if (x + w < 0)
				{
					room = tutorial_asRoom;
					x = SCREEN_WIDTH - w - 10;
					y = 600 - h;
				}
			}
			else if (room == area1_asRoom)
			{
				if (camera.x > SCREEN_WIDTH - camera.w) { camera.x = SCREEN_WIDTH - camera.w; }
				if (camera.y > SCREEN_HEIGHT - camera.h) { camera.y = SCREEN_HEIGHT - camera.h; }

				area1.render(0, 0, &camera);
				sprite[michael].render(358 - camera.x, 220 - camera.y, &michael_clip[globalCount % 120 / 15], 4);

				if (inRange(engage[michael].range))
				{ engage[michael].engage = true; }
				else
				{ engage[michael].engage = false; }

				if (engage[michael].engage)
				{ e_key.render(308 - camera.x, 170 - camera.y); }

				if (x > SCREEN_WIDTH)
				{
					room = area2_asRoom; // area2
					x = 10;
					y = 600 - h;
				}
				else if (x + w < 0)
				{
					room = tutorial2_asRoom;
					x = SCREEN_WIDTH - w - 10;
					y = 200 - h;
				}
			}
			else if (room == area2_asRoom)
			{
				if (camera.x > SCREEN_WIDTH - camera.w) { camera.x = SCREEN_WIDTH - camera.w; }
				if (camera.y > SCREEN_HEIGHT - camera.h) { camera.y = SCREEN_HEIGHT - camera.h; }

				area2.render(0, 0, &camera);
				sprite[chloe].render(573 - camera.x, 349 - camera.y, &npc_clip[globalCount % 120 / 10], 4);
				sprite[sophia].render(480 - camera.x, 349 - camera.y, &npc_clip[globalCount % 120 / 15], 4);
				sprite[justin].render(140 - camera.x, 290 - camera.y, &npc_clip[globalCount % 120 / 10], 4);
				sprite[adam].render(230 - camera.x, 290 - camera.y, &npc_clip[globalCount % 120 / 10], 4);
				sprite[katherine].render(332 - camera.x, 125 - camera.y, &npc_clip[globalCount % 120 / 10], 4);

				if (inRange(engage[chloe].range))
				{ engage[chloe].engage = true; }
				else
				{ engage[chloe].engage = false; }

				if (inRange(engage[justin].range))
				{ engage[justin].engage = true; }
				else
				{ engage[justin].engage = false; }

				if (inRange(engage[katherine].range))
				{ engage[katherine].engage = true; }
				else
				{ engage[katherine].engage = false; }

				if (engage[chloe].engage)
				{ e_key.render(425 - camera.x, 320 - camera.y); }
				else if (engage[justin].engage)
				{ e_key.render(90 - camera.x, 250 - camera.y); }
				else if (engage[katherine].engage)
				{ e_key.render(280 - camera.x, 90 - camera.y); }

				if (x > SCREEN_WIDTH)
				{
					room = area3_asRoom;
					x = 10;
					y = 120 - h;
				}
				else if (x + w < 0)
				{
					room = area1_asRoom;
					x = SCREEN_WIDTH - w - 10;
					y = 600 - h;
				}
			}
			else if (room == area3_asRoom)
			{
				if (camera.x > SCREEN_WIDTH - camera.w) { camera.x = SCREEN_WIDTH - camera.w; }
				if (camera.y > SCREEN_HEIGHT - camera.h) { camera.y = SCREEN_HEIGHT - camera.h; }

				area3.render(0, 0, &camera);

				if (x > SCREEN_WIDTH)
				{
					room = area4_asRoom;
					x = 10;
					y = 600 - h;
				}
				else if (y > SCREEN_HEIGHT)
				{
					x = 10;
					y = 120 - h;
				}
				else if (x + w < 0)
				{
					room = area2_asRoom;
					x = SCREEN_WIDTH - w - 10;
					y = 120 - h;
				}
			}
			else if (room == area4_asRoom)
			{
				if (camera.x > SCREEN_WIDTH - camera.w) { camera.x = SCREEN_WIDTH - camera.w; }
				if (camera.y > SCREEN_HEIGHT - camera.h) { camera.y = SCREEN_HEIGHT - camera.h; }

				area4.render(0, 0, &camera);

				if (x > SCREEN_WIDTH)
				{
					//room = area4_asRoom; // area4
					room = backstage_asRoom;
					x = 10;
					y = 600 - h;
				}
				else if (x + w < 0)
				{
					room = area3_asRoom;
					x = SCREEN_WIDTH - w - 10;
					y = 600 - h;
				}
			}
			else if (room == backstage_asRoom)
			{
				if (camera.x > BACKSTAGE_WIDTH - camera.w) { camera.x = BACKSTAGE_WIDTH - camera.w; }
				if (camera.y > BACKSTAGE_HEIGHT - camera.h) { camera.y = BACKSTAGE_HEIGHT - camera.h; }

				backstage.render(0, 0, &camera);

				if (x + w < 0)
				{
					//room = area1_asRoom;
					x = THEATRE_WIDTH - w - 10;
					y = 600 - h;
				}
				else if (x > BACKSTAGE_WIDTH)
				{
					room = stage1_asRoom;
					x = 10;
					y = 1850 - h;
				}
				else if (y > BACKSTAGE_HEIGHT)
				{
					reset();
				}
			}
			else if (room == stage1_asRoom)
			{
				if (camera.x > STAGE1_WIDTH - camera.w) { camera.x = STAGE1_WIDTH - camera.w; }
				if (camera.y > STAGE1_HEIGHT - camera.h) { camera.y = STAGE1_HEIGHT - camera.h; }

				stage1.render(0, 0, &camera);

				for (int i = 0; i < sizeof(spring_1) / sizeof(spring_1[0]); i++)
				{
					if (spring_1[i].check(x, y, w, h))
					{
						if (spring_1[i].type == "left")
						{ speed_x = -56; speed_y = -32; control = 30; }
						else if (spring_1[i].type == "right")
						{ speed_x = 56; speed_y = -32; control = 30; }
						else if (spring_1[i].type == "vertical")
						{ speed_y = -48; }
						
						left_rise_anim = 0;
						right_rise_anim = 0;
						dash = true;
						dashing = false;
						dash_stage = 0;

						if (direction_before < 0)
						{ direction = -4; }
						else if (direction_before > 0)
						{ direction = 4; }
					}
				}

				if (x + w < 0)
				{
					room = backstage_asRoom;
					x = BACKSTAGE_WIDTH - w - 10;
					y = 383 - h;
				}
				else if (x > STAGE1_WIDTH)
				{
					room = stage2_asRoom;
					x = 10;
					y = 205 - h;
				}
				else if (y > STAGE1_HEIGHT)
				{
					reset();
				}
			}
			else if (room == stage2_asRoom)
			{
				if (camera.x > STAGE2_WIDTH - camera.w) { camera.x = STAGE2_WIDTH - camera.w; }
				if (camera.y > STAGE2_HEIGHT - camera.h) { camera.y = STAGE2_HEIGHT - camera.h; }

				stage2.render(0, 0, &camera);

				for (int i = 0; i < sizeof(blade_2) / sizeof(blade_2[0]); i++)
				{
					blade_2[i].iter();

					blade.render(blade_2[i].x - blade.w / (2 / blade_2[i].size) - camera.x,
								 blade_2[i].y - blade.w / (2 / blade_2[i].size) - camera.y,
								 NULL, blade_2[i].size);

					if (blade_2[i].check(x, y, w, h))
					{
						reset();
					}
				}
				
				if (x + w < 0)
				{
					room = stage1_asRoom;
					x = STAGE1_WIDTH - 10 - w;
					y = 153 - h;
				}
				else if (y > STAGE2_HEIGHT)
				{
					reset();
				}
			}

			if (count < 16)
			{ count++; }
			else
			{ count = 0; }

			if (globalCount < 360)
			{ globalCount++; }
			else
			{ globalCount = 0; }

			switch (direction)
			{
			case -6:
				anim = 104;
				break;
			case -5:
				if (count == 8 || count == 16)
				{ left_land_anim++; }
				if (left_land_anim >= 7)
				{ left_land_anim = 0; direction = -1; }

				anim = 88 + left_land_anim;
				break;
			case -4:
				if (count == 8 || count == 16)
				{ left_rise_anim++; }
				if (left_rise_anim >= 8)
				{ left_rise_anim = 7; }

				anim = 72 + left_rise_anim;
				break;
			case -3:
				if (count == 8 || count == 16)
				{ left_fall_anim++; }
				if (left_fall_anim >= 8)
				{ left_fall_anim = 7; }

				anim = 80 + left_fall_anim;
				break;
			case -2:
				if (count == 8 || count == 16)
				{ left_anim++; }
				if (left_anim == 13)
				{ left_anim = 0; }

				anim = 32 + left_anim;
				break;
			case -1:
				if (count == 16)
				{ left_still_anim++; }
				if (left_still_anim == 8)
				{ left_still_anim = 0; }

				anim = 8 + left_still_anim;
				break;
			case 1:
				if (count == 16)
				{ right_still_anim++; }
				if (right_still_anim == 8)
				{ right_still_anim = 0; }

				anim = right_still_anim;
				break;
			case 2:
				if (count == 8 || count == 16)
				{ right_anim++; }
				if (right_anim == 13)
				{ right_anim = 0; }

				anim = 16 + right_anim;
				break;
			case 3:
				if (count == 8 || count == 16)
				{ right_fall_anim++; }
				if (right_fall_anim >= 8)
				{ right_fall_anim = 7; }

				anim = 56 + right_fall_anim;
				break;
			case 4:
				if (count == 8 || count == 16)
				{ right_rise_anim++; }
				if (right_rise_anim >= 8)
				{ right_rise_anim = 7; }

				anim = 48 + right_rise_anim;
				break;
			case 5:
				if (count == 8 || count == 16)
				{ right_land_anim++; }
				if (right_land_anim >= 7)
				{ right_land_anim = 0; direction = 1; }

				anim = 64 + right_land_anim;
				break;
			case 6:
				anim = 96;
				break;
			}

			nolan.render(x - camera.x, y - camera.y, &clip[anim], 4);

			break;
		case 2:
			free();
			break;
		default:
			break;
		}

		if (front == "nolan")
		{
			direction_before = direction;

			if ((state[SDL_SCANCODE_SPACE] || state[SDL_SCANCODE_Z]) && (jump || wall_jump))
			{
				switch (jump_stage)
				{
				case 0: y -= 20; left_rise_anim = 0; right_rise_anim = 0; break;
				case 1: speed_y = -12; break;
				case 6: speed_y = -18; break;
				case 12: speed_y = -24; jump = false; break;
				}

				if (wall_jump && abs(speed_y) > 4)
				{
					if (direction_before < 0)
					{ speed_x = -35; }
					if (direction_before > 0)
					{ speed_x = 35; }

					//speed_y -= 44; // = -36
					speed_y = -36;

					wall_jump = false;
				}

				if (direction_before < 0)
				{ direction = -4; }
				if (direction_before > 0)
				{ direction = 4; }

				left_fall_anim = 0;
				right_fall_anim = 0;

				slide = 0;

				jump_stage++;
			}
			else
			{
				if (jump_stage > 0)
				{ jump = false; wall_jump = false; }

				jump_stage = 0;
			}

			if ((((state[SDL_SCANCODE_LSHIFT] || state[SDL_SCANCODE_X]) && dash) || dashing) && control == 0)
			{
				slide = 0;

				if (direction_before < 0)
				{
					speed_y = 0;

					switch (dash_stage)
					{
					case 0: dashing = true; break;
					case 1: x -= 6; break;
					case 2: x -= 6; break;
					case 3: x -= 8; break;
					case 4: x -= 8; break;
					case 5: x -= 8; break;
					case 6: x -= 8; break;
					case 7: x -= 10; break;
					case 8: x -= 16; break;
					case 9: x -= 16; break;
					case 10: x -= 10; break;
						//hiiiiiiiiiiiiiiiiiiiiiiiiiiiiii
					case 11: x -= 8; break;
					case 12: x -= 8; break;
					case 13: x -= 8; break;
					case 14: x -= 8; break;
					case 15: x -= 6; break;
					case 16: x -= 6; break;
					case 17: x -= 6; break;
					case 18: x -= 6; break;
					case 19: x -= 4; break;
					case 20: x -= 4; dash = false; dashing = false; break;
					}
				}
						
				if (direction_before > 0)
				{
					speed_y = 0;

					switch (dash_stage)
					{
					case 0: dashing = true; break;
					case 1: x += 6; break;
					case 2: x += 6; break;
					case 3: x += 8; break;
					case 4: x += 8; break;
					case 5: x += 8; break;
					case 6: x += 8; break;
					case 7: x += 10; break;
					case 8: x += 16; break;
					case 9: x += 16; break;
					case 10: x += 10; break;
					case 11: x += 8; break;
					case 12: x += 8; break;
					case 13: x += 8; break;
					case 14: x += 8; break;
					case 15: x += 6; break;
					case 16: x += 6; break;
					case 17: x += 6; break;
					case 18: x += 6; break;
					case 19: x += 4; break;
					case 20: x += 4; dash = false; dashing = false; break;
					}
				}

				dash_stage++;
			}
			else
			{
				dash_stage = 0;
			}

			speed_y += 1;

			if (speed_y >= 28) { speed_y = 28; }

			if (speed_y > 4)
			{
				if (!state[SDL_SCANCODE_SPACE]) { jump = false; }

				if (direction_before < 0)
				{ direction = -3; }
				if (direction_before > 0)
				{ direction = 3; }

				if (speed_y > 12)
				{ slide = 0; wall_jump = false; }
			}

			if ((state[SDL_SCANCODE_A] || state[SDL_SCANCODE_LEFT]) && control == 0)
			{
				speed_x -= 1;
				if (speed_x <= -12) { speed_x = -12; }
				if (abs(direction) != 3 && abs(direction) != 4 && !dashing)
				{ direction = -2; }
				if (!dashing)
				{ direction = -1 * abs(direction); }
			}
			else if ((state[SDL_SCANCODE_D] || state[SDL_SCANCODE_RIGHT]) && control == 0)
			{
				speed_x += 1;
				if (speed_x >= 12) { speed_x = 12; }
				if (abs(direction) != 3 && abs(direction) != 4 && !dashing)
				{ direction = 2; }
				if (!dashing)
				{ direction = abs(direction); }
			}
			else
			{
				if (speed_x > 0) { speed_x -= 1; }
				if (speed_x < 0) { speed_x += 1; }
				if (abs(direction) != 3 && abs(direction) != 4 && abs(direction) != 5)
				{
					if (direction_before < 0)
					{ direction = -1; }
					if (direction_before > 0)
					{ direction = 1; }
				}
			}

			for (int i = 0; i < room[0].x; i++)
			{
				if (i < room[0].y)
				{
					if (x + w + speed_x / 4 > room[i].x && x + speed_x / 4 < room[i].x + room[i].w &&
						y + h + speed_y / 4 > room[i].y && y + speed_y / 4 < room[i].y + room[i].h)
					{
						if (y + h > room[i].y && y < room[i].y + room[i].h)
						{
							if (x < room[i].x)
							{
								x = room[i].x - w; speed_x = 0;
								if (speed_y > 8) { speed_y = 8; }
								if (speed_y > 4) { slide = -1; }
								if (!state[SDL_SCANCODE_SPACE] && !state[SDL_SCANCODE_Z] && speed_y > 4)
								{ wall_jump = true; }
								break;
							}
							if (x + w > room[i].x + room[i].w)
							{
								x = room[i].x + room[i].w; speed_x = 0;
								if (speed_y > 8) { speed_y = 8; }
								if (speed_y > 4) { slide = 1; }
								if (!state[SDL_SCANCODE_SPACE] && !state[SDL_SCANCODE_Z] && speed_y > 4)
								{ wall_jump = true; }
								break;
							}
							if (y < room[i].y)
							{
								y = room[i].y - h; speed_y = 0;
								slide = 0;
								wall_jump = false;
								if (!state[SDL_SCANCODE_SPACE] && !state[SDL_SCANCODE_LSHIFT] &&
									!state[SDL_SCANCODE_Z] && !state[SDL_SCANCODE_X])
								{ jump = true; dash = true; }
								if (direction_before == -3 || direction_before == -4)
								{ direction = -5; }
								if (direction_before == 3 || direction_before == 4)
								{ direction = 5; }
								break;
							}
							if (y + h > room[i].y + room[i].h)
							{ y = room[i].y + room[i].h; speed_y = 0; break; }
						}
						else
						{
							if (y < room[i].y)
							{
								y = room[i].y - h; speed_y = 0;
								slide = 0;
								wall_jump = false;
								if (!state[SDL_SCANCODE_SPACE] && !state[SDL_SCANCODE_LSHIFT] &&
									!state[SDL_SCANCODE_Z] && !state[SDL_SCANCODE_X])
								{ jump = true; dash = true; }
								if (direction_before == -3 || direction_before == -4)
								{ direction = -5; }
								if (direction_before == 3 || direction_before == 4)
								{ direction = 5; }
								break;
							}
							if (y + h > room[i].y + room[i].h)
							{ y = room[i].y + room[i].h; speed_y = 0; break; }
							if (x < room[i].x)
							{
								x = room[i].x - w; speed_x = 0;
								if (speed_y > 8) { speed_y = 8; }
								if (speed_y > 4) { slide = -1; }
								if (!state[SDL_SCANCODE_SPACE] && !state[SDL_SCANCODE_Z] && speed_y > 4)
								{ wall_jump = true; }
								break;
							}
							if (x + w > room[i].x + room[i].w)
							{
								x = room[i].x + room[i].w; speed_x = 0;
								if (speed_y > 8) { speed_y = 8; }
								if (speed_y > 4) { slide = 1; }
								if (!state[SDL_SCANCODE_SPACE] && !state[SDL_SCANCODE_Z] && speed_y > 4)
								{ wall_jump = true; }
								break;
							}
						}
					}
				}
				else
				{
					if (x + w > room[i].x && x < room[i].x + room[i].w &&
						y + h > room[i].y && y < room[i].y + room[i].h)
					{
						reset();
					}
				}
			}

			switch (slide)
			{
			case -1: direction = -6; break;
			case 1: direction = 6; break;
			}

			x += speed_x / 4;
			y += speed_y / 4;

			if (control > 0)
			{ control--; }
		}
		else if (front == "michael")
		{
			dialogueBox.render(dialogue_x, dialogue_y);
			madeline.render(100, 77, &madeline_clip[madeline_anim]);

			if (globalCount % 20 == 0 && speech)
			{ madeline_anim++; }
			else if (!speech)
			{ madeline_anim = 0; }

			if (madeline_anim >= 4)
			{ madeline_anim = 0; }

			if (dialogue.type(speech))
			{ speech = false; }

			arrow.render(dialogue_x + arrow_x, dialogue_y + arrow_y + arrow_y_add);

			if (arrow_switch)
			{
				if (globalCount % 8 == 0)
				{ arrow_y_add++; }
			}
			else
			{
				if (globalCount % 8 == 0)
				{ arrow_y_add--; }
			}

			if (arrow_y_add >= 6)
			{ arrow_switch = false; }
			if (arrow_y_add <= 0)
			{ arrow_switch = true; }

			switch (act_2)
			{
			case 0:
				dialogue.setString("Hey squad its me/Microsoft Paint Michael tm");
				dialogue.setCoords(300, 80);
				zote.loadFromFile("Assets/Audio/zote.wav");
				zote.setChannel(soundChannel);
				zote.play(0);
				speech = true;
				advance = false;
				act_2++;
				break;
			case 1:
				if (advance)
				{
					dialogue.setString("And Im here to tell you that/smoking weed is/super uncool");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 2:
				if (advance)
				{
					dialogue.setString("It has been scientifically/proven that smoking drugs/turns your brain into a fried egg");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 3:
				if (advance)
				{
					dialogue.setString("If someone offers you a joint/even a close friend/remember the three Ds");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 4:
				if (advance)
				{
					dialogue.setString("Denial");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 5:
				if (advance)
				{
					dialogue.setString("Discretion");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 6:
				if (advance)
				{
					dialogue.setString("Punch them in the face/and run away screaming");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 7:
				if (advance)
				{
					dialogue.setString("This has been SAY NO TO DRUGS/with Microsoft Paint Michael tm");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 8:
				if (advance)
				{
					zote.stop();
					zote.free();
					advance = false;
					act_2 = 0;
					jump = false;
					front = "nolan";
				} break;
			default:
				break;
			}
		}
		else if (front == "chloe")
		{
			dialogueBox.render(dialogue_x, dialogue_y);
			madeline.render(100, 77, &madeline_clip[madeline_anim]);

			if (globalCount % 20 == 0 && speech)
			{ madeline_anim++; }
			else if (!speech)
			{ madeline_anim = 0; }

			if (madeline_anim >= 4)
			{ madeline_anim = 0; }

			if (dialogue.type(speech))
			{ speech = false; }

			arrow.render(dialogue_x + arrow_x, dialogue_y + arrow_y + arrow_y_add);

			if (arrow_switch)
			{
				if (globalCount % 8 == 0)
				{ arrow_y_add++; }
			}
			else
			{
				if (globalCount % 8 == 0)
				{ arrow_y_add--; }
			}

			if (arrow_y_add >= 6)
			{ arrow_switch = false; }
			if (arrow_y_add <= 0)
			{ arrow_switch = true; }

			switch (act_2)
			{
			case 0:
				dialogue.setString("Hey kids and welcome/back to another episode of/SAY NO TO DRUGS");
				dialogue.setCoords(300, 80);
				zote.loadFromFile("Assets/Audio/zote.wav");
				zote.setChannel(soundChannel);
				zote.play(0);
				speech = true;
				advance = false;
				act_2++;
				break;
			case 1:
				if (advance)
				{
					dialogue.setString("This is Microsoft Paint Michael tm/and welcome to our second lesson");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 2:
				if (advance)
				{
					dialogue.setString("Drugs are bad/Every year about two bajillion children/die of drug use");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 3:
				if (advance)
				{
					dialogue.setString("Drugs can cause such effects as");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 4:
				if (advance)
				{
					dialogue.setString("Death");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 5:
				if (advance)
				{
					dialogue.setString("Loss of life");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 6:
				if (advance)
				{
					dialogue.setString("Mortality");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 7:
				if (advance)
				{
					dialogue.setString("Demise");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 8:
				if (advance)
				{
					dialogue.setString("And fatality");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 9:
				if (advance)
				{
					dialogue.setString("So remember kids/keep cool and don't do drugs");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 10:
				if (advance)
				{
					dialogue.setString("Thank you this has been/SAY NO TO DRUGS/with Microsoft Paint Michael tm");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 11:
				if (advance)
				{
					zote.stop();
					zote.free();
					advance = false;
					act_2 = 0;
					jump = false;
					front = "nolan";
				} break;
			default:
				break;
			}
		}
		else if (front == "justin")
		{
			dialogueBox.render(dialogue_x, dialogue_y);
			madeline.render(100, 77, &madeline_clip[madeline_anim]);

			if (globalCount % 20 == 0 && speech)
			{ madeline_anim++; }
			else if (!speech)
			{ madeline_anim = 0; }

			if (madeline_anim >= 4)
			{ madeline_anim = 0; }

			if (dialogue.type(speech))
			{ speech = false; }

			arrow.render(dialogue_x + arrow_x, dialogue_y + arrow_y + arrow_y_add);

			if (arrow_switch)
			{
				if (globalCount % 8 == 0)
				{ arrow_y_add++; }
			}
			else
			{
				if (globalCount % 8 == 0)
				{ arrow_y_add--; }
			}

			if (arrow_y_add >= 6)
			{ arrow_switch = false; }
			if (arrow_y_add <= 0)
			{ arrow_switch = true; }

			switch (act_2)
			{
			case 0:
				dialogue.setString("Hi kids Im Microsoft Paint Michael tm/And this our third lesson of/SAY NO TO DRUGS");
				dialogue.setCoords(300, 80);
				zote.loadFromFile("Assets/Audio/zote.wav");
				zote.setChannel(soundChannel);
				zote.play(0);
				speech = true;
				advance = false;
				act_2++;
				break;
			case 1:
				if (advance)
				{
					dialogue.setString("For todays lesson weve brought/in a special guest");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 2:
				if (advance)
				{
					dialogue.setString("This is Ike/he fights for his friends/and he does crack");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 3:
				if (advance)
				{
					dialogue.setString("So Ike how has doing crack/changed your life");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 4:
				if (advance)
				{
					dialogue.setString("");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 5:
				if (advance)
				{
					dialogue.setString("As you can clearly see/crack has taken away Ikes/ability to speak");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 6:
				if (advance)
				{
					dialogue.setString("Ike used to have a happy/fufilling life");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 7:
				if (advance)
				{
					dialogue.setString("He was a kingpin in an illegal/cartel dealing mexican black tar heroin");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 8:
				if (advance)
				{
					dialogue.setString("Truly Ike had a budding career/and a rich and successful/life ahead of him");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 9:
				if (advance)
				{
					dialogue.setString("But that was all thrown away/when Ike was sucked into the/dark world of crack cocaine");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 10:
				if (advance)
				{
					dialogue.setString("A deep well of inescapable bliss/he slowly saw his world falling apart");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 11:
				if (advance)
				{
					dialogue.setString("");
					speech = true;
					advance = false;
					act_2++;
				} break;
			case 12:
				if (advance)
				{
					zote.stop();
					zote.free();
					advance = false;
					act_2 = 0;
					jump = false;
					front = "nolan";
				} break;
			default:
				break;
			}
		}
		else if (front == "katherine")
		{
		dialogueBox.render(dialogue_x, dialogue_y);
		madeline.render(100, 77, &madeline_clip[madeline_anim]);

		if (globalCount % 20 == 0 && speech)
		{ madeline_anim++; }
		else if (!speech)
		{ madeline_anim = 0; }

		if (madeline_anim >= 4)
		{ madeline_anim = 0; }

		if (dialogue.type(speech))
		{ speech = false; }

		arrow.render(dialogue_x + arrow_x, dialogue_y + arrow_y + arrow_y_add);

		if (arrow_switch)
		{
			if (globalCount % 8 == 0)
			{ arrow_y_add++; }
		}
		else
		{
			if (globalCount % 8 == 0)
			{ arrow_y_add--; }
		}

		if (arrow_y_add >= 6)
		{ arrow_switch = false; }
		if (arrow_y_add <= 0)
		{ arrow_switch = true; }

		switch (act_2)
		{
		case 0:
			dialogue.setString("Yo squad welcome back/to another episode of/SAY NO TO DRUGS");
			dialogue.setCoords(300, 80);
			zote.loadFromFile("Assets/Audio/zote.wav");
			zote.setChannel(soundChannel);
			zote.play(0);
			speech = true;
			advance = false;
			act_2++;
			break;
		case 1:
			if (advance)
			{
				dialogue.setString("Im Microsoft Paint Michael tm/and today well be talking about/the dangers of tylenol");
				speech = true;
				advance = false;
				act_2++;
			} break;
		case 2:
			if (advance)
			{
				dialogue.setString("Tylenol is a highly dangerous/substance millions of teens/die every year of tylenol abuse");
				speech = true;
				advance = false;
				act_2++;
			} break;
		case 3:
			if (advance)
			{
				dialogue.setString("");
				speech = true;
				advance = false;
				act_2++;
			} break;
		case 4:
			if (advance)
			{
				dialogue.setString("");
				speech = true;
				advance = false;
				act_2++;
			} break;
		case 5:
			if (advance)
			{
				dialogue.setString("");
				speech = true;
				advance = false;
				act_2++;
			} break;
		case 6:
			if (advance)
			{
				dialogue.setString("");
				speech = true;
				advance = false;
				act_2++;
			} break;
		case 7:
			if (advance)
			{
				dialogue.setString("");
				speech = true;
				advance = false;
				act_2++;
			} break;
		case 8:
			if (advance)
			{
				zote.stop();
				zote.free();
				advance = false;
				act_2 = 0;
				jump = false;
				front = "nolan";
			} break;
		default:
			break;
		}
		}

		SDL_RenderPresent(renderer);
	}
};

Start start; // can you put this into a local scope?
Other other; // no u hecc nerd boi
Tech1 tech1; // no but rly, pls do that
Show1 show1;
Class2 class2;

void next()
{
	other.load();
	other.loopinTime();
}

void toTech1()
{
	printf("at tech1()\n");
	tech1.load();
	printf("loaded\n");
	tech1.loopinTime();
}

void toShow1()
{
	show1.load();
	show1.loopinTime();
}

void toClass2()
{
	class2.load();
	class2.loopinTime();
}

int main(int argc, char* args[])
{
	window = SDL_CreateWindow("lo the icy talons of death approach", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0xFF);
	SDL_Init(SDL_INIT_AUDIO);
	Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096);

	srand(time(NULL));

	printf("yeetus feetus deleetus!\n");

	//start.loopinTime();
	//start.free();

	if (!CEASE)
	{
		//other.load();
		//other.loopinTime();
		//other.free();
	}

	if (!CEASE)
	{
		//tech1.load();
		//tech1.loopinTime();
		//tech1.free();
	}

	if (!CEASE)
	{
		//show1.load();
		//show1.loopinTime();
		//show1.free();
	}

	if (!CEASE)
	{
		class2.load();
		class2.loopinTime();
		class2.free();
	}

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	window = NULL;
	renderer = NULL;

	Mix_CloseAudio();
	IMG_Quit();
	SDL_Quit();

	return 0;
}

void RenderLoopCallback(void* arg)
{ static_cast<Start*>(arg)->iter(); }

void callbackOther(void* arg)
{ static_cast<Other*>(arg)->iter(); }

void callbackTech1(void* arg)
{ static_cast<Tech1*>(arg)->iter(); }

void callbackShow1(void* arg)
{ static_cast<Show1*>(arg)->iter(); }
#pragma once

#ifdef __EMSCRIPTEN__
#include <SDL.h>
#include <SDL_image.h>
#include <iostream>
#include "mixer.h"
#endif

SDL_Window* window = NULL;
SDL_Renderer* renderer = NULL;

const int SCREEN_WIDTH = 900;
const int SCREEN_HEIGHT = 700;

extern bool CEASE = false;

class Sprite
{
public:
	Sprite() { mTexture = NULL; w = 0; h = 0; }

	~Sprite() { free(); }

	bool loadFromFile(std::string path)
	{
		free();

		SDL_Texture* newTexture = NULL;
		SDL_Surface* loadedSurface = IMG_Load(path.c_str());

		SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));

		newTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);

		w = loadedSurface->w;
		h = loadedSurface->h;

		SDL_FreeSurface(loadedSurface);

		mTexture = newTexture;
		return mTexture != NULL;
	}

	void setAlpha(Uint8 alpha)
	{
		if (alpha > 255)
			SDL_SetTextureAlphaMod(mTexture, 255);
		else
			SDL_SetTextureAlphaMod(mTexture, alpha);
	}

	void free()
	{
		if (mTexture != NULL)
		{
			SDL_DestroyTexture(mTexture);
			mTexture = NULL;
			w = 0;
			h = 0;
		}
	}

	void render(int x, int y, SDL_Rect* clip = NULL, float scalar = 1)
	{
		SDL_Rect renderQuad = { x, y, w * scalar, h * scalar };

		if (clip != NULL)
		{
			renderQuad.w = clip->w * scalar;
			renderQuad.h = clip->h * scalar;
		}

		SDL_RenderCopy(renderer, mTexture, clip, &renderQuad);
	}

	void renderEx(int x, int y, SDL_Rect* clip = NULL, float scalar = 1, double angle = NULL,
		SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_RendererFlip::SDL_FLIP_NONE)
	{
		SDL_Rect renderQuad = { x, y, w * scalar, h * scalar };

		if (clip != NULL)
		{
			renderQuad.w = clip->w * scalar;
			renderQuad.h = clip->h * scalar;
		}

		SDL_RenderCopyEx(renderer, mTexture, clip, &renderQuad, angle, center, flip);
	}

	int getWidth() { return w; }
	int getHeight() { return h; }

	SDL_Texture* mTexture;

	int w, h;
};

class Sound
{
public:
	Sound() { audio = NULL; };

	~Sound() { }

	void loadFromFile(std::string path)
	{
		audio = Mix_LoadWAV(path.c_str());
	}

	void setChannel(int channel)
	{
		Sound::channel = channel;
	}

	void play(int repetitions)
	{
		Mix_PlayChannel(channel, audio, repetitions);
	}

	void stop()
	{
		Mix_HaltChannel(channel);
	}

	void free()
	{
		Mix_FreeChunk(audio);
	}

	Mix_Chunk* audio;
	int channel = 0;
};

class Actor : public Sprite
{
public:
	int x = 900, y = 725, anim = 0, count = 0;

	const int CHAR_WIDTH = 35;
	const int CHAR_HEIGHT = 82;

	void move(int move_x, int move_y, SDL_Rect* room = NULL)
	{
		x += move_x;
		y += move_y;

		if (move_y < 0)
		{ anim = positions::up1 + (count / 12); }
		else if (move_y > 0)
		{ anim = positions::down1 + (count / 12); }
		
		if (move_x < 0)
		{ anim = positions::left1 + (count / 12); }
		else if (move_x > 0)
		{ anim = positions::right1 + (count / 12); }

		for (int i = 0; i < room[0].x; i++)
		{
			if (x + CHAR_WIDTH > room[i].x && x < room[i].x + room[i].w &&
				y + CHAR_HEIGHT > room[i].y && y + 70 < room[i].y + room[i].h)
			{
				x -= move_x;
				y -= move_y;
			}
		}
	}

private:
	enum positions
	{
		right1, right2, right3, right4, right5,
		left1, left2, left3, left4, left5,
		up1, up2, up3, up4, up5,
		down1, down2, down3, down4, down5
	};
};

class Picture : public Sprite
{
public:
	Picture() { }
	~Picture() { free(); }

	void render()
	{
		SDL_Rect renderQuad = { x, y, w, h };

		SDL_RenderCopy(renderer, mTexture, NULL, &renderQuad);
	}

	void iter()
	{
		if (count >= against)
		{
			y -= speed;

			if (y < -h)
			{
				free();
				on = false;
			}

			count = 0;
		}
		else
		{
			count++;
		}
	}

	void set(int x, int speed, int against)
	{
		Picture::x = x;
		Picture::speed = speed;
		Picture::against = against;
	}

	int x = 0, y = 700, speed = 0, against = 0, count = 0;
	bool on = false;
};

class Text
{
public:
	Text() {}

	~Text() { spriteFree(); audioFree(); }

	bool type(int index, bool typing_effect = false, float size = 1)
	{
		int j = 0, y = 0;
		if (typing_effect)
		{
			if (pos == 1)
				play(text[index].size());
			for (int i = 0; i < pos / 6; i++)
			{
				render(coords[index][0] + (j * 8 * size), coords[index][1] + y, &character[getLetter(text[index][i])], size);
				j++;
				if (text[index][i] == '/')
				{
					y += 20; j = 0;
				}
			}

			if (pos > text[index].size() * 6)
			{
				stop();
				pos = 0;
				return true;
			}
			else
			{
				pos++;
				return false;
			}
		}
		else
		{
			for (int i = 0; i < text[index].size(); i++)
			{
				render(coords[index][0] + (j * 8 * size), coords[index][1] + y, &character[getLetter(text[index][i])], size);
				j++;
				if (text[index][i] == '/')
				{
					y += 20; j = 0;
				}
			}
			return true;
		}
	}

	void setString(int index, std::string string)
	{
		if (string != text[index])
		{
			text[index] = string;
			pos = 0;
		}
	}

	void setCoords(int index, int x, int y)
	{
		if (x != coords[index][0])
		{
			coords[index][0] = x;
		}
		if (y != coords[index][1])
		{
			coords[index][1] = y;
		}
	}

	bool getTyping(int index)
	{
		if (pos > text[index].size() * 6) { return true; }
		else { return false; }
	}

	std::string getString(int index)
	{
		return text[index];
	}

	void stop()
	{
		Mix_HaltChannel(channel);
	}

	void load(std::string imagePath, std::string audioPath)
	{
		spriteFree();

		SDL_Texture* newTexture = NULL;
		SDL_Surface* loadedSurface = IMG_Load(imagePath.c_str());
		audio = Mix_LoadWAV(audioPath.c_str());

		SDL_SetColorKey(loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));

		newTexture = SDL_CreateTextureFromSurface(renderer, loadedSurface);

		w = loadedSurface->w;
		h = loadedSurface->h;

		SDL_FreeSurface(loadedSurface);

		mTexture = newTexture;

		for (int i = 0; i < 100; i++)
		{
			character[i].x = ((i % 10) * 11);
			character[i].y = ((i / 10) * 14);
			character[i].w = 10;
			character[i].h = 13;
		}
	}

	void free()
	{
		spriteFree();
		audioFree();
	}

//private:
	void render(int x, int y, SDL_Rect* clip = NULL, float scalar = 1)
	{
		SDL_Rect renderQuad = { x, y, w * scalar, h * scalar };

		if (clip != NULL)
		{
			renderQuad.w = clip->w * scalar;
			renderQuad.h = clip->h * scalar;
		}

		SDL_RenderCopy(renderer, mTexture, clip, &renderQuad);
	}

	int getLetter(int index)
	{
		int letter = static_cast<int>(index); // -er mage

		if (letter >= 32 && letter < 65)
			letter += 20;
		else if (letter >= 65 && letter <= 90)
			letter -= 65;
		else if (letter >= 97)
			letter -= 71;

		return letter;
	}

	void play(int repetitions)
	{
		Mix_PlayChannel(channel, audio, repetitions);
	}

	void spriteFree()
	{
		if (mTexture != NULL)
		{
			SDL_DestroyTexture(mTexture);
			mTexture = NULL;
			w = 0;
			h = 0;
		}
	}

	void audioFree()
	{
		Mix_FreeChunk(audio);
	}

	SDL_Texture* mTexture;
	SDL_Rect character[100];

	Mix_Chunk* audio;

	int w, h, pos = 0;
	int channel = 1;
	int coords[17][2];
	std::string text[17];
};

class celesteText : public Sprite
{
public:
	celesteText() { }
	~celesteText() { free(); }

	void load()
	{
		for (int i = 0; i < 100; i++)
		{
			character[i].x = ((i % 10) * 22);
			character[i].y = ((i / 10) * 28);
			character[i].w = 21;
			character[i].h = 27;
		}
	}

	bool type(bool typing_effect = false, float size = 1)
	{
		int j = 0, y = 0;

		if (typing_effect)
		{
			for (int i = 0; i < pos / 6; i++)
			{
				if (i == 0)
				{ j = 0; }
				else
				{ j += getSpace(i - 1) + 3; }

				render(coords[0] + j * size, coords[1] + y, &character[getLetter(text[i])], size);

				if (text[i] == '/')
				{ y += 30; j = -getSpace(i) - 3; }
			}

			if (pos > text.size() * 6)
			{
				pos = 0;
				return true;
			}
			else
			{
				pos++;
				return false;
			}
		}
		else
		{
			for (int i = 0; i < text.size(); i++)
			{
				if (i == 0)
				{ j = 0; }
				else
				{ j += getSpace(i - 1) + 3; }

				render(coords[0] + j * size, coords[1] + y, &character[getLetter(text[i])], size);

				if (text[i] == '/')
				{ y += 30; j = -getSpace(i) - 3; }
			}
			return true;
		}
	}

	void setString(std::string string)
	{
		if (string != text)
		{
			text = string;
			pos = 0;
		}
	}

	void setCoords(int x, int y)
	{
		if (x != coords[0])
		{ coords[0] = x; }
		if (y != coords[1])
		{ coords[1] = y; }
	}

	bool getTyping(int index)
	{
		if (pos > text.size() * 6) { return true; }
		else { return false; }
	}

	int getLetter(int index)
	{
		int letter = static_cast<int>(index); // -er mage

		if (letter >= 32 && letter < 65)
			letter += 20;
		else if (letter >= 65 && letter <= 90)
			letter -= 65;
		else if (letter >= 97)
			letter -= 71;

		return letter;
	}

	int getSpace(int letter)
	{
		if (text[letter] == *"A")
		{ return 20; }
		else if (text[letter] == *"B")
		{ return 15; }
		else if (text[letter] == *"C")
		{ return 19; }
		else if (text[letter] == *"D")
		{ return 18; }
		else if (text[letter] == *"E")
		{ return 15; }
		else if (text[letter] == *"F")
		{ return 15; }
		else if (text[letter] == *"G")
		{ return 19; }
		else if (text[letter] == *"H")
		{ return 18; }
		else if (text[letter] == *"I")
		{ return 5; }
		else if (text[letter] == *"J")
		{ return 15; }
		else if (text[letter] == *"K")
		{ return 18; }
		else if (text[letter] == *"L")
		{ return 15; }
		else if (text[letter] == *"M")
		{ return 20; }
		else if (text[letter] == *"N")
		{ return 18; }
		else if (text[letter] == *"O")
		{ return 20; }
		else if (text[letter] == *"P")
		{ return 16; }
		else if (text[letter] == *"Q")
		{ return 20; }
		else if (text[letter] == *"R")
		{ return 17; }
		else if (text[letter] == *"S")
		{ return 17; }
		else if (text[letter] == *"T")
		{ return 16; }
		else if (text[letter] == *"U")
		{ return 18; }
		else if (text[letter] == *"V")
		{ return 18; }
		else if (text[letter] == *"W")
		{ return 20; }
		else if (text[letter] == *"X")
		{ return 20; }
		else if (text[letter] == *"Y")
		{ return 19; }
		else if (text[letter] == *"Z")
		{ return 18; }
		else if (text[letter] == *"a")
		{ return 16; }
		else if (text[letter] == *"b")
		{ return 16; }
		else if (text[letter] == *"c")
		{ return 15; }
		else if (text[letter] == *"d")
		{ return 16; }
		else if (text[letter] == *"e")
		{ return 15; }
		else if (text[letter] == *"f")
		{ return 10; }
		else if (text[letter] == *"g")
		{ return 17; }
		else if (text[letter] == *"h")
		{ return 14; }
		else if (text[letter] == *"i")
		{ return 6; }
		else if (text[letter] == *"j")
		{ return 8; }
		else if (text[letter] == *"k")
		{ return 14; }
		else if (text[letter] == *"l")
		{ return 4; }
		else if (text[letter] == *"m")
		{ return 20; }
		else if (text[letter] == *"n")
		{ return 14; }
		else if (text[letter] == *"o")
		{ return 18; }
		else if (text[letter] == *"p")
		{ return 16; }
		else if (text[letter] == *"q")
		{ return 16; }
		else if (text[letter] == *"r")
		{ return 9; }
		else if (text[letter] == *"s")
		{ return 14; }
		else if (text[letter] == *"t")
		{ return 9; }
		else if (text[letter] == *"u")
		{ return 14; }
		else if (text[letter] == *"v")
		{ return 16; }
		else if (text[letter] == *"w")
		{ return 20; }
		else if (text[letter] == *"x")
		{ return 16; }
		else if (text[letter] == *"y")
		{ return 16; }
		else if (text[letter] == *"z")
		{ return 15; }
		else return 8;
	}

	void free()
	{
		if (mTexture != NULL)
		{
			SDL_DestroyTexture(mTexture);
			mTexture = NULL;
			w = 0;
			h = 0;
		}
	}

	SDL_Texture* mTexture;
	SDL_Rect character[100];

	int w, h, pos = 0;
	int coords[2] = { 0, 0 };
	std::string text;
};

struct Engage
{
public:
	Engage() { }
	~Engage() { }

	void setRange(int x, int y, int w, int h, std::string name = "foo")
	{
		range.x = x;
		range.y = y;
		range.w = w;
		range.h = h;
		Engage::name = name;
	}

	SDL_Rect range = { 0, 0, 0, 0 };
	std::string name = "foo";
	bool engage = false;
};

class Vector : public Engage
{
public:
	Vector() { }
	~Vector() { }

	void set(int x, int y, int ico_x = NULL, int ico_y = NULL, int flipVal = NULL, int mod = NULL, int div = NULL)
	{
		Vector::x = x;
		Vector::y = y;
		Vector::ico_x = ico_x;
		Vector::ico_y = ico_y;
		Vector::flipVal = flipVal;
		Vector::mod = mod;
		Vector::div = div;

		if (mod || div)
		{ clip_init = true; }
	}

	int x = 0, y = 0;
	int ico_x = 0, ico_y = 0;
	int flipVal = 0, div = 0, mod = 0;
	SDL_Rect clip[12] = { 0, 0, 0, 0 };
	bool front = false, clip_init = false;
};

class Character : public Sprite, public Vector
{
public:
	Character() { }
	~Character() { }

	void setLayer(int against)
	{
		if (against < flipVal)
		{ front = true; }
		else
		{ front = false; }
	}

	void setLayer_curtain(int x, int y, int w, int h)
	{
		if (y + h < range.y + range.h && ((x < range.x + range.w && x + w > range.x) || (
			x < range.x + range.w && x + w > range.x)))
		{ front = true; setAlpha(100); }
		else
		{ front = false; setAlpha(255); }
	}

	bool display = true;
};

class Spring
{
public:
	Spring() { }
	~Spring() { }

	void set(int x, int y, std::string type)
	{
		Spring::x = x;
		Spring::y = y;
		Spring::type = type;

		if (type != "vertical")
		{ w = 8; h = 40; }
	}

	bool check(int x, int y, int w, int h)
	{
		if (x < Spring::x + Spring::w && x + w > Spring::x && y + h > Spring::y && y < Spring::y + Spring::h)
		{ return true; }
		else
		{ return false; }
	}

	int x = 0, y = 0;
	int w = 40, h = 8;
	std::string type = "vertical";
};

class Sawblade
{
public:
	Sawblade() { }
	~Sawblade() { }

	void set(int x_1, int y_1, int x_2, int y_2, int speed, float size)
	{
		Sawblade::x_1 = x_1;
		Sawblade::y_1 = y_1;
		Sawblade::x_2 = x_2;
		Sawblade::y_2 = y_2;
		Sawblade::speed = speed;
		Sawblade::size = size;

		x = x_1;
		y = y_1;

		if (x_2 - x_1 == 0)
		{ vert = true; }
		else
		{ slope = (y_2 - y_1) / (x_2 - x_1); }
	}

	void iter()
	{
		if (vert)
		{
			if (y > y_2)
			{ target = 0; }
			if (y < y_1)
			{ target = 1; }

			if (target)
			{ y += speed; }
			if (!target)
			{ y -= speed; }
		}
		else
		{
			if (x > x_2 || y > y_2)
			{ target = 0; }
			if (x < x_1 || y < y_1)
			{ target = 1; }

			if (target)
			{
				x += speed;
				y += speed * slope;
			}
			if (!target)
			{
				x -= speed;
				y -= speed * slope;
			}
		}
	}

	bool check(int x, int y, int w, int h)
	{
		int radius = 300 * size / 2;
		int cX, cY;
	
		// closest x offset
		if (Sawblade::x < x)
		{ cX = x; }
		else if (Sawblade::x > x + w)
		{ cX = x + w; }
		else
		{ cX = Sawblade::x; }

		// closest y offset
		if (Sawblade::y < y)
		{ cY = y; }
		else if (Sawblade::y > y + h)
		{  cY = y + h; }
		else
		{ cY = Sawblade::y; }

		// if the closest point is inside the circle
		if (distanceSquared(Sawblade::x, Sawblade::y, cX, cY) < radius * radius)
		{
			// box and the circle have collided
			return true;
		}

		// shapes have not collided
		return false;
	}

	double distanceSquared(int x1, int y1, int x2, int y2)
	{
		int deltaX = x2 - x1;
		int deltaY = y2 - y1;
		return deltaX * deltaX + deltaY * deltaY;
	}

	int x_1 = 0, y_1 = 0, x_2 = 0, y_2 = 0;
	int x = 0, y = 0;
	int speed = 0;

	float slope = 0, size = 1.0;

	bool target = 1, vert = false;
};

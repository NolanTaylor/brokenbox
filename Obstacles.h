#pragma once

#ifdef __EMSCRIPTEN__
#include <SDL.h>
#endif

/*
	hey, so apparently past nolan is some kind of idiot
	who doesn't know how to dynamically loop through a simple array.
	instead he decided it would be a good idea to have the size of the array
	recorded on the x value of the first rect and loop through that many iterations.
	But as present nolan i'm too lazy to change this so just go along with it i guess...

	okay, but now only for the first three because rooms past that use the newer Node class
	which loops dynamically over arrays... huzzah for efficiency? boo for consistency?

	k, nvm. we're going back to the room[0].x thing because getting the size of a 
	SDL_Rect POINTER is acting all wack and stuff...
*/

void setTheatre(SDL_Rect theatre[50])
{
	/*--------------------------------------- OBSTACLES IN THEATRE ----------------------------------------*/

	theatre[0].x = 50;   theatre[1].x = 344;  theatre[2].x = 322;  theatre[3].x = 322;  theatre[4].x = 322;
	theatre[0].y = 0;    theatre[1].y = 646;  theatre[2].y = 700;  theatre[3].y = 754;  theatre[4].y = 808;
	theatre[0].w = 0;    theatre[1].w = 142;  theatre[2].w = 242;  theatre[3].w = 292;  theatre[4].w = 292;
	theatre[0].h = 0;    theatre[1].h = 30;   theatre[2].h = 30;   theatre[3].h = 30;   theatre[4].h = 30;

	theatre[5].x = 322;  theatre[6].x = 322;  theatre[7].x = 322;  theatre[8].x = 324;  theatre[9].x = 372;
	theatre[5].y = 862;  theatre[6].y = 916;  theatre[7].y = 970;  theatre[8].y = 1024; theatre[9].y = 1078;
	theatre[5].w = 292;  theatre[6].w = 292;  theatre[7].w = 292;  theatre[8].w = 308;  theatre[9].w = 292;
	theatre[5].h = 30;   theatre[6].h = 30;   theatre[7].h = 30;   theatre[8].h = 30;   theatre[9].h = 30;

	theatre[10].x = 372; theatre[11].x = 704; theatre[12].x = 704; theatre[13].x = 704; theatre[14].x = 704;
	theatre[10].y = 1132;theatre[11].y = 646; theatre[12].y = 700; theatre[13].y = 754; theatre[14].y = 808;
	theatre[10].w = 292; theatre[11].w = 392; theatre[12].w = 392; theatre[13].w = 392; theatre[14].w = 392;
	theatre[10].h = 30;  theatre[11].h = 30;  theatre[12].h = 30;  theatre[13].h = 30;  theatre[14].h = 30;

	theatre[15].x = 704; theatre[16].x = 704; theatre[17].x = 736; theatre[18].x = 754; theatre[19].x = 754;
	theatre[15].y = 862; theatre[16].y = 916; theatre[17].y = 970; theatre[18].y = 1024;theatre[19].y = 1078;
	theatre[15].w = 392; theatre[16].w = 392; theatre[17].w = 328; theatre[18].w = 292; theatre[19].w = 292;
	theatre[15].h = 30;  theatre[16].h = 30;  theatre[17].h = 30;  theatre[18].h = 30;  theatre[19].h = 30;

	theatre[20].x = 754; theatre[21].x = 1312;theatre[22].x = 1236;theatre[23].x = 1186;theatre[24].x = 1186;
	theatre[20].y = 1132;theatre[21].y = 646; theatre[22].y = 700; theatre[23].y = 754; theatre[24].y = 808;
	theatre[20].w = 292; theatre[21].w = 143; theatre[22].w = 243; theatre[23].w = 292; theatre[24].w = 292;
	theatre[20].h = 30;  theatre[21].h = 30;  theatre[22].h = 30;  theatre[23].h = 30;  theatre[24].h = 30;

	theatre[25].x = 1186;theatre[26].x = 1186;theatre[27].x = 1186;theatre[28].x = 1166;theatre[29].x = 1136;
	theatre[25].y = 862; theatre[26].y = 916; theatre[27].y = 970; theatre[28].y = 1024;theatre[29].y = 1078;
	theatre[25].w = 292; theatre[26].w = 292; theatre[27].w = 292; theatre[28].w = 309; theatre[29].w = 292;
	theatre[25].h = 30;  theatre[26].h = 30;  theatre[27].h = 30;  theatre[28].h = 30;  theatre[29].h = 30;

	theatre[30].x = 1136;theatre[31].x = 0;   theatre[32].x = 1550;theatre[33].x = 0;   theatre[34].x = 142;
	theatre[30].y = 1132;theatre[31].y = 300; theatre[32].y = 300; theatre[33].y = 1295;theatre[34].y = 178;
	theatre[30].w = 292; theatre[31].w = 250; theatre[32].w = 250; theatre[33].w = 1800;theatre[34].w = 238;
	theatre[30].h = 30;  theatre[31].h = 1000;theatre[32].h = 1000;theatre[33].h = 5;   theatre[34].h = 1;

	theatre[35].x = 1419;theatre[36].x = 1419;theatre[37].x = 142; theatre[38].x = 142; theatre[39].x = 1419;
	theatre[35].y = 178; theatre[36].y = 244; theatre[37].y = 244; theatre[38].y = 310; theatre[39].y = 310;
	theatre[35].w = 238; theatre[36].w = 238; theatre[37].w = 238; theatre[38].w = 238; theatre[39].w = 238;
	theatre[35].h = 1;   theatre[36].h = 1;   theatre[37].h = 1;   theatre[38].h = 1;   theatre[39].h = 1;

	theatre[40].x = 0;   theatre[41].x = 1493;theatre[42].x = 835; theatre[43].x = 1734; theatre[44].x = 0;
	theatre[40].y = 300; theatre[41].y = 300; theatre[42].y = 232; theatre[43].y = 86;   theatre[44].y = 164;
	theatre[40].w = 309; theatre[41].w = 309; theatre[42].w = 171; theatre[43].w = 38;   theatre[44].w = 72;
	theatre[40].h = 212; theatre[41].h = 212; theatre[42].h = 25;  theatre[43].h = 153;  theatre[44].h = 73;

	theatre[45].x = 480; theatre[46].x = 0;   theatre[47].x = 1778;theatre[48].x = 21;   //theatre[49].x = 0;
	theatre[45].y = 300; theatre[46].y = 73;  theatre[47].y = 0;   theatre[48].y = 0;    //theatre[49].y = 164;
	theatre[45].w = 74;  theatre[46].w = 1800;theatre[47].w = 1;   theatre[48].w = 1;    //theatre[49].w = 72;
	theatre[45].h = 25;  theatre[46].h = 1;   theatre[47].h = 300; theatre[48].h = 300;  //theatre[49].h = 73;
}

void setBackstage(SDL_Rect backstage[30])
{
	backstage[0].x = 30;   backstage[1].x = 0;   backstage[2].x = 0;   backstage[3].x = 1567;
	backstage[0].y = 0;    backstage[1].y = 0;   backstage[2].y = 181; backstage[3].y = 0;
	backstage[0].w = 0;    backstage[1].w = 1651;backstage[2].w = 1362;backstage[3].w = 84;
	backstage[0].h = 0;    backstage[1].h = 63;  backstage[2].h = 999; backstage[3].h = 1180;

	backstage[4].x = 974;  backstage[5].x = 772; backstage[6].x = 345; backstage[7].x = 148;
	backstage[4].y = 42;   backstage[5].y = 37;  backstage[6].y = 63;  backstage[7].y = 64;
	backstage[4].w = 121;  backstage[5].w = 179; backstage[6].w = 4;   backstage[7].w = 47;
	backstage[4].h = 50;   backstage[5].h = 64;  backstage[6].h = 117; backstage[7].h = 31;

	backstage[8].x = 70;   backstage[9].x = 0;   backstage[10].x = 0;  backstage[11].x = 30;
	backstage[8].y = 64;   backstage[9].y = 64;  backstage[10].y = 86; backstage[11].y = 129;
	backstage[8].w = 75;   backstage[9].w = 50;  backstage[10].w = 32; backstage[11].w = 16;
	backstage[8].h = 36;   backstage[9].h = 32;  backstage[10].h = 42; backstage[11].h = 45;

	backstage[12].x = 1363;backstage[13].x = 0;  backstage[14].x = 0;  backstage[15].x = 30;
	backstage[12].y = 480; backstage[13].y = 64; backstage[14].y = 86; backstage[15].y = 129;
	backstage[12].w = 204; backstage[13].w = 50; backstage[14].w = 32; backstage[15].w = 16;
	backstage[12].h = 4;   backstage[13].h = 32; backstage[14].h = 42; backstage[15].h = 45;

	// last three havn't been set (just there for convience) 
}

void setMaleDressingRoom(SDL_Rect room[10])
{
	room[0].x = 10;   room[1].x = 174;  room[2].x = 580;  room[3].x = 0;
	room[0].y = 0;    room[1].y = 0;    room[2].y = 0;    room[3].y = 492;
	room[0].w = 0;    room[1].w = 1;    room[2].w = 1;    room[3].w = 900;
	room[0].h = 0;    room[1].h = 700;  room[2].h = 700;  room[3].h = 1;

	room[4].x = 0;    room[5].x = 167;  room[6].x = 563;
	room[4].y = 174;  room[5].y = 322;  room[6].y = 254;
	room[4].w = 900;  room[5].w = 31;   room[6].w = 10;
	room[4].h = 1;    room[5].h = 170;  room[6].h = 250;
}

void setTheatre2(SDL_Rect theatre[5])
{
	theatre[0].x = 5;    theatre[1].x = 0;    theatre[2].x = 700;    theatre[3].x = 0; // fix later
	theatre[0].y = 5;    theatre[1].y = 600;  theatre[2].y = 545;    theatre[3].y = 0;
	theatre[0].w = 0;    theatre[1].w = 700;  theatre[2].w = 1400;   theatre[3].w = 20;
	theatre[0].h = 0;    theatre[1].h = 100;  theatre[2].h = 200;    theatre[3].h = 700;

	theatre[4].x = 1580;
	theatre[4].y = 0;
	theatre[4].w = 20;
	theatre[4].h = 450;
}

void setBackstage2(SDL_Rect backstage[17]) // backstage[0].y == where new group STARTS!!
{
	backstage[0].x = 17;   backstage[1].x = 0;    backstage[2].x = 204;  backstage[3].x = 0;
	backstage[0].y = 13;   backstage[1].y = 600;  backstage[2].y = 540;  backstage[3].y = 0;
	backstage[0].w = 0;    backstage[1].w = 262;  backstage[2].w = 413;  backstage[3].w = 255;
	backstage[0].h = 0;    backstage[1].h = 100;  backstage[2].h = 160;  backstage[3].h = 355;

	backstage[4].x = 425;  backstage[5].x = 1547; backstage[6].x = 904;  backstage[7].x = 812;
	backstage[4].y = 234;  backstage[5].y = 365;  backstage[6].y = 0;    backstage[7].y = 600;
	backstage[4].w = 192;  backstage[5].w = 94;   backstage[6].w = 31;   backstage[7].w = 198;
	backstage[4].h = 466;  backstage[5].h = 16;   backstage[6].h = 493;  backstage[7].h = 100;

	backstage[8].x = 1094; backstage[9].x = 1317; backstage[10].x = 0;   backstage[11].x = 1980;
	backstage[8].y = 528;  backstage[9].y = 365;  backstage[10].y = 0;   backstage[11].y = 0;
	backstage[8].w = 94;   backstage[9].w = 94;   backstage[10].w = 2000;backstage[11].w = 20;
	backstage[8].h = 16;   backstage[9].h = 16;   backstage[10].h = 40;  backstage[11].h = 264;

	backstage[12].x = 1837;
	backstage[12].y = 383;
	backstage[12].w = 163;
	backstage[12].h = 317;

	backstage[13].x = 747; backstage[14].x = 747; backstage[15].x = 1452;backstage[16].x = 1452;
	backstage[13].y = 0;   backstage[14].y = 489; backstage[15].y = 41;  backstage[16].y = 382;
	backstage[13].w = 64;  backstage[14].w = 64;  backstage[15].w = 64;  backstage[16].w = 64;
	backstage[13].h = 338; backstage[14].h = 211; backstage[15].h = 223; backstage[16].h = 318;
}

void setStage1(SDL_Rect room[18])
{
	room[0].x = 18;   room[1].x = 0;    room[2].x = 0;    room[3].x = 0;
	room[0].y = 15;   room[1].y = 1850; room[2].y = 0;    room[3].y = 0;
	room[0].w = 0;    room[1].w = 184;  room[2].w = 20;   room[3].w = 900;
	room[0].h = 0;    room[1].h = 150;  room[2].h = 1750; room[3].h = 20;

	room[4].x = 880;  room[5].x = 349;  room[6].x = 258;  room[7].x = 349;
	room[4].y = 175;  room[5].y = 1475; room[6].y = 1168; room[7].y = 1168;
	room[4].w = 20;   room[5].w = 212;  room[6].w = 219;  room[7].w = 128;
	room[4].h = 1900; room[5].h = 20;   room[6].h = 20;   room[7].h = 112;

	room[8].x = 0;    room[9].x = 339;  room[10].x = 500;
	room[8].y = 363;  room[9].y = 696;  room[10].y = 736;
	room[8].w = 161;  room[9].w = 20;   room[10].w = 20;
	room[8].h = 560;  room[9].h = 115;  room[10].h = 115;

	room[12].x = 0;   room[14].x = 777; room[15].x = 339;
	room[12].y = 0;   room[14].y = 175; room[15].y = 686;
	room[12].w = 281; room[14].w = 123; room[15].w = 20;
	room[12].h = 383; room[14].h = 20;  room[15].h = 9;

	room[16].x = 500;
	room[16].y = 726;
	room[16].w = 20;
	room[16].h = 9;
}

void setStage2(SDL_Rect room[20])
{
	room[0].x = 20;   room[1].x = 0;    room[2].x = 0;    room[3].x = 880;
	room[0].y = 9;    room[1].y = 205;  room[2].y = 0;    room[3].y = 0;
	room[0].w = 0;    room[1].w = 100;  room[2].w = 900;  room[3].w = 20;
	room[0].h = 0;    room[1].h = 1795; room[2].h = 20;   room[3].h = 1675;

	room[4].x = 295;  room[5].x = 295;  room[6].x = 590;  room[7].x = 814;
	room[4].y = 0;    room[5].y = 1955; room[6].y = 227;  room[7].y = 1820;
	room[4].w = 100;  room[5].w = 100;  room[6].w = 100;  room[7].w = 86;
	room[4].h = 1811; room[5].h = 1795; room[6].h = 1773; room[7].h = 20;

	room[8].x = 880;  //room[1].x = 295;  room[2].x = 590;  room[3].x = 880;
	room[8].y = 1820; //room[1].y = 1955; room[2].y = 227;  room[3].y = 0;
	room[8].w = 20;   //room[1].w = 100;  room[2].w = 100;  room[3].w = 20;
	room[8].h = 180;  //room[1].h = 1795; room[2].h = 1773; room[3].h = 1675;
}

void setTutorial(SDL_Rect room[4])
{
	room[0].x = 4;    room[1].x = 0;    room[2].x = 0;    room[3].x = 550;
	room[0].y = 4;    room[1].y = 0;    room[2].y = 600;  room[3].y = 600;
	room[0].w = 0;    room[1].w = 900;  room[2].w = 400;  room[3].w = 350;
	room[0].h = 0;    room[1].h = 500;  room[2].h = 100;  room[3].h = 100;
}

void setTutorial2(SDL_Rect room[5])
{
	room[0].x = 5;    room[1].x = 0;    room[2].x = 0;    room[3].x = 575;
	room[0].y = 5;    room[1].y = 0;    room[2].y = 600;  room[3].y = 200;
	room[0].w = 0;    room[1].w = 400;  room[2].w = 900;  room[3].w = 325;
	room[0].h = 0;    room[1].h = 500;  room[2].h = 100;  room[3].h = 500;

	room[4].x = 0;
	room[4].y = 0;
	room[4].w = 900;
	room[4].h = 20; 
}

void setArea1(SDL_Rect room[7])
{
	room[0].x = 7;    room[1].x = 0;    room[2].x = 880;  room[3].x = 0;
	room[0].y = 7;    room[1].y = 0;    room[2].y = 0;    room[3].y = 0;
	room[0].w = 0;    room[1].w = 20;   room[2].w = 20;   room[3].w = 900;
	room[0].h = 0;    room[1].h = 500;  room[2].h = 500;  room[3].h = 20;

	room[4].x = 0;    room[5].x = 350;  room[6].x = 200;
	room[4].y = 600;  room[5].y = 398;  room[6].y = 300;
	room[4].w = 900;  room[5].w = 200;  room[6].w = 500;
	room[4].h = 100;  room[5].h = 400;  room[6].h = 100;
}

void setArea2(SDL_Rect room[9])
{
	room[0].x = 9;    room[1].x = 0;    room[2].x = 0;    room[3].x = 654;
	room[0].y = 9;    room[1].y = 0;    room[2].y = 0;    room[3].y = 120;
	room[0].w = 0;    room[1].w = 20;   room[2].w = 900;  room[3].w = 246;
	room[0].h = 0;    room[1].h = 500;  room[2].h = 20;   room[3].h = 20;

	room[4].x = 0;    room[5].x = 800;  room[6].x = 445;  room[7].x = 111;
	room[4].y = 600;  room[5].y = 120;  room[6].y = 433;  room[7].y = 374;
	room[4].w = 900;  room[5].w = 100;  room[6].w = 200;  room[7].w = 200;
	room[4].h = 100;  room[5].h = 580;  room[6].h = 20;   room[7].h = 20;

	room[8].x = 316;  //room[5].x = 800;  room[6].x = 445;  room[7].x = 97;
	room[8].y = 209;  //room[5].y = 120;  room[6].y = 384;  room[7].y = 325;
	room[8].w = 200;  //room[5].w = 100;  room[6].w = 200;  room[7].w = 200;
	room[8].h = 20;   //room[5].h = 580;  room[6].h = 20;   room[7].h = 20;
}

void setArea3(SDL_Rect room[8])
{
	room[0].x = 8;    room[1].x = 0;    room[2].x = 0;    room[3].x = 880;
	room[0].y = 8;    room[1].y = 0;    room[2].y = 120;  room[3].y = 0;
	room[0].w = 0;    room[1].w = 900;  room[2].w = 200;  room[3].w = 20;
	room[0].h = 0;    room[1].h = 20;   room[2].h = 680;  room[3].h = 500;

	room[4].x = 750;  room[5].x = 320;  room[6].x = 600;  room[7].x = 350;
	room[4].y = 600;  room[5].y = 200;  room[6].y = 340;  room[7].y = 520;
	room[4].w = 150;  room[5].w = 200;  room[6].w = 120;  room[7].w = 260;
	room[4].h = 100;  room[5].h = 20;   room[6].h = 20;   room[7].h = 20;
}

void setArea4(SDL_Rect room[5])
{
	room[0].x = 5;    room[1].x = 0;    room[2].x = 0;    room[3].x = 880;
	room[0].y = 5;    room[1].y = 600;  room[2].y = 0;    room[3].y = 0;
	room[0].w = 0;    room[1].w = 900;  room[2].w = 20;   room[3].w = 20;
	room[0].h = 0;    room[1].h = 100;  room[2].h = 500;  room[3].h = 500;

	room[4].x = 0;
	room[4].y = 0;
	room[4].w = 900;
	room[4].h = 20;
}
